connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent JTAG-HS2 210241179917" && level==0 && jtag_device_ctx=="jsn-JTAG-HS2-210241179917-13722093-0"}
fpga -file D:/Dinomotion/dinocon1/SW/Steg_app/_ide/bitstream/STEG_TOP.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw D:/Dinomotion/dinocon1/SW/STEG_TOP/export/STEG_TOP/hw/STEG_TOP.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source D:/Dinomotion/dinocon1/SW/Steg_app/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#1"}
dow D:/Dinomotion/dinocon1/SW/Steg_app/Debug/Steg_app.elf
configparams force-mem-access 0
bpadd -addr &main
