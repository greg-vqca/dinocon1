connect -url tcp:127.0.0.1:3121
targets -set -nocase -filter {name =~"APU*"}
rst -system
after 3000
targets -set -filter {jtag_cable_name =~ "Digilent JTAG-HS2 210241179917" && level==0 && jtag_device_ctx=="jsn-JTAG-HS2-210241179917-13722093-0"}
fpga -file D:/Dinomotion/Dinocon1/SW/ZturnDriveCore1/_ide/bitstream/ZturnDesign_wrapper.bit
targets -set -nocase -filter {name =~"APU*"}
loadhw -hw D:/Dinomotion/Dinocon1/SW/ZturnDesign_wrapper/export/ZturnDesign_wrapper/hw/ZturnDesign_wrapper.xsa -mem-ranges [list {0x40000000 0xbfffffff}] -regs
configparams force-mem-access 1
targets -set -nocase -filter {name =~"APU*"}
source D:/Dinomotion/Dinocon1/SW/ZturnDriveCore1/_ide/psinit/ps7_init.tcl
ps7_init
ps7_post_config
targets -set -nocase -filter {name =~ "*A9*#1"}
dow D:/Dinomotion/Dinocon1/SW/ZturnDriveCore1/Debug/ZturnDriveCore1.elf
configparams force-mem-access 0
bpadd -addr &main
