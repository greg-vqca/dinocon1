################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../HwIndependentDriveCode/source/RealTime/CurrentLoopDQ.c \
../HwIndependentDriveCode/source/RealTime/EncHandler.c \
../HwIndependentDriveCode/source/RealTime/FaultHandler.c \
../HwIndependentDriveCode/source/RealTime/HallsHandler.c \
../HwIndependentDriveCode/source/RealTime/I2T_Handler.c \
../HwIndependentDriveCode/source/RealTime/JogHandler.c \
../HwIndependentDriveCode/source/RealTime/PTPGeneratorFPU.c \
../HwIndependentDriveCode/source/RealTime/PosLoop.c \
../HwIndependentDriveCode/source/RealTime/QuaternionMath.c \
../HwIndependentDriveCode/source/RealTime/RealTime.c \
../HwIndependentDriveCode/source/RealTime/Recorder.c \
../HwIndependentDriveCode/source/RealTime/SPI_Handler.c \
../HwIndependentDriveCode/source/RealTime/StabilizationHandler.c \
../HwIndependentDriveCode/source/RealTime/VelLoop.c 

OBJS += \
./HwIndependentDriveCode/source/RealTime/CurrentLoopDQ.o \
./HwIndependentDriveCode/source/RealTime/EncHandler.o \
./HwIndependentDriveCode/source/RealTime/FaultHandler.o \
./HwIndependentDriveCode/source/RealTime/HallsHandler.o \
./HwIndependentDriveCode/source/RealTime/I2T_Handler.o \
./HwIndependentDriveCode/source/RealTime/JogHandler.o \
./HwIndependentDriveCode/source/RealTime/PTPGeneratorFPU.o \
./HwIndependentDriveCode/source/RealTime/PosLoop.o \
./HwIndependentDriveCode/source/RealTime/QuaternionMath.o \
./HwIndependentDriveCode/source/RealTime/RealTime.o \
./HwIndependentDriveCode/source/RealTime/Recorder.o \
./HwIndependentDriveCode/source/RealTime/SPI_Handler.o \
./HwIndependentDriveCode/source/RealTime/StabilizationHandler.o \
./HwIndependentDriveCode/source/RealTime/VelLoop.o 

C_DEPS += \
./HwIndependentDriveCode/source/RealTime/CurrentLoopDQ.d \
./HwIndependentDriveCode/source/RealTime/EncHandler.d \
./HwIndependentDriveCode/source/RealTime/FaultHandler.d \
./HwIndependentDriveCode/source/RealTime/HallsHandler.d \
./HwIndependentDriveCode/source/RealTime/I2T_Handler.d \
./HwIndependentDriveCode/source/RealTime/JogHandler.d \
./HwIndependentDriveCode/source/RealTime/PTPGeneratorFPU.d \
./HwIndependentDriveCode/source/RealTime/PosLoop.d \
./HwIndependentDriveCode/source/RealTime/QuaternionMath.d \
./HwIndependentDriveCode/source/RealTime/RealTime.d \
./HwIndependentDriveCode/source/RealTime/Recorder.d \
./HwIndependentDriveCode/source/RealTime/SPI_Handler.d \
./HwIndependentDriveCode/source/RealTime/StabilizationHandler.d \
./HwIndependentDriveCode/source/RealTime/VelLoop.d 


# Each subdirectory must supply rules for building sources it contributes
HwIndependentDriveCode/source/RealTime/%.o: ../HwIndependentDriveCode/source/RealTime/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -ID:/Dinomotion/dinocon1/SW/STEG_TOP/export/STEG_TOP/sw/STEG_TOP/standalone_domain/bspinclude/include -I"D:\Dinomotion\dinocon1\SW\Steg_app\HAL" -I"D:\Dinomotion\dinocon1\SW\Steg_app\HW_System\include" -I"D:\Dinomotion\dinocon1\SW\Steg_app\HwIndependentDriveCode\include" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -ID:/Dinomotion/dinocon1/SW/STEG_TOP/export/STEG_TOP/sw/STEG_TOP/standalone_domain/bspinclude/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


