/* created by SINTAB */
/* command line:sintab sint_dz  */

/* data file contents:
 * table_size              256
 * max_value               200
 * start_angle     0        -2.0943951     <- comment:)
 * type         const_signed_int
 * table_name       sin_table_A
 * output_file     sintab_A.h
 * third_harmonics off
 */
#ifndef SINTAB_H
#define SINTAB_H

#define TABLE_SIZE          1024


extern const float32_t  sin_table[TABLE_SIZE];
#endif

