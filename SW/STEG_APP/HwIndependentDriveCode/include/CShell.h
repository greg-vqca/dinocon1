#ifndef CSHEL_H
#define CSHEL_H
#define NEW_LINE "\n\r\0"
#define PROMPT "-->\0"


extern int16_t FindFunction(char* cmdName);
extern int16_t FindParam(char* cmdName);
extern void (*cmdFunc)(void);
extern void PrintFaults (Faults_tag fault);
extern void program(void);
extern void DumpEeprom(void);
extern void PrintHallsState(void);
extern void DisablePower(void);
extern void EnablePower(void);
extern void BothEnablePower(void);
extern void BothMoveAbs(void);

extern void LoadSavedParameters(void);
extern void PrintParametersSavedToEEPROM(void);
extern void JumpToBoot(void);
extern void SaveParameters(void);
extern void ReadStat(void);
extern void ClearFaultsHistory(void);
extern void ClearFaults(void);
extern void ReadFaultsHistory(void);
void ReadBothPFB(void);
void ReadBothENC(void);

extern void ReadWarnings(void);
extern void ReadFaults(void);
extern void StopRecorder(void);
extern void RecordCommand(void);
extern void MoveAbsolute(void);
extern void MoveIncremental(void);
extern void parserGet(void);
extern void parserRecTrig(void);
extern void printVersion(void);
extern void PrintSigned(long val);
extern int16_t CalculateAxis (void);

extern void ParseCMD (void);
extern void ParsePDO (void);
extern void ParseSDO (void);
extern void SetPwmFreq(void);
void ZeroCmdOffset(void);


void readmemory(void);
void writememory(void);


extern PDO_Dwn_Packet_Tag      PrevPDO_Dwn_Packet;
extern PDO_Dwn_Packet_Tag      PDO_Dwn_Packet;
extern  PDO_Up_Packet_Tag       PDO_Up_Packet;
void SendSDO (void);

#endif

