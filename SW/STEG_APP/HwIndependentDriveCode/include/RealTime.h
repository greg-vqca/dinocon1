#ifndef REALTIME_H
#define REALTIME_H

#if TI_DSP_Pragma
__interrupt void HighPriorityInterupt(void);

#endif


#if Xilinx_Pragma
void HighPriorityInterupt(void *data, uint8_t TmrCtrNumber) ;  // ADC  (Can also be ISR for INT10.1 when enabled)

#endif


#if TI_DSP_Pragma
__interrupt
#endif
void UserInt1Func(void *data, uint8_t TmrCtrNumber);

#if TI_DSP_Pragma
__interrupt
#endif
void readSCIB(void);

extern int32_t	WatchDog;
extern	int32_t WatchDogTime;
extern int16_t WatchDogFlag;
extern int16_t  PWM_Freq_scaling;

extern uint16_t  Led_CNTR[NumberOfAxes];
extern	int32_t mSecCounter;
extern int16_t FirstSpiRead;

extern int32_t SPI_Data_checkSumDiffCNTR;
extern int32_t volatile Max_PE_in_Jog;
extern int32_t volatile MaxSystemPe;

extern int32_t interpolationCTR;
extern volatile int16_t EEPROM_Delay;


extern int32_t  DistanceToPositiveLimmit[NumberOfAxes];
extern int32_t  DistanceToNegativeLimmit[NumberOfAxes];


extern float32_t Test_runTime;
extern float32_t MaxTest_runTime;

extern uint32_t Test_endTime;
extern uint32_t Test_timePeriod;

extern uint32_t Test_startTime ;

extern int16_t CPLD_SPI_Data_Recieved_Flag;
extern int16_t CPLD_FIRST_SPI_Data_Recieved_Flag;
extern int16_t CPLD_SPI_FaultCtr;
#endif
