#ifndef QUATERNIONMATH_H
#define QUATERNIONMATH_H


typedef struct Quaternion_struct_tag {
    float32_t x;
    float32_t y;
    float32_t z;
    float32_t w;
} Quaternion_struct;

typedef struct Angles_struct_tag{
    float32_t yaw;
    float32_t roll;
    float32_t pitch;
} Angles_struct;


Quaternion_struct  fromAngles(   float yaw, float roll, float pitch) ;
Angles_struct toAngles(float w,float x,float y,float z);
Quaternion_struct fromAngleNormalAxis(float angle, Angles_struct axis);
Quaternion_struct  Quaternion_mult(Quaternion_struct a, Quaternion_struct b);

#endif
