
#ifndef COMMANDLIST_H
#define COMMANDLIST_H
typedef struct
{
	int32_t			ParamPointer;
	int64_t			Min;
	int64_t	 			Max;
	int64_t				Default;
	char*			Name; //maximum command length MUST NOT exid 15 chars!
	int16_t				Signed;
	uint16_t				Length;
	int16_t			SavedToEEPROM;
}ParamStruct_tag;



typedef struct
{
	int32_t			FunctionPointer;
	char*			Name; //maximum command length MUST NOT exid 15 chars!

}FuncStruct_tag;

extern const ParamStruct_tag ParamStruct [];
extern const FuncStruct_tag FuncStruct [];
//test
extern int32_t Runtime;


#endif
