#ifndef SCIHANDLER_H
#define SCIHANDLER_H


void PrintChar(char character);
void PrintStr(char* Str);
void writeSCIB(void);

extern void  HandleSCIB(void);
extern void ReadSCIA(void);


extern int16_t      PDOCmdFlagSCIB,PDOFbkFlagSCIB;
extern int16_t      SDOCmdFlagSCIB,SDOCmdLength;
extern int32_t ParserPeriod;
extern int32_t ParserStartTime;


extern int32_t MaxParserPeriod;
extern int32_t PDOPeriod;

extern  char SCIB_BufferIn[];

extern int16_t SCIA_BufferInPointer;
extern  char SCIA_BufferIn[];
extern  char SerCommandBufferOut[];

extern int16_t SCIB_RX_BufferInPointer;
extern int16_t SCIB_RX_BufferLevel;
extern int16_t SCIB_RX_BufferOutPointer;

extern int16_t SCIB_TX_BufferInPointer;
extern volatile int16_t SCIB_TX_BufferLevel;
extern volatile int16_t SCIB_TX_BufferPacketLenght;
extern int16_t SCIB_TX_BufferOutPointer;

extern  char SerCommandBufferA[];
extern  char SerCommandBufferB[];
extern char SCIB_PDO_BufferOut[];
extern  int16_t     NewCmdFlagSCIA;
extern int16_t SCIB_PDO_TX_Transmission_Ready;
extern int16_t  SCIB_SDO_TX_Transmission_Ready;

extern volatile int16_t BufferOF ;

extern int16_t SDOBufferOutPtr;
extern char SCIB_BufferOut[];

extern char SCIB_SDO_BufferOut[];
extern int16_t SCIB_SDO_TX_BufferLevel;
extern int16_t SDOBufferOutLVL;
extern int16_t SCIB_SDO_TX_BufferOutPointer;




#endif
