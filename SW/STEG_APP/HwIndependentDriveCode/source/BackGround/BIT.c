

#include "GlobalIncludes.h"
int32_t BIT_Timer[NumberOfAxes]={0,0};
float32_t FilteredCurrentA[NumberOfAxes]={0,0};
float32_t FilteredCurrentB[NumberOfAxes]={0,0};
float32_t FilteredCurrentC[NumberOfAxes]={0,0};

float32_t CalculatedResistance[NumberOfAxes]={0,0};
int16_t adi_tst1=0;
int16_t adi_tst2=0;

int16_t PhaseTestVoltage =30;
void BIT_Handler(int16_t a)
{


    switch (Axis[a].BuidInTest.State)
    {
    case IdleBIT:
        break;
    case  InitBIT:

        PWM_Enable(a,1,0);
        PWM_Enable(a,2,0);
        PWM_Enable(a,3,0);


        PWM_out(a,1,0);
        PWM_out(a,2,0);
        PWM_out(a,3,0);

        Axis[a].BuidInTest.Results = Noresult;

        Axis[a].Drive.Faults = no_faults;
        Axis[a].Drive.Faults_history = no_faults;
        Axis[a].Drive.State = enable;
        Axis[a].BuidInTest.State=   PerformePhaseAandC_BIT;
        BIT_Timer[a] =0;
        FilteredCurrentA[a]=0.0;
        FilteredCurrentB[a]=0.0;
        FilteredCurrentC[a]=0.0;

        break;

    case  PerformePhaseAandC_BIT:

        if(abs((int16_t)(Axis[a].Current.Ia_OffSet-(1<<11)* 0.70361328125))>200)
        {
            Axis[a].BuidInTest.Results |= MotorPhaseA_CurrentSence;
            Axis[a].BuidInTest.State=FinishBIT;
        }


        if(abs((int16_t)(Axis[a].Current.Ic_OffSet-(1<<11) * 0.70361328125))>200)
        {
            if(a==0)
            {
                Axis[a].BuidInTest.Results |= MotorPhaseC_CurrentSence;
            }
            Axis[a].BuidInTest.State=FinishBIT;
        }

        PWM_Enable(a,1,1);
        PWM_Enable(a,2,0);
        PWM_Enable(a,3,1);

        if(a==0)
        {
            PWM_out(a,1,(uint16_t)(PWM_TBPRD)+(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus); //3 volt on phases
            PWM_out(a,3,(uint16_t)PWM_TBPRD);//(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus;
        }
        else  if(a==1)
        {
            PWM_out(a,1,(uint16_t)(PWM_TBPRD)+(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus); //3 volt on phases
            PWM_out(a,3,(uint16_t)PWM_TBPRD);//(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus;
        }

        if(BIT_Timer[a]>100)
        {
            FilteredCurrentA[a]+=(Axis[a].Current.Ia-FilteredCurrentA[a])/20.0;
            FilteredCurrentC[a]+=(Axis[a].Current.Ic-FilteredCurrentC[a])/20.0;

        }
        if(BIT_Timer[a]>2000)
        {
            if(FilteredCurrentA[a]>0)
            {
                CalculatedResistance[a] =((float32_t)PhaseTestVoltage/10) / (FilteredCurrentA[a]/100.0)/2;
            }
            else
            {
                Axis[a].BuidInTest.Results |= MotorPhaseA_CurrentSence;

            }
            if(FilteredCurrentC[a]>=0)
            {
                Axis[a].BuidInTest.Results |= MotorPhaseC_CurrentSence;
            }

            if(FilteredCurrentA[a]+FilteredCurrentC[a]>10)
            {
                Axis[a].BuidInTest.Results |= MotorPhaseC_CurrentSence;
                Axis[a].BuidInTest.Results |= MotorPhaseA_CurrentSence;

            }
            if((CalculatedResistance[a]*1000 -Axis[a].BuidInTest.Phase_resistance )>100)
            {
                Axis[a].BuidInTest.Results |= MotorPhaseA;
                Axis[a].BuidInTest.Results |= MotorPhaseC;

            }
            Axis[a].BuidInTest.State=PerformePhaseAandB_BIT;


        }
        if( Axis[a].BuidInTest.Results != Noresult)
        {
            Axis[a].BuidInTest.State=FinishBIT;
        }

        break;


    case  PerformePhaseAandB_BIT:

        if(abs((int16_t)(Axis[a].Current.Ia_OffSet-(1<<11)* 0.70361328125))>200)
        {
            Axis[a].BuidInTest.Results |= MotorPhaseA_CurrentSence;
            Axis[a].BuidInTest.State=FinishBIT;
        }


        if(abs((int16_t)(Axis[a].Current.Ic_OffSet-(1<<11) * 0.70361328125))>200)
        {
            if(a==0)
                Axis[a].BuidInTest.Results |= MotorPhaseC_CurrentSence;
            Axis[a].BuidInTest.State=FinishBIT;
        }



        PWM_Enable(a,1,1);
        PWM_Enable(a,2,1);
        PWM_Enable(a,3,0);

        if(a==0)
        {
            PWM_out(a,1,(uint16_t)(PWM_TBPRD)+(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus); //3 volt on phases
            PWM_out(a,2,(uint16_t)PWM_TBPRD);//(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus;
        }
        else  if(a==1)
        {
            PWM_out(a,1,(uint16_t)(PWM_TBPRD)+(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus); //3 volt on phases
            PWM_out(a,2,(uint16_t)PWM_TBPRD);//(int32_t)PhaseTestVoltage*(PWM_TBPRD*2)/Axis[0].Drive.VBus;
        }


        if(BIT_Timer[a]>100)
        {
            FilteredCurrentA[a]+=(Axis[a].Current.Ia-FilteredCurrentA[a])/20.0;
            FilteredCurrentC[a]+=(Axis[a].Current.Ic-FilteredCurrentC[a])/20.0;

        }
        if(BIT_Timer[a]>2000)
        {
            if(FilteredCurrentA[a]>0)
            {
                CalculatedResistance[a] =((float32_t)PhaseTestVoltage/10) / (FilteredCurrentA[a]/100.0)/2;
            }
            else
            {
                Axis[a].BuidInTest.Results |= MotorPhaseA_CurrentSence;

            }
            if((FilteredCurrentC[a]>=300)||(FilteredCurrentC[a]<=-300))
            {
                Axis[a].BuidInTest.Results |= MotorPhaseC_CurrentSence;
            }

            if((CalculatedResistance[a]*1000 -Axis[a].BuidInTest.Phase_resistance )>100)
            {
                Axis[a].BuidInTest.Results |= MotorPhaseA;
                Axis[a].BuidInTest.Results |= MotorPhaseB;

            }
            Axis[a].BuidInTest.State=FinishBIT;


        }
        if( Axis[a].BuidInTest.Results != Noresult)
        {
            Axis[a].BuidInTest.State=FinishBIT;
        }

        break;


    case FinishBIT:
        if(    Axis[a].Drive.State == enable)
            Axis[a].Drive.State = disable;
        Axis[a].Current.Phase_A_output=0;
        Axis[a].Current.Phase_B_output=0;
        Axis[a].Current.Phase_C_output=0;
        Axis[a].BuidInTest.State= IdleBIT;
        break;
    }
    if(Axis[a].BuidInTest.State!=IdleBIT)
    {
        FaultHandler_func(a);
        if(Axis[a].Drive.Faults_history  !=no_faults)
        {
            Axis[a].BuidInTest.State= FinishBIT;
            Axis[a].BuidInTest.Results|= FaultDuringTest;
        }

    }

}
