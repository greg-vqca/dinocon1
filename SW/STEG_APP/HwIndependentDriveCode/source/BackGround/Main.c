#define GLOBAL
#include "GlobalIncludes.h"

#undef GLOBAL

Axis_tag 		Axis[NumberOfAxes];

//SPI_Dwn_Packet_Tag		SPI_Dwn_Packet;
//SPI_Up_Packet_Tag		SPI_Up_Packet;
#define BOOT_RSVD_RAM ((long*)0x10)
void CLA_Init_Interrupts(void);

volatile int16_t temp[6];
unsigned long CPDL_Version=0;
int16_t I2C_Read_CPLD_Byte(uint16_t address);
void main(void)
{
    int16_t i;
    unsigned long  LoopCount;
    InitFinishedFlag=0;

    Init();

    UpdateScalings(0);
    UpdateScalings(1);

    ParseCMD();
    //ParseVector();
   // EnableWatchDog();
    //CLA_Init_Interrupts();
    for(i=0;i<=1;i++)
    {
        Motor_FBK_Failed[i]=0;
        Load_FBK_Failed[i]=0;
        Halls_FBK_Failed[i]=0;
        Motor_FBK_FailedDelay[i]=0;
        General_FBK_FailedDelay[i]=0;
        Illegal_halls_Flag[i]=0;
        AbsEncFirstReadingRecievedFlag[i]=0;
        PrevAbsEncFirstReadingRecievedFlag[i]=0;
        Axis[i].Current.Commutation_timer=1000000;
        Axis[i].Current.Command=0;
        //  Axis[i].Feedback.GyroItegrated=0;
        Axis[i].Feedback.GyroShiftedoutItegrated=0;

        Axis[i].Serial.Vcmd=0;
        Axis[i].Serial.Jcmd=0;
        Axis[i].Serial.Icmd=0;
        Axis[i].Position.LimitedCommand= Axis[i].Feedback.IntegratedDegPosition;
        Axis[i].Position.LastCommand =Axis[i].Position.LimitedCommand;
        Axis[i].Position.SystemCommand= Axis[i].Position.LimitedCommand;
        Axis[i].Ptp.EndCommand = Axis[i].Position.SystemCommand;
        Axis[i].Ptp.Output=0;
        Axis[i].Ptp.DistanceLeft_f=0;
        Axis[i].Ptp.Vel_f=0;
        Axis[i].Current.Command=0;
        Axis[i].Velocity.Command=0;
        Axis[i].Ptp.Command =0;
        Axis[i].Position.Output=0;
        Axis[i].Position.PeInteg=0;
        Axis[i].Velocity.Param.ErrorInteg=0;
        HWD_DisablePwm(i);
        Axis[i].Current.Phase_A_output = 0;
        Axis[i].Current.Phase_B_output = 0;
        Axis[i].Current.Phase_C_output = 0;
        Axis[i].Current.Commutation_timer=4000;
        Axis[i].Current.Effort=0;
        Axis[i].Feedback.GyroOffset =Axis[i].Feedback.BaseGyroPos  ;
        Axis[i].Feedback.Load.EncPosition=0;
        Axis[i].Feedback.Load.PrevEncPosition=0;
        Axis[i].Feedback.Motor.PrevEncPositionScaledToDeg=0;

        PrevAbsEncFirstReadingRecievedFlag[i]=0;

    }

    Axis[0].Feedback.Load.SmoothDegPosition =0;
    Axis[0].Feedback.Load.DegPosition=0;
    Axis[0].Feedback.Motor.DegPosition=0;
    Axis[0].Feedback.Halls.DegPosition =0;


    Axis[1].Feedback.Load.SmoothDegPosition =900000;
    Axis[1].Feedback.Load.DegPosition=900000;
    Axis[1].Feedback.Motor.DegPosition=900000;
    Axis[1].Feedback.Halls.DegPosition =900000;


    temp[1] = I2C_Read_CPLD_Byte(0x10);
    temp[2] = I2C_Read_CPLD_Byte(0x11);
    temp[3] = I2C_Read_CPLD_Byte(0x12);
    temp[4] = I2C_Read_CPLD_Byte(0x13);
    CPDL_Version=(unsigned long) (((unsigned long) temp[4]<<24)+((unsigned long) temp[3]<<16)+((unsigned long) temp[2]<<8)+(unsigned long) temp[1]);
    InitFinishedFlag=1;

    EnableWatchDog();

    // EnableDog();
    InitSystem();


  //  Axis[0].Current.ForcedCommutFlag=1;
    PWM_TBPRD=0x5f6;
 //   Axis[0].Serial.Jcmd=1000;
  //  Axis[0].Serial.Icmd	=100;

    for(;;)
    {

        TemperatureCompensateInternalOscilator();
        LoopCount++;
        if(DoSaveParameters)

        {
            PrepareSaveEeprom();
            DoSaveParameters--;
        }


        PerformSaveEeprom();
        ParseSDO();
        ParseCMD();
        ParsePDO();

        if(SDOBufferOutLVL>0)
            SendSDO();

        LedHandler(0);
        LedHandler(1);


        //	ParseVector();
        UpdateScalings(0);
        UpdateScalings(1);
        BIT_Handler(0);
        BIT_Handler(1);
        Axis[0].Drive.DriveTemp_sense=GetBoardTemperatureReading(0);
        Axis[1].Drive.DriveTemp_sense=GetBoardTemperatureReading(1);

        if(*BOOT_RSVD_RAM == 123456789)
        {
            EnableInterupts(0);

            StopInterupts();

            DisableWatchDog();
            BranchToBoot();
        }


    }

}

void UpdateScalings(int16_t a)
{

    int32_t saturatedV_Cruise;

    Axis[a].Velocity.MotorEncVelocityScaling =(float32_t) Axis[a].Motor.GearNominator / (Axis[a].Feedback.Motor.EncResolution*Axis[a].Motor.GearDenominator);
    Axis[a].Velocity.LoadEncVelocityScaling =(float32_t) Axis[a].Motor.GearNominator / (Axis[a].Feedback.Load.EncResolution*Axis[a].Motor.GearDenominator);

    Axis[a].Velocity.DegVelocityScaling  =(float32_t)0.0001*((float32_t)90000000/(float32_t)PWM_TBPRD/16)*1000;//   0.0001 deg/256 usec  =0.4 deg/sec we use 0.001 deg /sec resolusion

    saturatedV_Cruise=CustomIQsat(Axis[a].Ptp.Vcruise, (long)Axis[a].Velocity.Vmax, -(long)Axis[a].Velocity.Vmax);
    //	Axis[a].Ptp.ScaledVCruise=(long long)saturatedV_Cruise * (Axis[a].Feedback.Resolution<<2) * 179>>15 ;
    Axis[a].Ptp.ScaledVCruise_f =(float32_t)saturatedV_Cruise/ Axis[a].Velocity.DegVelocityScaling*2;

    Axis[a].Ptp.ScaledAcc_f = (float32_t) Axis[a].Ptp.Acc/100.0   ;
    Axis[a].Ptp.ScaledDec_f = (float32_t) Axis[a].Ptp.Dec/100.0 ;


  //  Axis[a].Jog.ScaledAcc = (long) Axis[a].Ptp.Acc * 840  ;
  //  Axis[a].Jog.ScaledDec=(long) Axis[a].Ptp.Dec * 840;


    Axis[a].Motor.GearScaling =(float32_t) Axis[a].Motor.GearNominator/Axis[a].Motor.GearDenominator;

    Axis[a].Velocity.Filter	=(long) ((long)((long)	Axis[a].Velocity.FilterFc  * 804 ))/(((long)1024+	((long)Axis[a].Velocity.FilterFc  * 804 >>10)));
    if(Axis[a].Velocity.Filter<1)Axis[a].Velocity.Filter=1;



    Axis[a].Feedback.Motor.PRD_ScalingFactor  =(float32_t)16384/Axis[a].Feedback.Motor.EncResolution;

    Axis[a].Feedback.Motor.DegScalingFactor  =(float32_t)PositionRevolutionRes*Axis[a].Motor.GearScaling/(Axis[a].Feedback.Motor.EncResolution*4);    Axis[a].Feedback.Load.DegScalingFactor  =(float32_t)PositionRevolutionRes/(Axis[a].Feedback.Load.EncResolution*4);

    Axis[a].Feedback.Halls.DegScalingFactor  =(float32_t)PositionRevolutionRes*Axis[a].Motor.GearScaling/(Axis[a].Motor.Poles*6/2);

    Axis[a].Current.Param.f_Gp =  Axis[a].Current.Param.Gp*(float32_t)1E-06;
    Axis[a].Current.Param.f_Gi = Axis[a].Current.Param.Gi*(float32_t)1E-06;
    Axis[a].Current.Param.f_GiAdap = Axis[a].Current.Param.GiAdap*(float32_t)1E-9;
    Axis[a].Current.Param.f_GpAdap = Axis[a].Current.Param.GpAdap*(float32_t)1E-9;


    Axis[a].Position.Param.f_Gff    =     Axis[a].Position.Param.Gff*(float32_t)1E-06;
    Axis[a].Position.Param.f_Gp =  Axis[a].Position.Param.Gp*(float32_t)1E-06;
    Axis[a].Position.Param.f_Gi = Axis[a].Position.Param.Gi*(float32_t)1E-06;
    Axis[a].Position.Param.f_Gd = Axis[a].Position.Param.Gd*(float32_t)1E-06;
    Axis[a].Position.Param.f_GiAdap = Axis[a].Position.Param.GiAdap*(float32_t)1E-9;
    Axis[a].Position.Param.f_GpAdap = Axis[a].Position.Param.GpAdap*(float32_t)1E-9;



    Axis[a].Velocity.Param.f_Gff    =     Axis[a].Velocity.Param.Gff*(float32_t)1E-06;
    Axis[a].Velocity.Param.f_Gp =  Axis[a].Velocity.Param.Gp*(float32_t)1E-06;
    Axis[a].Velocity.Param.f_Gi = Axis[a].Velocity.Param.Gi*(float32_t)1E-06;
    Axis[a].Velocity.Param.f_Gd = Axis[a].Velocity.Param.Gd*(float32_t)1E-06;
    Axis[a].Velocity.Param.f_GiAdap = Axis[a].Velocity.Param.GiAdap*(float32_t)1E-9;
    Axis[a].Velocity.Param.f_GpAdap = Axis[a].Velocity.Param.GpAdap*(float32_t)1E-9;


    SetIncrementalEncoderCounterDirection(a,Axis[a].Motor.Fbk_pol);

    SetIncrementalEncoderCounterResolution(a,Axis[a].Feedback.Motor.EncResolution<<2);


    if(GetIncrementalEncoderCounterValue(a) >(Axis[a].Feedback.Motor.EncResolution<<2))
    {
        SetIncrementalEncoderCounterValue(a, 0) ;
    }



}

void LedHandler(int16_t a)
{



#define LED_ON 0;
#define LED_OFF 1;

    int16_t green_led_state[2], red_led_state[2];

    if (Axis[a].Drive.State == enable)
    {
        green_led_state[a] = LED_ON;
        red_led_state[a] = LED_OFF;
    }

    else if(Axis[a].Drive.State == disable)
    {
        if (Led_CNTR[a]<3000)
        {
            green_led_state[a] = LED_OFF;
            red_led_state[a] = LED_OFF;
        }
        else if (Led_CNTR[a]>3000)
        {
            green_led_state[a] = LED_ON;
            red_led_state[a] = LED_OFF;
        }
    }

    else if(Axis[a].Drive.State == fault)
    {
        if(Axis[a].Drive.Faults == no_faults)
        {
            if (Led_CNTR[a]<3000)
            {
                green_led_state[a] = LED_OFF;
                red_led_state[a] = LED_OFF;
            }
            else if (Led_CNTR[a]>3000)
            {
                green_led_state[a] = LED_OFF;
                red_led_state[a] = LED_ON;
            }
        }
        else
        {
            green_led_state[a] = LED_OFF;
            red_led_state[a] = LED_ON;
        }
    }

    SetRedLED(a,red_led_state[a]);
    SetGreenLED(a,green_led_state[a]);



}
