

#include "GlobalIncludes.h"

int16_t SCIB_TX_BufferInPointer=0;
volatile int16_t SCIB_TX_BufferPacketLenght=0;
volatile int16_t SCIB_TX_BufferLevel=0;
int16_t SCIB_TX_BufferOutPointer=0;


int16_t SCIB_SDO_TX_BufferLevel=0;
int16_t SCIB_SDO_TX_BufferOutPointer=0;
int16_t SCIB_SDO_TX_Transmission_Started=0;
int16_t SCIB_PDO_TX_Transmission_Ready=0;
int16_t SCIB_SDO_TX_Transmission_Ready=0;



int16_t SCIB_RX_BufferInPointer=0;
int16_t SCIB_RX_BufferLevel=0;
int16_t SCIB_RX_BufferOutPointer=0;


extern char SerCommandBufferA[SERIAL_BUFFER_SIZE];
extern char SerCommandBufferOut[SERIAL_BUFFER_SIZE];
char SCIB_BufferIn[SERIAL_BUFFER_SIZE];
char SerCommandBufferB[SERIAL_BUFFER_SIZE];
char SCIB_PDO_BufferOut[SERIAL_BUFFER_SIZE];
char SCIB_SDO_BufferOut[SERIAL_BUFFER_SIZE];
char SCIB_BufferOut[SERIAL_BUFFER_SIZE];

int16_t SDOBufferOutPtr =0;
int16_t SDOBufferOutLVL =0;

int32_t ParserStartTime=0;
int32_t ParserPeriod=0;

int32_t MaxParserPeriod=0;


int32_t PDOStartTime=0;
int32_t PDOPeriod=0;

int32_t MaxPDOPeriod=0;

extern CMD_Src_tag CMD_Src;

int16_t  	PDOCmdFlagSCIB=0;
int16_t     SDOCmdFlagSCIB=0;
int16_t SDOCmdLength=0;
int16_t     PDOFbkFlagSCIB=0;


#if TI_DSP_Pragma==1

#pragma CODE_SECTION(readSCIB, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(readSCIB);

#pragma FUNC_ALWAYS_INLINE(writeSCIB);
#pragma CODE_SECTION(writeSCIB, "ramfuncs");

#pragma CODE_SECTION(HandleSCIB, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(HandleSCIB);

#pragma FUNC_ALWAYS_INLINE(PrintChar);

#pragma CODE_SECTION(PrintChar, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(PrintStr);

#pragma CODE_SECTION(PrintStr, "ramfuncs");
#endif


int16_t SCIBTEST=0;

#define SCI_BUFFER_SIZE 320
int16_t receivedCharSCIB[SCI_BUFFER_SIZE+1];
int16_t receivedCharSCIBError[SCI_BUFFER_SIZE+1];
int16_t receivedCharSCIBLevel=0;

int16_t CorrectPackets =0;
int16_t packetError =0;
volatile int16_t BufferOF =0;

volatile int16_t BufferOF1 =0,BufferOF1Lvl=0;

int16_t SCIB_Buffer_Head=0;
int16_t SCIB_Buffer_Tail=0;
int16_t NextCharFromSCIB=0;
// INT9.3


#if TI_DSP_Pragma
__interrupt
#endif
void readSCIB(void)
{
    ClearSCIB_ERRORS();

    EnableInterupts(0);

    while((GetSCIB_RX_Buffer_LVL() )&&(receivedCharSCIBLevel<SCI_BUFFER_SIZE))
    {

        receivedCharSCIB[SCIB_Buffer_Head] = GetSCIB_RX_Buffer();
        receivedCharSCIBError[SCIB_Buffer_Head] = receivedCharSCIB[SCIB_Buffer_Head]>>14;
        receivedCharSCIB[SCIB_Buffer_Head] = receivedCharSCIB[SCIB_Buffer_Head] & 0xff;

        SCIB_Buffer_Head++;
        receivedCharSCIBLevel++;

        if(SCIB_Buffer_Head>=SCI_BUFFER_SIZE)       SCIB_Buffer_Head-=SCI_BUFFER_SIZE;
    }



    //
    AcknowledgeSCIB_Interupt();
}


void writeSCIB(void)
{
    int16_t TX_FIFO_FREE=0;

    if((SCIB_TX_BufferLevel>0)&&(SCIB_SDO_TX_Transmission_Started==0)&& SCIB_PDO_TX_Transmission_Ready)
    {
        TX_FIFO_FREE=   HWD_SERIAL_BUFFER_SIZE- GetSCIB_TX_Buffer_LVL();
        while ((TX_FIFO_FREE>0) && (SCIB_TX_BufferLevel>0))
        {
            SetSCIB_TX_Buffer(SCIB_PDO_BufferOut[SCIB_TX_BufferOutPointer]);
            TX_FIFO_FREE--;
            SCIB_TX_BufferOutPointer++;

            if( SCIB_TX_BufferOutPointer >SERIAL_BUFFER_SIZE) SCIB_TX_BufferOutPointer -=SERIAL_BUFFER_SIZE;
            SCIB_TX_BufferLevel--;
            //  if(SCIB_TX_BufferLevel==0)             ScibRegs.SCITXBUF = 0xa;

        }
    }
    else
    {
        if((SCIB_SDO_TX_BufferLevel>0)&& SCIB_SDO_TX_Transmission_Ready)
        {

            SCIB_SDO_TX_Transmission_Started=1;
            TX_FIFO_FREE=   HWD_SERIAL_BUFFER_SIZE- GetSCIB_TX_Buffer_LVL();
            while ((TX_FIFO_FREE>0) && (SCIB_SDO_TX_BufferLevel>0))
            {
                SetSCIB_TX_Buffer( SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferOutPointer]);

                SCIB_SDO_TX_BufferOutPointer++;
                TX_FIFO_FREE--;

                if( SCIB_SDO_TX_BufferOutPointer >SERIAL_BUFFER_SIZE) SCIB_SDO_TX_BufferOutPointer -=SERIAL_BUFFER_SIZE;
                SCIB_SDO_TX_BufferLevel--;
                //  if(SCIB_TX_BufferLevel==0)             ScibRegs.SCITXBUF = 0xa;

            }
        }
        else
        {
            if(SCIB_SDO_TX_BufferLevel==0)
            {
                SCIB_SDO_TX_Transmission_Ready=0;
                SCIB_SDO_TX_Transmission_Started=0;
            }

        }
    }

}

void HandleSCIB(void)
{
    int16_t prevCharFromSCIB=0;
    int16_t lengthMSB_add, lengthLSB_add, checksum_add;
    int16_t i;
    int16_t j=0;
    int16_t length;
    uint16_t CalculatedChecksum;
    int16_t shadowreceivedCharSCIBLevel = receivedCharSCIBLevel;
    receivedCharSCIBLevel-=shadowreceivedCharSCIBLevel;
    while ( shadowreceivedCharSCIBLevel>0)
    {
        prevCharFromSCIB=NextCharFromSCIB;
        NextCharFromSCIB = receivedCharSCIB[SCIB_Buffer_Tail];
        shadowreceivedCharSCIBLevel--;
        if(shadowreceivedCharSCIBLevel<0) shadowreceivedCharSCIBLevel=0;
        SCIB_Buffer_Tail++;
        if(SCIB_Buffer_Tail>=SCI_BUFFER_SIZE) SCIB_Buffer_Tail-=SCI_BUFFER_SIZE;


        if(((((NextCharFromSCIB == 0XB6u) && (prevCharFromSCIB==0xA5u)) )) && (PDOCmdFlagSCIB==0) && (SCIB_RX_BufferInPointer>=2))//file end received and previous message is not being processed
        {

            lengthMSB_add= SCIB_RX_BufferInPointer-2;
            if(lengthMSB_add>SERIAL_BUFFER_SIZE)
            {
                lengthMSB_add-=SERIAL_BUFFER_SIZE;
            }
            if(lengthMSB_add<0)
            {
                lengthMSB_add+=SERIAL_BUFFER_SIZE;
            }

            lengthLSB_add= SCIB_RX_BufferInPointer-3;
            if(lengthLSB_add>SERIAL_BUFFER_SIZE)
            {
                lengthLSB_add-=SERIAL_BUFFER_SIZE;
            }
            if(lengthLSB_add<0)
            {
                lengthLSB_add+=SERIAL_BUFFER_SIZE;
            }

            checksum_add= SCIB_RX_BufferInPointer-4;
            if(checksum_add>SERIAL_BUFFER_SIZE)
            {
                checksum_add-=SERIAL_BUFFER_SIZE;
            }
            if(checksum_add<0)
            {
                checksum_add+=SERIAL_BUFFER_SIZE;
            }


            length = ((SCIB_BufferIn[ lengthLSB_add] <<8) +(SCIB_BufferIn[lengthMSB_add]&0xff));

            if((SCIB_RX_BufferLevel>=length )&& (length>1) && (length<SERIAL_BUFFER_SIZE) ) // if enough packets received
            {
                //test checksum
                CalculatedChecksum=0;
                j=SCIB_RX_BufferInPointer-5;
                if(j<0) j += SERIAL_BUFFER_SIZE;

                for (i=0;i<length;i++)
                {
                    SerCommandBufferB[length-i-1] = SCIB_BufferIn[j];
                    CalculatedChecksum += SCIB_BufferIn[j];
                    j--;
                    if(j<0) j += SERIAL_BUFFER_SIZE;
                }
                CalculatedChecksum++;
                if( SCIB_BufferIn[checksum_add] ==(CalculatedChecksum&0xff) )// if checksum is corect
                {


                    if(SerCommandBufferB[0] == MSG_TYPE_PDO)
                    {
                        PDOPeriod = mSecCounter - PDOStartTime;
                        if (MaxPDOPeriod<PDOPeriod)MaxPDOPeriod=PDOPeriod;
                        PDOStartTime = mSecCounter;
                        PDOCmdFlagSCIB++; //set

                        WatchDog=WatchDogTime;

                        WatchDogFlag=1;

                    }
                    if(SerCommandBufferB[0] == MSG_TYPE_SDO)
                    {
                        SDOCmdFlagSCIB++; //set
                        SDOCmdLength= length-1;

                    }
                    SCIB_RX_BufferLevel=0;
                    SCIB_RX_BufferInPointer=0;
                    CorrectPackets++;

                }

            }
        }

        SCIB_BufferIn[ SCIB_RX_BufferInPointer]= NextCharFromSCIB;
        SCIB_RX_BufferInPointer++;
        if( SCIB_RX_BufferInPointer >= SERIAL_BUFFER_SIZE) SCIB_RX_BufferInPointer -=SERIAL_BUFFER_SIZE;
        SCIB_RX_BufferLevel++;
        if( SCIB_RX_BufferLevel >= SERIAL_BUFFER_SIZE) SCIB_RX_BufferLevel =SERIAL_BUFFER_SIZE;

    }

}




int SDO_TEST3;
void PrintCharSDO(char character)
{
    if(CMD_Src == SDO)
    {

        SCIB_BufferOut[SDOBufferOutPtr++] = character;
        if(SDOBufferOutPtr>=SERIAL_BUFFER_SIZE) SDOBufferOutPtr -= SERIAL_BUFFER_SIZE;
        SDOBufferOutLVL++;
    }
}


