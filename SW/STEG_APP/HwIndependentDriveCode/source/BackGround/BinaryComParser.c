#include "GlobalIncludes.h"

int32_t ParserEndTime=0;
int32_t ParserTime=0, 	LongestParserTime=0;

PDO_Dwn_Packet_Tag      PDO_Dwn_Packet;
PDO_Dwn_Packet_Tag      PrevPDO_Dwn_Packet;

PDO_Up_Packet_Tag       PDO_Up_Packet;

int32_t interpolationCTRCaptured=0;


void ParseSDO (void)
{


    int16_t i;


    if(SDOCmdFlagSCIB)//parser recieved data
    {
        for (i = 0; i<(SDOCmdLength);i++)
        {
            SerCommandBufferA[i]=SerCommandBufferB[i+1];
        }
        SerCommandBufferA[i] ='\0';
        //printf("%s",SerCommandBufferA);
        SCIA_BufferInPointer = 0;
        NewCmdFlagSCIA = 1;
        CMD_Src = SDO;

        SDOCmdFlagSCIB=0;

    }
}

void ParsePDO (void)
{
    static int16_t  firstPDO=1;

    int32_t Temp_long=0;

    int16_t i;
    int16_t calculatedChechsum=0;
    int16_t PacketLength=0;
    if(PDOCmdFlagSCIB)//parser recieved data
    {
        ParserPeriod = mSecCounter - ParserStartTime;
        if (MaxParserPeriod<ParserPeriod)MaxParserPeriod=ParserPeriod;
        ParserStartTime = mSecCounter;
        for (i=0;i<PDO_DOWN_MSG_SIZE/4;i++)
        {
            //read input data
            PrevPDO_Dwn_Packet.Word[i]= PDO_Dwn_Packet.Word[i];
            PDO_Dwn_Packet.Word[i] = SerCommandBufferB[4*i]
                                                       +((uint32_t)SerCommandBufferB[4*i+1]<<8)
                                                       +((uint32_t)SerCommandBufferB[4*i+2]<<16)

                                                       +((uint32_t)SerCommandBufferB[4*i+3]<<24);

        }
        if(firstPDO==1)
        {
            firstPDO=0;
            return;
        }

        if(PDO_Dwn_Packet.Bits.DriveControlEn && (!PrevPDO_Dwn_Packet.Bits.DriveControlEn))
        {
            Axis[0].Drive.State = enable;
        }
        if((PDO_Dwn_Packet.Bits.DriveControlEn==0) && (Axis[0].Drive.State==enable) )     Axis[0].Drive.State =disable;




        if((PDO_Dwn_Packet.Bits.DriveControlFltClr!=0)|| (PDO_Dwn_Packet.Bits.DriveControlFltHstClr!=0))
        {
            Axis[0].Drive.Faults &= ~PDO_Dwn_Packet.Bits.DriveControlFltClr;
            Axis[0].Drive.Faults_history &= ~PDO_Dwn_Packet.Bits.DriveControlFltHstClr;

            EnableInterupts(0);


            FaultHandler_func(0);
            EnableInterupts(1);

        }


        if(PDO_Dwn_Packet.Bits.DriveControlWrnClr!=0)
        {
            Axis[0].Drive.Warnings &= ~PDO_Dwn_Packet.Bits.DriveControlWrnClr;
            EnableInterupts(0);


            WarningHandler_func(0);
            EnableInterupts(1);

        }




        if( Axis[0].Drive.State != enable)   Axis[0].Drive.Opmode = (Opmode_tag)PDO_Dwn_Packet.Bits.Opmode;

        if(( PDO_Dwn_Packet.Bits.SetCmd&2 )&& ((PrevPDO_Dwn_Packet.Bits.SetCmd&2)==0))
        {

            //   printf("move abs %ld\n",(int32_t)PDO_Dwn_Packet.Bits.MoveAbs);

            Temp_long= Axis[0].Ptp.EndCommand-(int32_t)PDO_Dwn_Packet.Bits.MoveAbs;
            OverFlowPosDerivOnDeg(&Temp_long);

            Axis[0].Ptp.Command += Temp_long;

            OverFlowPosOnDeg(&Axis[0].Ptp.Command);


            //     Axis[1].Ptp.State = PTP_INIT;
        }

        if( (PDO_Dwn_Packet.Bits.SetCmd&1) && ((PrevPDO_Dwn_Packet.Bits.SetCmd&1)==0))
        {
            Temp_long= Axis[0].Ptp.EndCommand-(int32_t)Axis[0].Feedback.IntegratedDegPosition-PDO_Dwn_Packet.Bits.MoveInc;
            OverFlowPosDerivOnDeg(&Temp_long);

            Axis[0].Ptp.Command += Temp_long;           // printf("move inc %ld \n",(int32_t)PDO_Dwn_Packet.Bits.Ax2MoveInc);

            //printf("move inc %ld \n",(int32_t)PDO_Dwn_Packet.Bits.MoveInc);

            OverFlowPosOnDeg(&Axis[0].Ptp.Command);


            //     Axis[1].Ptp.State = PTP_INIT;
        }

        PDO_Up_Packet.Bits.CMD_Acknowledge = PDO_Dwn_Packet.Bits.SetCmd;
        Axis[0].Serial.Jcmd = (int32_t)PDO_Dwn_Packet.Bits.Jog* Axis[0].Serial.ExternCmdScale;
        //  Axis[0].Position.Plim =  (int16_t)PDO_Dwn_Packet.Bits.Plim;
        if(AbsEncFirstReadingRecievedFlag[0])
        {
            if((int16_t)PDO_Dwn_Packet.Bits.PlimMax==-1)
                Axis[0].Position.PlimMax =-1;
            else
                Axis[0].Position.PlimMax =(int32_t) PDO_Dwn_Packet.Bits.PlimMax*10000 ;
            if((int16_t)PDO_Dwn_Packet.Bits.PlimMin==-1)
                Axis[0].Position.PlimMin =-1;
            else
                Axis[0].Position.PlimMin = (int32_t)PDO_Dwn_Packet.Bits.PlimMin*10000;
        }
        else
        {
            Axis[0].Position.PlimMax=-1;
            Axis[0].Position.PlimMin =-1;
        }

        //axis 2
        if(PDO_Dwn_Packet.Bits.Ax2DriveControlEn && (!PrevPDO_Dwn_Packet.Bits.Ax2DriveControlEn))
        {
            Axis[1].Drive.State = enable;
        }
        if((PDO_Dwn_Packet.Bits.Ax2DriveControlEn==0) && (Axis[1].Drive.State==enable) )     Axis[1].Drive.State =disable;




        if((PDO_Dwn_Packet.Bits.Ax2DriveControlFltClr!=0)|| (PDO_Dwn_Packet.Bits.Ax2DriveControlFltHstClr!=0))
        {
            Axis[1].Drive.Faults &= ~PDO_Dwn_Packet.Bits.Ax2DriveControlFltClr;
            Axis[1].Drive.Faults_history &= ~PDO_Dwn_Packet.Bits.Ax2DriveControlFltHstClr;

            EnableInterupts(0);


            FaultHandler_func(1);
            EnableInterupts(1);


        }


        if(PDO_Dwn_Packet.Bits.Ax2DriveControlWrnClr!=0)
        {
            Axis[1].Drive.Warnings &= ~PDO_Dwn_Packet.Bits.Ax2DriveControlWrnClr;
            EnableInterupts(0);


            WarningHandler_func(1);
            EnableInterupts(1);

        }
        if((( Axis[0].Drive.Warnings & GYRO_warning )==0)&& (( Axis[1].Drive.Warnings & GYRO_warning )==0))
        {
            Stabilization.GyroStabilization = PDO_Dwn_Packet.Bits.GyroEn;
        }
        else
        {
            Stabilization.GyroStabilization = 0;
        }



        if( Axis[1].Drive.State != enable)   Axis[1].Drive.Opmode = (Opmode_tag)PDO_Dwn_Packet.Bits.Ax2Opmode;


        if( (PDO_Dwn_Packet.Bits.Ax2SetCmd&2 )&& ((PrevPDO_Dwn_Packet.Bits.Ax2SetCmd&2)==0))
        {

            // printf("move abs %ld\n",(int32_t)PDO_Dwn_Packet.Bits.Ax2MoveAbs);

            Temp_long= Axis[1].Ptp.EndCommand-(int32_t)PDO_Dwn_Packet.Bits.Ax2MoveAbs;
            OverFlowPosDerivOnDeg(&Temp_long);

            Axis[1].Ptp.Command += Temp_long;

            OverFlowPosOnDeg(&Axis[1].Ptp.Command);


            //     Axis[1].Ptp.State = PTP_INIT;
        }

        if( (PDO_Dwn_Packet.Bits.Ax2SetCmd&1) && ((PrevPDO_Dwn_Packet.Bits.Ax2SetCmd&1)==0))
        {
            Temp_long= Axis[1].Ptp.EndCommand-(int32_t)Axis[1].Feedback.IntegratedDegPosition-PDO_Dwn_Packet.Bits.Ax2MoveInc;
            OverFlowPosDerivOnDeg(&Temp_long);

            Axis[1].Ptp.Command += Temp_long;           // printf("move inc %ld \n",(int32_t)PDO_Dwn_Packet.Bits.Ax2MoveInc);

            OverFlowPosOnDeg(&Axis[1].Ptp.Command);


            //     Axis[1].Ptp.State = PTP_INIT;
        }

        PDO_Up_Packet.Bits.Ax2CMD_Acknowledge = PDO_Dwn_Packet.Bits.Ax2SetCmd;


        Axis[1].Serial.Jcmd = (int32_t)PDO_Dwn_Packet.Bits.Ax2Jog*Axis[1].Serial.ExternCmdScale;
        Axis[1].Position.Plim =  (int16_t)PDO_Dwn_Packet.Bits.Ax2Plim;


        if(AbsEncFirstReadingRecievedFlag[1])
        {
            if((int16_t)PDO_Dwn_Packet.Bits.Ax2PlimMax==-1)
                Axis[1].Position.PlimMax =-1;
            else
                Axis[1].Position.PlimMax = (int32_t)PDO_Dwn_Packet.Bits.Ax2PlimMax*10000;
            if((int16_t)PDO_Dwn_Packet.Bits.Ax2PlimMin==-1)
                Axis[1].Position.PlimMin =-1;
            else
                Axis[1].Position.PlimMin = (int32_t)PDO_Dwn_Packet.Bits.Ax2PlimMin*10000;


        }
        else
        {
            Axis[1].Position.PlimMax=-1;
            Axis[1].Position.PlimMin =-1;
        }
        Axis[0].Position.FOV = (int32_t)PDO_Dwn_Packet.Bits.FOV_Asimuth*50;
        Axis[1].Position.FOV = (int32_t)PDO_Dwn_Packet.Bits.FOV_Elevation*50;

        PDOCmdFlagSCIB--;
        PDOFbkFlagSCIB++;
        ParserEndTime=mSecCounter;
        ParserTime= ParserEndTime-ParserStartTime ;
        if( LongestParserTime <=ParserTime)LongestParserTime =ParserTime;


    }

    if((PDOFbkFlagSCIB>0) && (SCIB_TX_BufferLevel==0)) //prepare transmit data
    {
        PDOFbkFlagSCIB=0;
        SCIB_TX_BufferInPointer=0;
        SCIB_TX_BufferOutPointer=0;
        // global
        PDO_Up_Packet.Bits.MsgType=MSG_TYPE_PDO;

        PDO_Up_Packet.Bits.Runtime=Runtime;
        PDO_Up_Packet.Bits.CPLD_Status = CPLD_Status;
        PDO_Up_Packet.Bits.GyroYaw= Stabilization.BaseGyro.Angle.yaw;
        PDO_Up_Packet.Bits.GyroPitch= Stabilization.BaseGyro.Angle.pitch;
        PDO_Up_Packet.Bits.GyroRoll= Stabilization.BaseGyro.Angle.roll;

        PDO_Up_Packet.Bits.VBus = Axis[0].Drive.VBus;
        PDO_Up_Packet.Bits.VLogic = Axis[0].Drive.VLogic;
        PDO_Up_Packet.Bits.Drive_PDO_Up_GBL_reserved1=0;

        PDO_Up_Packet.Bits.Drive_PDO_Up_GBL_reserved4=0;

        //ax1
        PDO_Up_Packet.Bits.Current = Axis[0].Current.I;
        PDO_Up_Packet.Bits.CurrentCommand= Axis[0].Current.Command;
        PDO_Up_Packet.Bits.DriveState = Axis[0].Drive.State;
        PDO_Up_Packet.Bits.Faults = Axis[0].Drive.Faults;
        PDO_Up_Packet.Bits.Warnings = Axis[0].Drive.Warnings;

        PDO_Up_Packet.Bits.FaultsHst = Axis[0].Drive.Faults_history;
        PDO_Up_Packet.Bits.GyroEn = ((Stabilization.GyroStabilization == 1 )&& (Stabilization.InMotionCounter>0));
        PDO_Up_Packet.Bits.GyroPfb = Axis[0].Feedback.BaseGyroPos    ;
        PDO_Up_Packet.Bits.SystemCommand = Axis[0].Position.SystemCommand;
        PDO_Up_Packet.Bits.LoadPfb = Axis[0].Feedback.Load.DegPosition ;
        PDO_Up_Packet.Bits.LoadVfb= Axis[0].Velocity.LoadEncVfb;
        PDO_Up_Packet.Bits.IntegratedDegPosition= Axis[0].Feedback.IntegratedDegPosition;
        PDO_Up_Packet.Bits.MotorPfb = Axis[0].Feedback.Motor.DegPosition;
        PDO_Up_Packet.Bits.MotorVfb =  Axis[0].Velocity.MotorEncVfb;
        PDO_Up_Packet.Bits.GyroVfb= Axis[0].Feedback.GyroVelocty;
        PDO_Up_Packet.Bits.PositionError = Axis[0].Position.Pe;
        PDO_Up_Packet.Bits.Temperature =  Axis[0].Drive.DriveTemp_sense;
        PDO_Up_Packet.Bits.Vcmd = Axis[0].Velocity.Command;

        PDO_Up_Packet.Bits.Drive_PDO_Up_AX1_reserved10=0;
        PDO_Up_Packet.Bits.Drive_PDO_Up_AX1_reserved11=0;


        // ax2
        PDO_Up_Packet.Bits.Ax2Current = Axis[1].Current.I;
        PDO_Up_Packet.Bits.Ax2CurrentCommand= Axis[1].Current.Command;
        PDO_Up_Packet.Bits.Ax2DriveState = Axis[1].Drive.State;
        PDO_Up_Packet.Bits.Ax2Warnings = Axis[1].Drive.Warnings;
        PDO_Up_Packet.Bits.Ax2Faults = Axis[1].Drive.Faults;
        PDO_Up_Packet.Bits.Ax2FaultsHst = Axis[1].Drive.Faults_history;
        PDO_Up_Packet.Bits.Ax2GyroEn = ((Stabilization.GyroStabilization == 1 )&& (Stabilization.InMotionCounter>0));
        PDO_Up_Packet.Bits.Ax2GyroPfb = Axis[1].Feedback.BaseGyroPos ;
        PDO_Up_Packet.Bits.Ax2SystemCommand = Axis[1].Position.SystemCommand;
        PDO_Up_Packet.Bits.Ax2LoadPfb = Axis[1].Feedback.Load.DegPosition ;
        PDO_Up_Packet.Bits.Ax2LoadVfb= Axis[1].Velocity.LoadEncVfb;
        PDO_Up_Packet.Bits.Ax2IntegratedDegPosition= Axis[1].Feedback.IntegratedDegPosition;
        PDO_Up_Packet.Bits.Ax2MotorPfb = Axis[1].Feedback.Motor.DegPosition;
        PDO_Up_Packet.Bits.Ax2MotorVfb =  Axis[1].Velocity.MotorEncVfb;
        PDO_Up_Packet.Bits.Ax2GyroVfb= Axis[1].Feedback.GyroVelocty;
        PDO_Up_Packet.Bits.Ax2PositionError = Axis[1].Position.Pe;
        PDO_Up_Packet.Bits.Ax2Temperature = Axis[1].Drive.DriveTemp_sense;
        PDO_Up_Packet.Bits.Ax2Vcmd = Axis[1].Velocity.Command;
        PDO_Up_Packet.Bits.Drive_PDO_Up_AX2_reserved10=0;
        PDO_Up_Packet.Bits.Drive_PDO_Up_AX2_reserved11=0;


        SCIB_PDO_TX_Transmission_Ready =0;
        PacketLength=0;
        for (i=0;i<PDO_UP_MSG_SIZE/4;i++)
        {
            //  write output data
            /*
            PDO_Up_Packet.Word[i]= (uint32_t)4*i;
            PDO_Up_Packet.Word[i]+=(uint32_t)(4*i+1)<<8;
            PDO_Up_Packet.Word[i]+=(uint32_t)(4*i+2)<<16;
            PDO_Up_Packet.Word[i]+=(uint32_t)(4*i+3)<<24;
             */



            PDO_Up_Packet.Bits.MsgType=MSG_TYPE_PDO;

            SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer] = PDO_Up_Packet.Word[i] & 0xff;
            calculatedChechsum+=SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer];
            SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+1] = (PDO_Up_Packet.Word[i]>>8) & 0xff;
            calculatedChechsum+=SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+1];
            SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+2] = (PDO_Up_Packet.Word[i]>>16) & 0xff;
            calculatedChechsum+=SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+2];
            SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+3] = (PDO_Up_Packet.Word[i]>>24) & 0xff;
            calculatedChechsum+=SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer+3];
            SCIB_TX_BufferInPointer+=4;

            SCIB_TX_BufferLevel+=4;
            PacketLength+=4;

        }
        SCIB_TX_BufferPacketLenght = PacketLength;
        calculatedChechsum++;


        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= calculatedChechsum& 0xff;
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;

        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= (SCIB_TX_BufferPacketLenght>>8) & 0xff;
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;

        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= SCIB_TX_BufferPacketLenght & 0xff;
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;

        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= 0xA5;//file end indication
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;

        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= 0xB6;//file end indication
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;

        SCIB_PDO_BufferOut[SCIB_TX_BufferInPointer]= 0xa;//file end indication
        SCIB_TX_BufferLevel++;
        SCIB_TX_BufferInPointer++;
        //  if(SCIB_TX_BufferLevel==0)             ScibRegs.SCITXBUF = 0xa;

        SCIB_PDO_TX_Transmission_Ready =1;

    }

}

void SendSDO (void)
{


    int16_t i;

    int16_t calculatedChechsum=0;
    SCIB_SDO_TX_BufferOutPointer=0;
    SCIB_SDO_TX_BufferLevel=0;
    SCIB_SDO_BufferOut[0]=MSG_TYPE_SDO;
    calculatedChechsum=SCIB_SDO_BufferOut[0];
    SCIB_SDO_TX_BufferLevel++;
    for (i=1;i<SDOBufferOutLVL;i++)
    {
        SCIB_SDO_BufferOut[i] = SCIB_BufferOut[i];
        calculatedChechsum+=SCIB_SDO_BufferOut[i];
        SCIB_SDO_TX_BufferLevel++;
    }
    SDOBufferOutPtr=0;
    SDOBufferOutLVL=0;
    SCIB_TX_BufferPacketLenght = SCIB_SDO_TX_BufferLevel;
    calculatedChechsum++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= calculatedChechsum& 0xff;
    SCIB_SDO_TX_BufferLevel++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= (SCIB_TX_BufferPacketLenght>>8) & 0xff;
    SCIB_SDO_TX_BufferLevel++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= SCIB_TX_BufferPacketLenght & 0xff;
    SCIB_SDO_TX_BufferLevel++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= 0xA5;//file end indication
    SCIB_SDO_TX_BufferLevel++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= 0xB6;//file end indication
    SCIB_SDO_TX_BufferLevel++;
    SCIB_SDO_BufferOut[SCIB_SDO_TX_BufferLevel]= 0xa;//file end indication
    //  if(SCIB_SDO_TX_BufferLevel==0)             ScibRegs.SCITXBUF = 0xa;

     SCIB_SDO_TX_Transmission_Ready=1;



}
int SDO_TEST=0;
int SDO_TEST1=0;
int SDO_TEST2=0;
