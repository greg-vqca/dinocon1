#include "GlobalIncludes.h"
#define BOOT_RSVD_RAM ((int32_t*)0x10)
int32_t miCommand[NumberOfAxes];
void (*cmdFunc)(void);
int16_t argc=0;
char* argv[100];
int32_t argv_long[15];

int16_t Axis2SelectFlag =0;
float VectorRoll;
float VectorPitch;
float VectorYaw;
int32_t VectorRoll_longShadow;
int32_t VectorPitch_longShadow;
int32_t VectorYaw_longShadow;
int32_t VectorRoll_long;
int32_t VectorPitch_long;
int32_t VectorYaw_long;

void ParseCMD (void)
{


    char* SerCommandBufferParser;
    int16_t paramIndex;
    int16_t functionIndex;
    int64_t param=0;





    uint16_t i;



    if(NewCmdFlagSCIA)
    {
        SerCommandBufferParser=SerCommandBufferA ;
        NewCmdFlagSCIA=0;
    }
    else return;

    argc=0;
    argv[argc]=strtok(SerCommandBufferParser," \r\n");
    if(argv[argc] == 0)
    {

        PrintStr( PROMPT);
        PrintStr(NEW_LINE);

        return;
    }
    while(argv[argc] != 0 && argc<15)
    {
        argc++;
        argv[argc]=strtok(NULL," \r\n");
    }
    argc--;



    PrintStr( PROMPT);
    WatchDog=WatchDogTime;

    WatchDogFlag=1;

    //execute functions
    functionIndex =  FindFunction(argv[0]);
    //execute parameters
    paramIndex = FindParam(argv[0]);

    if(functionIndex)
    {

        Axis2SelectFlag=0;
        /* the ampersand is actually optional */
        cmdFunc = (void(*)(void))FuncStruct[functionIndex].FunctionPointer;
        cmdFunc();
        //	PrintStr(PROMPT);

    }
    else if (paramIndex)
    {

        if(argc == 0)//read
        {
            for(i=0;i<ParamStruct[paramIndex].Length;i++)
            {
                param+=(uint32_t)(*(int16_t*)(ParamStruct[paramIndex].ParamPointer+i*2)&0xFFFF)<<(i*16);
            }
            if(ParamStruct[paramIndex].Signed==1)
            {
                if(ParamStruct[paramIndex].Length ==2)
                    PrintSigned((int32_t)param);
                else 	PrintSigned((int16_t)param);

            }

            else
            {
                PrintSigned(((uint32_t) (param>>16) & 0xFFFF));
                PrintSigned(((uint32_t) param & 0xFFFF));

            }

            //	PrintStr(PROMPT);
        }
        else if(argc == 1)
        {
            if(ParamStruct[paramIndex].Signed)		param = atol(argv[1]);
            else 									param = (uint32_t)atol(argv[1]);

            if (param > ParamStruct[paramIndex].Max || param < ParamStruct[paramIndex].Min)
            {
                PrintStr("Value out of bounds\0");

            }
            else
            {
                if(ParamStruct[paramIndex].Length==1) 				*(int16_t*)ParamStruct[paramIndex].ParamPointer=(int16_t)param;
                else if(ParamStruct[paramIndex].Length==2) 			*(int32_t*)(ParamStruct[paramIndex].ParamPointer) = (int32_t)param;
            }

        }
    }
    else if((argv[0][0]=='a') &&(argv[0][1]=='x')&&(argv[0][2]=='2'))
    {
        i=0;
        while(argv[0][i]!='\0' && i<13)
        {
            argv[0][i]=argv[0][i+3];
            i++;
        }
        functionIndex =  FindFunction(argv[0]);
        /* the ampersand is actually optional */
        if(functionIndex)
        {
            Axis2SelectFlag=1;
            cmdFunc = (void(*)(void))FuncStruct[functionIndex].FunctionPointer;

            cmdFunc();
        }
        else
        {
            //execute functions

            PrintStr( "Illegal Command\0");
        }
    }
    else
    {
        //execute functions

        PrintStr( "Illegal Command\0");
    }

    PrintStr(NEW_LINE);


}






int16_t FindParam(char* cmdName)
{
    int16_t i=0;

    while (i < NUMBER_OF_PARAMETERS)
    {
        if (strcmp(ParamStruct[i].Name, cmdName) == 0)
        {
            return i;
        }

        i++;
    }

    return 0;
}



int16_t FindFunction(char* cmdName)
{
    int16_t i=0;

    while (i < NUMBER_OF_FUNCTIONS)
    {
        if (strcmp(FuncStruct[i].Name, cmdName) == 0)
        {
            return i;
        }

        i++;
    }

    return 0;
}






void PrintSigned(int32_t val)
{

	 sprintf(SerCommandBufferOut, "%ld", val);
    PrintStr((char*)SerCommandBufferOut);
}





void DumpEeprom(void)
{


    int16_t i = 0;
    int64_t param;


    for (i=0;i<NUMBER_OF_PARAMETERS;i++)
    {
        if (ParamStruct[i].SavedToEEPROM == 1)
        {
            PrintStr((char*)";");
            PrintStr(ParamStruct[i].Name);
            PrintStr(" ");
            param=0;
            if(ParamStruct[i].Length==1) 				param = *(int16_t*)ParamStruct[i].ParamPointer;
            else if(ParamStruct[i].Length==2) 			param = *(int32_t*)(ParamStruct[i].ParamPointer) ;

            /*	for(j=0;j<ParamStruct[i].Length;j++)
			{
				param+=(int32_t)(*(int16_t*)(ParamStruct[i].ParamPointer+j*2))<<j;
			}*/
            if(ParamStruct[i].Signed==1)
            {
                PrintSigned((int32_t)param);
            }

            else
            {
                PrintSigned(((uint32_t) (param>>16) & 0xFFFF));
                PrintSigned(((uint32_t) param & 0xFFFF));

            }

        }

    }


}








void PrintFaults (Faults_tag fault)
{
    if (fault &	over_current)
    {
        PrintStr( "Over Current fault \0");

    }

    if (fault &	watchdog_flt)
    {
        PrintStr( "Watchdog fault \0");

    }
    if (fault &   under_voltage_flt)
    {
        PrintStr( "undervoltage  fault \0");

    }
    if (fault &   motor_over_temp_flt)
    {
        PrintStr( "Motor over temperature fault \0");

    }


    if (fault &	illegal_halls_flt)
    {
        PrintStr( "Illegal Halls fault \0");

    }


    if (fault &	hw_over_current_flt)
    {
        PrintStr( "Hardware Over Current 10A limit reached \0");

    }
    if (fault &	motor_encoder_flt)
    {
        PrintStr( "motor encoder fault \0");

    }
    if (fault &   position_error_flt)
    {
        PrintStr( "position error fault \0");

    }
    if (fault ==	no_faults)
    {
        PrintStr( "No Faults\0");

    }
}


void PrintWarnings (Warnings_tag warning)
{

    if (warning & PositionError_warning)
    {
        PrintStr( "Position error warning \0");

    }
    if (warning & over_speed_warning)
    {
        PrintStr( "Over Speed warning \0");

    }

    if (warning & over_voltage_warning)
    {
        PrintStr( "Over Voltage warning \0");

    }
    if (warning & i2lim_warning)
    {
        PrintStr( "I2T warning \0");

    }
    if (warning & under_voltage_warning)
    {
        PrintStr( "Under Voltage warning \0");

    }

    if (warning & eeprom_write_warning)
    {
        PrintStr( "EEPROM write warning \0");

    }

    if (warning & illegal_halls_warning)
    {
        PrintStr( "Illegal Halls \0");

    }
    if (warning & eeprom_checksum_warning)
    {
        PrintStr( "EEPROM checksum warning \0");

    }


    if (warning & IPM_over_temp_warning)
    {
        PrintStr( "IPM Over Temperature warning \0");

    }

    if (warning & sci_buff_OF_warning)
    {
        PrintStr( "SCI buffer Overflowed \0");

    }
    if (warning & AbsEncNotInUse_warning)
    {
        PrintStr( "Abs encoder error \0");

    }
    if (warning & CPLD_SPI_warning)
    {
        PrintStr( "CPLD SPI error \0");

    }
    if (warning & CPLD_SPI_CHK_SUM_warning)
    {
        PrintStr( "CPLD SPI Checksum error \0");

    }



    if (warning & eeprom_read_warning)
    {
        PrintStr( "EEPROM read warning \0");

    }


    if (warning & eeprom_checksum_warning)
    {
        PrintStr( "EEPROM checksum warning \0");

    }

    if (warning ==    no_warnings)
    {
        PrintStr( "No Faults\0");

    }
}

int16_t CalculateAxis (void)
{
    int16_t a=0;
    if(argc >1)
    {
        PrintStr("Illegal Parameters\0");
    }
    else if(argc >0)
    {
        a = atoi((const char *)argv[1]);
    }

    if( a>2)
    {
        PrintStr("Illegal Parameters\0");
    }
    else if(a<2)a=0;
    else a=1;

    if(Axis2SelectFlag)
    {
        Axis2SelectFlag=0;
        a=1;
    }

    return a;
}



//"ver"
void printVersion(void)
{
    PrintStr(VER);
    return;
}
//"rectrig"  //mode || channel|| value|| position

void parserRecTrig(void)
{


    if(strcmp(argv[1], "imm")==0)

    {
        if(argc>1)
        {
            PrintStr("Too many parameters\0");
        }
        else
        {
            Record.TrigPosition = 0;
            Record.State = initRAM;
            Record.Trig_Mod =ImmTrigger ;
            return;
        }
    }
    else 	if(strcmp(argv[1], "rise")==0)
    {
        if(argc!=3)
        {
            PrintStr("Incorrect parameters\0");
        }
        else
        {
            //"rectrig"  //mode || channel|| value|| position

            Record.TrigPosition = atoi((const char *)argv[4]);
            Record.Trig_Value = atol((const char *)argv[3]);
            Record.Trig_Channel = atol((const char *)argv[2]);

            Record.State = initRAM;
            Record.Trig_Mod =RisingTrigger ;
            return;
        }

    }
    else 	if(strcmp(argv[1],  "fall")==0)
    {

        if(argc!=3)
        {
            PrintStr("Incorrect parameters\0");
        }
        else
        {
            //"rectrig"  //mode || channel|| value|| position

            Record.TrigPosition = atoi((const char *)argv[4]);
            Record.Trig_Value = atol((const char *)argv[3]);
            Record.Trig_Channel = atol((const char *)argv[2]);

            Record.State = initRAM;
            Record.Trig_Mod =FallingTrigger ;
            return;
        }

    }
    if (Record.Trig_Channel > Record.NumberOfChannels)
    {
        PrintStr("Invalid Channel Number\0");

        return;

    }

}
//"get"
void parserGet(void)
{
    uint16_t i=0,j,w;
    int32_t recorded_data;
    static int32_t recorded_address=0;


    if(Record.Done==0)
    {
        PrintStr("Recording not finished");
    }
    else	if(argc>0)
    {
        PrintStr("Too Many Parameters");
    }
    else
    {
        PrintStr("Recording ");
        PrintStr(": ");
        PrintSigned((int32_t)((Record.NumberOfChannels)));
        PrintStr(" , ");
        PrintSigned((int32_t)((Record.Length)));
        PrintStr(" , ");

        for(i=0;i< Record.NumberOfChannels;i++)
        {
            if(i) PrintStr(" , ");
            PrintStr("\"");
            PrintStr(ParamStruct[Record.Channel_Index[i]].Name);
            PrintStr("\"");
        }
        //	for (j=0;j<(Record.Length * Record.NumberOfChannels);j++)
        //	{
        PrintStr((char*)";");
        recorded_address=0;
        for (w=0 ; w<Record.Length; w++)
        {
            for (i = 0; i < Record.NumberOfChannels; i++)
            {
                recorded_data=0;
                for(j=0;j<(ParamStruct[Record.Channel_Index[i]].Length);j++)
                {

                    recorded_data+=((uint32_t)ReadRamInt(recorded_address)&0xFFFF)<<(j*16);
                    recorded_address++;
                    //recorded_data+=(Record.Memory[(Record.Address - Record.Length)+j+i+a]&0xFFFF)<<(j*16);
                }
                if(ParamStruct[Record.Channel_Index[i]].Signed==1)
                {
                    if(ParamStruct[Record.Channel_Index[i]].Length==2)
                        PrintSigned(recorded_data);
                    else	PrintSigned((int16_t)recorded_data);

                }

                if (i<Record.NumberOfChannels-1)
                {
                    PrintStr(",");
                }
                else
                {
                    PrintStr((char*)";");

                }

            }
        }
        PrintStr( NEW_LINE);

    }


    return;
}
//"mi"
void MoveIncremental(void)
{
    int16_t a=0;

    if(argc == 0)
    {
        PrintStr("Missing Distance\0");
    }
    else if(argc == 1)
    {
        if(Axis2SelectFlag )
        {
            a=2;
            Axis2SelectFlag=0;
            if(a<3)
                Axis[a-1].Ptp.Command -= atol((const char *) argv[1]);
            Axis[a-1].Ptp.State = PTP_INIT;//jump to state that gives motion from current pfb
            miCommand[a-1]= atol((const char *) argv[1]);
        }
        else
        {
            a=0;

            Axis[a].Ptp.Command -= atol((const char *) argv[1]);
            Axis[a].Ptp.State = PTP_INIT;//jump to state that gives motion from current pfb
            miCommand[a]= atol((const char *) argv[1]);

        }
    }
    else if(argc == 2)
    {
        a=atol( (const char *)argv[1]);
        if(a<3)
            Axis[a-1].Ptp.Command -= atol((const char *) argv[2]);
        Axis[a-1].Ptp.State = PTP_INIT;//jump to state that gives motion from current pfb
        miCommand[a-1]= atol((const char *) argv[1]);

    }

    else
    {
        PrintStr("Too many parameters\0");
    }
    return;
}

//"ma"
void MoveAbsolute(void)
{
    int16_t  a;

    if(argc == 0)
    {
        PrintStr("Missing Distance\0");
    }
    else if(argc == 1)
    {
        if(Axis2SelectFlag )
        {
            a=2;
            Axis[a-1].Ptp.Command += Axis[a-1].Position.SystemCommand-atol((const char *) argv[1]);
            Axis[a-1].Ptp.State = PTP_IN_MOTION;
        }
        else
        {
            a=0;
            Axis[a].Ptp.Command += Axis[a].Position.SystemCommand- atol( (const char *)argv[1]);
            Axis[a].Ptp.State = PTP_IN_MOTION;
        }

    }
    else if(argc == 2)
    {
        a=atol((const char *) argv[1]);
        if(a<3)
            Axis[a-1].Ptp.Command += Axis[a-1].Position.SystemCommand-atol((const char *) argv[2]);
        Axis[a-1].Ptp.State = PTP_IN_MOTION;
    }

    else
    {
        PrintStr("Too many parameters\0");
    }

    OverFlowPosOnDeg(& Axis[a-1].Ptp.Command);

    return;


}
//"bothma"
void BothMoveAbs(void)
{


    if(argc <2)
    {
        PrintStr("Missing Distance\0");
    }
    else	 if(argc == 2)
    {

        Axis[0].Ptp.Command += Axis[0].Position.SystemCommand-atol((const char *) argv[2]);
        Axis[0].Ptp.State = PTP_IN_MOTION;

        Axis[1].Ptp.Command += Axis[1].Position.SystemCommand- atol( (const char *)argv[1]);
        Axis[1].Ptp.State = PTP_IN_MOTION;
    }



    else
    {
        PrintStr("Too many parameters\0");
    }

    return;


}


//"record"
void RecordCommand(void)
{

    int16_t channelNumber, recordSmaple, i;
    uint16_t recordLength =0;
    int16_t paramIndex[RECORD_CHANNELS] ;

    if (Record.Done == 0)
    {
        PrintStr("Record in progress");

        return;
    }
    else if (argc==0)
    {
        PrintStr("Record finished");
        return;
    }
    else if (argc<3)
    {
        PrintStr("Missing Parameters");
        return;
    }
    else	if((argc-2)>RECORD_CHANNELS)
    {
        PrintStr("too many recording channels");
        return;
    }
    recordSmaple = atoi((const char *)argv[1])-1;
    recordLength  = atoi((const char *)argv[2]);
    if(recordSmaple==-1 || (int16_t)recordLength ==-1)
    {
        PrintStr("Illegal Parameters");
        return;

    }
    channelNumber = ((argc-2));
    if (channelNumber*recordLength > MAX_RECORD_LENGTH)
    {
        PrintStr("Recording is too int32_t");
        return;
    }

    for (i=0;i<channelNumber;i++)
    {

        paramIndex[i] = FindParam(( char *)argv[3+i]);
        if (paramIndex[i] == 0)
        {
            PrintStr("Invalid Parameter");
            return;
        }
    }

    for (i=0; i<channelNumber; i++)
    {
        Record.Channel_Index[i] = paramIndex[i];
    }

    Record.Sample_Period = recordSmaple;
    Record.NumberOfChannels = channelNumber;
    Record.Length=recordLength;

}

//"recoff"
void StopRecorder(void)
{

    Record.State = finishRecord;
}

//"flt"
void ReadFaults(void)
{
    int16_t a;

    a=CalculateAxis();

    PrintFaults(Axis[a].Drive.Faults);

    return;
}

void ReadWarnings(void)
{
    int16_t a;

    a=CalculateAxis();

    PrintWarnings(Axis[a].Drive.Warnings);

    return;
}
//"bothpfb"
void ReadBothPFB(void)
{


    PrintSigned(Axis[0].Feedback.Load.DegPosition);
    PrintCharSerial(';');
    PrintSigned(Axis[1].Feedback.Load.DegPosition);

    return;
}



//"bothpfb"
void ReadBothENC(void)
{


    PrintSigned(Axis[0].Feedback.Motor.EncPosition);
    PrintCharSerial(';');
    PrintSigned(Axis[1].Feedback.Motor.EncPosition);

    return;
}
//"flthst"
void ReadFaultsHistory(void)
{
    int16_t a;

    a=CalculateAxis();

    PrintFaults(Axis[a].Drive.Faults_history);


}
//"clrflt"
void ClearFaults(void)
{
    int16_t a;

    a=CalculateAxis();

    Axis[a].Drive.Faults =no_faults;

}
//"clrhst"
void ClearFaultsHistory(void)
{
    int16_t a;

    a=CalculateAxis();

    Axis[a].Drive.Faults_history =no_faults;

}

//"stat
void ReadStat(void)
{
    int16_t a;

    a=CalculateAxis();

    PrintSigned(Axis[a].Drive.State);

}

//"save"
void SaveParameters(void)
{

    DoSaveParameters ++;
}

//"dump"
void PrintParametersSavedToEEPROM(void)
{

    DumpEeprom();

}
//"bootloader"
void JumpToBoot(void)
{
    PrintStr((char*)PROMPT);
    //PrintStr( "jumping to boot");
    //PrintStr((int8_t*)PROMPT);

    //	while (SciaRegs.SCIFFTX.bit.TXFFST >0 )
    //	{
    //		uSleep(1);;
    //	}

    *BOOT_RSVD_RAM = 123456789;
    //	ESTOP0;
    //	DINT;
    //	IER = 0x0000;
    //	IFR = 0x0000;
    //	asm(" LB 0x3F7FF6");

    return;
}

//"load"
void LoadSavedParameters(void)
{
    if(SaveFlag)
    {
        PrintStr("Save In Process\0");
        return;
    }

    if (LoadEeprom()!=0)
    {

        //InitParamsArray();
        PrintStr("Reading from EEPROM failed\0");

    }
}

//"en"
void EnablePower(void)
{
    int16_t a;

    a=CalculateAxis();

    Axis[a].Drive.Faults = no_faults;
    Axis[a].Drive.Faults_history = no_faults;
    Axis[a].Drive.State = enable;

}

//"en"
void BothEnablePower(void)
{



    Axis[0].Drive.Faults = no_faults;
    Axis[0].Drive.Faults_history = no_faults;
    Axis[0].Drive.State = enable;
    Axis[1].Drive.Faults = no_faults;
    Axis[1].Drive.Faults_history = no_faults;
    Axis[1].Drive.State = enable;


}

//setpwmfreq
void SetPwmFreq(void)
{


    if(	Axis[0].Drive.State == enable||	Axis[1].Drive.State == enable)
    {
        PrintStr("Drive Enabled please disable to proceed\0");
        return;
    }
    else
    {
        InitPwm(PWM_Freq_scaling);

    }

}
//"k"
void DisablePower(void)
{
    int16_t a;

    a=CalculateAxis();


    Axis[a].Drive.State = disable;


}

//"halls"
void PrintHallsState(void)
{
    int16_t a;

    a=CalculateAxis();

    if(a<NumberOfAxes)
    {
        PrintSigned(GetHallsScensorsReading(a));

    }
    else
    {
        PrintStr("Illegal Parameters\0");
    }

}



void writememory(void)
{
    int16_t a=0,d;



    a=atol( (const char *)argv[1]);

    d =		atol((const char *) argv[2]);
    I2C_Write_EEPROM_Int(a,d);

    return;
}

void readmemory(void)
{
    int16_t a=0,d;



    a=atol((const char *) argv[1]);

    d =		I2C_Read_EEPROM_Int(a);

    PrintSigned(d);

    return;
}
