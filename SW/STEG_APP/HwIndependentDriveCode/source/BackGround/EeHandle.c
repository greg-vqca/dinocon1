
#include "GlobalIncludes.h"
int16_t buffer [(NUMBER_OF_PARAMETERS+1)*2];
int16_t 	SaveFlag=0;
int16_t 	DoSaveParameters=0;

int16_t 	EEPROM_Lenght=0;

void PrepareSaveEeprom(void)
{
    uint16_t i,j,address=0;
    int16_t checkSum=0;
    Eeprom_Write_Fault_Flag=0;
    for (i=0; i<NUMBER_OF_PARAMETERS; i++)
    {

        if (ParamStruct[i].SavedToEEPROM == 1)
        {
            for(j=0;j<ParamStruct[i].Length;j++)
            {
                buffer[address]=(*(int16_t*)(ParamStruct[i].ParamPointer+j*2));
                checkSum = checkSum + (int16_t)(buffer[address]);
                address++;

            }
        }


    }
    checkSum += checkSum >> 8;
    checkSum &= 0xff;
    checkSum++;
    buffer[address] = checkSum;
    EEPROM_Lenght=address;


    SaveFlag=1;
}

void PerformSaveEeprom(void)
{


    if(EEPROM_Delay>0 || (SaveFlag==0)) return;
    EEPROM_Delay= 10;

    I2C_Write_EEPROM_Int(	EEPROM_Lenght*2,	buffer[	EEPROM_Lenght]);
    EEPROM_Lenght--;

    if(	EEPROM_Lenght<0)
    {
        SaveFlag=0;
    }
}


int16_t LoadEeprom(void)
{
    int16_t checkSum = 0;

    uint16_t i, j,address=0;



    for (i=0; i<NUMBER_OF_PARAMETERS; i++)
    {
        if (ParamStruct[i].SavedToEEPROM == 1)
        {
            for(j=0;j<ParamStruct[i].Length;j++)
            {
                buffer[address]= I2C_Read_EEPROM_Int((address*2));
                checkSum = checkSum + (int16_t)(buffer[address]);
                address++;

            }
        }
    }
    checkSum += checkSum >> 8;
    checkSum &= 0xff;
    checkSum++;
    buffer[address] = I2C_Read_EEPROM_Int((address*2));


    if (Axis[0].Drive.Warnings== eeprom_read_warning ||Axis[1].Drive.Warnings== eeprom_read_warning) return -1;



    if (checkSum != buffer[address])
    {
        Axis[0].Drive.Warnings|= eeprom_checksum_warning;
        Axis[1].Drive.Warnings|= eeprom_checksum_warning;
        PrintStr("Checksum error\0");
        return -1;
    }
    address=0;
    for (i=0; i<NUMBER_OF_PARAMETERS; i++)
    {
        if (ParamStruct[i].SavedToEEPROM == 1)
        {
            for(j=0;j<ParamStruct[i].Length;j++)
            {
                (*(int16_t*)(ParamStruct[i].ParamPointer+j*2))=buffer[address];
                address++;
            }
        }
    }

    return 0;
}
