#include "GlobalIncludes.h"



int16_t InitFinishedFlag=0;


void Init (void)
{
    int16_t i;



    uSleep(5000);


    EnableInterupts(0);




    InitVar();



    for(i=0;i<2;i++)
    {

        Axis[i].Drive.Faults =no_faults;
        HWD_DisablePwm(i);
    }

   // EnableInterupts(1);


    InitParamsArray();
    Axis[0].Drive.Faults=no_faults;
    Axis[1].Drive.Faults=no_faults;
    Axis[0].Drive.Warnings=no_warnings;
    Axis[1].Drive.Warnings=no_warnings;

    Axis[0].InitHallsNeededFlag=10;
    Axis[1].InitHallsNeededFlag=10;



  //  LoadEeprom();
    InitGPIO();
    InitEncoders();
    InitPwm(PWM_Freq_scaling);
    EnableInterupts(0);
    SetupInterupts();
    StartInterupts();

     EnableInterupts(1);


     ADC_Offset_Callculation(0,&Axis[0].Current.Ia_OffSet, &Axis[0].Current.Ic_OffSet);
     ADC_Offset_Callculation(1,&Axis[1].Current.Ia_OffSet, &Axis[1].Current.Ic_OffSet);


} 





void InitVar (void)
{
    int16_t i;
    for (i=0 ; i < (SERIAL_BUFFER_SIZE -1); i++)
    {
        SCIA_BufferIn[i]=0;
        SerCommandBufferOut[i]=0;
        SCIB_BufferIn[i]=0;
        SerCommandBufferB[i]=0;

    }
    for (i=0;i<PDO_DOWN_MSG_SIZE/4;i++)
         {
             //read input data
             PrevPDO_Dwn_Packet.Word[i]= 0;
                     PDO_Dwn_Packet.Word[i]=0;
         }
    for (i=0;i<PDO_UP_MSG_SIZE/4;i++)
             {
                 //read input data

        PDO_Up_Packet.Word[i]=0;
             }


    for (i=0 ; i < 2; i++)
    {
        Axis[i].Position.LimitedToSystemDistanceOut=0;
        Axis[i].Motor.BEMF_HallsPositiontemp=0;
        Axis[i].Motor.BEMF_HallsPosition=0;

        Axis[i].Velocity.Command=0;
        Axis[i].Velocity.Filter=0;
        Axis[i].Velocity.LoadEncVfb=0;
        Axis[i].Velocity.MotorEncVfb=0;

        Axis[i].Velocity.FilterFc=0;
        Axis[i].Position.PeInteg=0;

        Axis[i].Drive.Faults=no_faults;
        Axis[i].Drive.Faults_history=no_faults;
        Axis[i].Drive.State = disable;
        Axis[i].Drive.PrevState=disable;

        Axis[i].Current.Command=0;
        Axis[i].Current.I=0;
        Axis[i].Current.Commutation_timer=0;
        Axis[i].Current.ForcedCommutCntr=0;
        Axis[i].Current.ForcedCommutFlag=0;
        Axis[i].Current.Phase_A_output =0;
        Axis[i].Current.Phase_B_output =0;
        Axis[i].Current.Phase_C_output = 0;
        Axis[i].CurrLoopSaturated = 0;
        Axis[i].Motor.Phaseoffset =0;
        Axis[i].Motor.Direction=1;
        Axis[i].Position.JogInPosVelocityOutputFix=0;
        Axis[i].Feedback.Halls.DegPosition=0;
        Axis[i].Feedback.Motor.DegPosition=0;
        Axis[i].Feedback.Load.DegPosition=0;

        Axis[i].Feedback.Halls.Position=0;
        Axis[i].Feedback.Motor.EncPosition=0;
        Axis[i].Feedback.Load.EncPosition=0;

        Axis[i].Feedback.Halls.PositionScaledToDeg =0;
        Axis[i].Feedback.Motor.EncPositionScaledToDeg =0;
        Axis[i].Feedback.Motor.PrevEncPositionScaledToDeg=0;
        Axis[i].Feedback.Halls.PrevPositionScaledToDeg=0;

        Axis[i].InitHallsNeededFlag=10;
        Axis[i].Serial.Icmd=0;
        Axis[i].Serial.Jcmd=0;
        Axis[i].Serial.Vcmd=0;


        Axis[i].Ptp.State = PTP_IDLE;


        Axis[i].Ptp.DistanceLeft_f=0;

        Axis[i].Jog.Vout=0;
        Axis[i].Jog.Output=0;

        Axis[i].Feedback.GyroItegrated=0;

        Axis[i].Current.I2T_64=0;
        Axis[i].Current.I2T_integrator=0;
        Axis[i].Current.I =0;
        Axis[i].Current.SinComponent =0;
        Axis[i].Current.CosComponent =0;
        Axis[i].Feedback.BaseGyroPos  =0;
        Axis[i].Feedback.GyroVeloctyChange =0;

        Axis[i].Feedback.GyroVelocty=0;


        Axis[i].BuidInTest.Results=Noresult;
        Axis[i].BuidInTest.State= IdleBIT;
    }

    for(i=0;i<SPI_TO_CPLD_LENGHT;i++)
    {
       ShadowSPI_RX_DATA[i]=0;
       SPI_RX_DATA[i]=0;
       DMA_rdata[i]=0xFFFF;
    }
    Stabilization.InMotionCounter=0;
    Runtime=0;

    Record.Done =0;
    Record.State = finishRecord;

    Record.Sample_Period =1;
    Record.Length= 100;
    Record.NumberOfChannels=1;
    Record.Trig_Mod = ImmTrigger;



}

void InitParamsArray(void)
{
    int16_t i;


    for (i = 0; i < NUMBER_OF_PARAMETERS; i++)
    {
        if(ParamStruct[i].Length==1) 				*(int16_t*)ParamStruct[i].ParamPointer=(int16_t)ParamStruct[i].Default;
        else if(ParamStruct[i].Length==2) 			*(int32_t*)(ParamStruct[i].ParamPointer) = (int32_t)ParamStruct[i].Default;


    }
}


