

#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(HallsHandlerfunc, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(HallsHandlerfunc);
#pragma CODE_SECTION(InitHalls, "ramfuncs");
#endif
int16_t CurrentHalls[NumberOfAxes]={0,0};
int32_t AveragePhaseoffset[NumberOfAxes]={0,0};
int16_t Phaseoffset[NumberOfAxes]={0,0};
int32_t PhaseoffsetChange[NumberOfAxes]={0,0};


void HallsHandlerfunc(int16_t a)
{
    int32_t halls_deriv=0;

    CurrentHalls[a] =  GetHallsScensorsReading(a);

    Axis[a].Motor.HallsState &= 0x7;

    if((CurrentHalls[a] == 0)||(CurrentHalls[a] == 7))
    {

        if(Axis[a].InitHallsNeededFlag)
        {
 //           Axis[a].Drive.Faults|= illegal_halls_flt;
        }
        Illegal_halls_Flag[a]=1;

    }
    else
    {
        Illegal_halls_Flag[a]=0;
    }

    if (Axis[a].Motor.HallsState != CurrentHalls[a])
    {



        Axis[a].Motor.HallsState=(Axis[a].Motor.HallsState<<4)|CurrentHalls[a];
        switch(	Axis[a].Motor.HallsState)
        {
        case 0x31:
            Axis[a].Motor.HallsPosition=5;
            Axis[a].Motor.Direction=1;
            break;
        case 0x13:
            Axis[a].Motor.HallsPosition=6;
            Axis[a].Motor.Direction=-1;
            break;
        case 0x23:
            Axis[a].Motor.HallsPosition=6;
            Axis[a].Motor.Direction=1;
            break;
        case 0x32:
            Axis[a].Motor.HallsPosition=1;
            Axis[a].Motor.Direction=-1;
            break;
        case 0x62:
            Axis[a].Motor.HallsPosition=1;
            Axis[a].Motor.Direction=1;
            break;
        case 0x26:
            Axis[a].Motor.HallsPosition=2;
            Axis[a].Motor.Direction=-1;
            break;
        case 0x46:
            Axis[a].Motor.HallsPosition=2;
            Axis[a].Motor.Direction=1;
            break;
        case 0x64:
            Axis[a].Motor.HallsPosition=3;
            Axis[a].Motor.Direction=-1;
            break;
        case 0x54:
            Axis[a].Motor.HallsPosition=3;
            Axis[a].Motor.Direction=1;
            break;
        case 0x45:
            Axis[a].Motor.HallsPosition=4;
            Axis[a].Motor.Direction=-1;
            break;
        case 0x15:
            Axis[a].Motor.HallsPosition=4;
            Axis[a].Motor.Direction=1;
            break;
        case 0x51:
            Axis[a].Motor.HallsPosition=5;
            Axis[a].Motor.Direction=-1;
            break;
        default:
            if(Axis[a].InitHallsNeededFlag)
            {
      //          if(Axis[a].Current.ForcedCommutFlag==0)
     //               Axis[a].Drive.Faults|= illegal_halls_flt;
            }
            else
            {
                Axis[a].Drive.Warnings|= illegal_halls_warning;

            }

            break;
        }
        Phaseoffset[a] =(uint16_t)((Axis[a].Motor.HallsPosition) * 10923 +(Axis[a].Motor.Direction*10923>>1) - (Axis[a].Motor.ComutationAngle));

        PhaseoffsetChange[a] = Phaseoffset[a] -  Axis[a].Motor.Phaseoffset ;//(Axis[a].Motor.HallsPosition) * 10923 +Axis[a].Motor.Direction*5462 - (Axis[a].Motor.ComutationAngle);
        if(PhaseoffsetChange[a]> (int32_t) 1<<15) PhaseoffsetChange[a]-= (int32_t)1<<16;
        if(PhaseoffsetChange[a]< (int32_t)-1<<15) PhaseoffsetChange[a]+= (int32_t)1<<16;

        AveragePhaseoffset[a]+=((PhaseoffsetChange[a]<<4)-AveragePhaseoffset[a])>>3;

        Axis[a].Feedback.Halls.Position -=(Axis[a].Motor.Direction);


        if(Axis[a].InitHallsNeededFlag || (Motor_FBK_FailedDelay[a]&& (Halls_FBK_Failed[a]==0))||(labs(PhaseoffsetChange[a])>(10923>>1)))
        {

            Axis[a].Motor.Phaseoffset =Phaseoffset[a];//(Axis[a].Motor.HallsPosition) * 10923 +Axis[a].Motor.Direction*5462 - (Axis[a].Motor.ComutationAngle);
            if(Axis[a].InitHallsNeededFlag>0)Axis[a].InitHallsNeededFlag--;


        }
        else if (Halls_FBK_Failed[a]==0)
        {
            Axis[a].Motor.Phaseoffset +=PhaseoffsetChange[a]>>4;
        }
        Axis[a].Motor.HallsState = CurrentHalls[a];
    }

    Axis[a].Feedback.Halls.PositionScaledToDeg = Axis[a].Feedback.Halls.Position*Axis[a].Feedback.Halls.DegScalingFactor ;
    halls_deriv = Axis[a].Feedback.Halls.PositionScaledToDeg - Axis[a].Feedback.Halls.PrevPositionScaledToDeg;
    Axis[a].Feedback.Halls.PrevPositionScaledToDeg = Axis[a].Feedback.Halls.PositionScaledToDeg ;
    OverFlowPosDerivOnDeg(&halls_deriv);
    Axis[a].Feedback.Halls.DegPosition+=halls_deriv;
    OverFlowPosOnDeg(&Axis[a].Feedback.Halls.DegPosition);


}

void InitHalls(int16_t a)
{
    int16_t temp_direction;
    CurrentHalls[a] =  GetHallsScensorsReading(a);

    switch(	CurrentHalls[a])
    {
    case 1:
        Axis[a].Motor.HallsPosition=5;
        break;
    case 3:
        Axis[a].Motor.HallsPosition=6;
        break;
    case 2:
        Axis[a].Motor.HallsPosition=1;
        break;
    case 6:
        Axis[a].Motor.HallsPosition=2;
        break;
    case 4:
        Axis[a].Motor.HallsPosition=3;
        break;
    case 5:
        Axis[a].Motor.HallsPosition=4;
        break;
    default:
        if(Axis[a].Current.ForcedCommutFlag==0)

   //         Axis[a].Drive.Faults|= illegal_halls_flt;
        break;
    }
    Axis[a].Motor.HallsState=CurrentHalls[a];
    Axis[a].InitHallsNeededFlag=10;

    Axis[a].Motor.ComutationAngle=Axis[a].Feedback.MechPos*(Axis[a].Motor.Poles>>1);

    Axis[a].Motor.Phaseoffset =(Axis[a].Motor.HallsPosition) * 10923 - (Axis[a].Motor.ComutationAngle);
    if ( Axis[a].Current.Command < 0  )
    {
        temp_direction = 0;
    }
    else if ( Axis[a].Current.Command > 0 )
    {
        temp_direction = 0;
    }

    if(1)//ZerroCrossComm)
    {
        Axis[a].Motor.Rotor_pos =	Axis[a].Motor.HallsPosition +temp_direction +Axis[a].Motor.Phase+Axis[a].Motor.Direction	;
    }	if (Axis[a].Motor.Rotor_pos > 6) 	Axis[a].Motor.Rotor_pos -= 6;
    if (Axis[a].Motor.Rotor_pos < 1) 	Axis[a].Motor.Rotor_pos += 6;
}
