#include "GlobalIncludes.h"



uint32_t  Prev_encoderReading[NumberOfAxes]={0,0};


int32_t PrevVectorRoll_long;
int32_t PrevVectorPitch_long;
int32_t ChangeVector[NumberOfAxes];
int32_t ChangeVectorPGFF[NumberOfAxes];

int16_t ElevationEncoder=0;
int32_t  encoder[NumberOfAxes];
int32_t CoarsToSmoothPosDif=0;
#if TI_DSP_Pragma ==1
#pragma CODE_SECTION(EncHandler_func, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(EncHandler_func);
#endif
void EncHandler_func(int16_t a)
{


    int32_t motor_enc_deriv=0,motor_deg_enc_deriv=0;
    encoder[a]= GetIncrementalEncoderCounterValue(a);

    Axis[a].Feedback.MechPos=encoder[a] *Axis[a].Feedback.Motor.PRD_ScalingFactor;

    motor_enc_deriv = (encoder[a] - Prev_encoderReading[a]);
    if(motor_enc_deriv>(int32_t)Axis[a].Feedback.Motor.EncResolution<<1) motor_enc_deriv-=(int32_t)Axis[a].Feedback.Motor.EncResolution<<2;
    if(motor_enc_deriv<-(int32_t)Axis[a].Feedback.Motor.EncResolution<<1) motor_enc_deriv+=(int32_t)Axis[a].Feedback.Motor.EncResolution<<2;
    Prev_encoderReading[a]=    encoder[a];
    Axis[a].Feedback.Motor.EncPosition +=(motor_enc_deriv);
    Axis[a].Feedback.Motor.EncPositionScaledToDeg = Axis[a].Feedback.Motor.EncPosition*Axis[a].Feedback.Motor.DegScalingFactor;
    motor_deg_enc_deriv = Axis[a].Feedback.Motor.EncPositionScaledToDeg - Axis[a].Feedback.Motor.PrevEncPositionScaledToDeg;
    Axis[a].Feedback.Motor.PrevEncPositionScaledToDeg = Axis[a].Feedback.Motor.EncPositionScaledToDeg ;
    OverFlowPosDerivOnDeg(&motor_deg_enc_deriv);
    Axis[a].Feedback.Motor.DegPosition+=motor_deg_enc_deriv;
    OverFlowPosOnDeg(&Axis[a].Feedback.Motor.DegPosition);





    Axis[a].Feedback.Load.DegPosition= Axis[a].Feedback.Load.EncPosition* Axis[a].Feedback.Load.DegScalingFactor;
    OverFlowPosOnDeg(&Axis[a].Feedback.Load.DegPosition);
    Axis[a].Feedback.Load.DegPosition -= Axis[a].Feedback.Load.DegPositionOffset;
    OverFlowPosOnDeg(&Axis[a].Feedback.Load.DegPosition);

    if((AbsEncFirstReadingRecievedFlag[a]==1) && (PrevAbsEncFirstReadingRecievedFlag[a]==0)&&InitFinishedFlag)//first abs enc reading
    {
        Axis[a].Feedback.Motor.DegPosition = Axis[a].Feedback.Load.DegPosition;
        Axis[a].Feedback.Halls.DegPosition = Axis[a].Feedback.Load.DegPosition;
        PrevAbsEncFirstReadingRecievedFlag[a]=1;
    }


    if(LoadEncoderSmoothingBaseOnMotorEncoder)
    {
        Axis[a].Feedback.Load.SmoothDegPosition +=motor_deg_enc_deriv;
        OverFlowPosOnDeg(&Axis[a].Feedback.Load.SmoothDegPosition);

        if((((Axis[a].Feedback.Load.EncPosition!= Axis[a].Feedback.Load.PrevEncPosition)||(Motor_FBK_FailedDelay[a])))&&AbsEncFirstReadingRecievedFlag[a])
        {

            Axis[a].Feedback.Load.SmoothDegPosition = Axis[a].Feedback.Load.DegPosition;
            Axis[a].Feedback.Load.PrevEncPosition = Axis[a].Feedback.Load.EncPosition;

        }
    }
    else
    {

        Axis[a].Feedback.Load.SmoothDegPosition = Axis[a].Feedback.Load.DegPosition;
        Axis[a].Feedback.Load.PrevEncPosition = Axis[a].Feedback.Load.EncPosition;
    }
    /*
    CoarsToSmoothPosDif = Axis[a].Feedback.Load.SmoothDegPosition - Axis[a].Feedback.Load.DegPosition ;

    OverFlowPosDerivOnDeg(&CoarsToSmoothPosDif);
    if(labs(CoarsToSmoothPosDif)>100)  Axis[a].Feedback.Load.SmoothDegPosition = Axis[a].Feedback.Load.DegPosition ;
     */

    if(Axis[a].Feedback.UseLoadEncoder==1)
    {
        Axis[a].Feedback.IntegratedDegPosition =      Axis[a].Feedback.Load.SmoothDegPosition;
    }
    else
    {
        if(Motor_FBK_FailedDelay==0)
            Axis[a].Feedback.IntegratedDegPosition =      Axis[a].Feedback.Motor.DegPosition;
        else
            Axis[a].Feedback.IntegratedDegPosition =      Axis[a].Feedback.Load.DegPosition;


    }



    // attempt to recover failed fbks if one failed
    if(AutoFeedbackFailRecovery)
    {
        if(( Motor_FBK_Failed[a]>0)+    (Load_FBK_Failed[a]>0)+    (Halls_FBK_Failed[a]>0) ==1)
        {
            if(Motor_FBK_Failed[a])
            {
                Axis[a].Feedback.Motor.DegPosition = Axis[a].Feedback.Load.DegPosition;
                Motor_FBK_Failed[a]=0;
                Axis[a].InitHallsNeededFlag=10;
            }

            if( Load_FBK_Failed[a])
            {
                Axis[a].Feedback.Load.DegPosition =   Axis[a].Feedback.Motor.DegPosition ;
                Load_FBK_Failed[a]=0;
            }

            if( Halls_FBK_Failed[a])
            {
                Axis[a].Feedback.Halls.DegPosition =   Axis[a].Feedback.Load.DegPosition ;
                Halls_FBK_Failed[a]=0;
            }

        }
    }



}
