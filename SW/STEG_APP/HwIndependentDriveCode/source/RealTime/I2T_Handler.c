

#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(I2T_Handle, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(I2T_Handle);
#endif
void I2T_Handle(int16_t a)
{
	static int32_t  SecondCntr[NumberOfAxes]={0,0};


	Axis[a].Current.I2T_integrator += Axis[a].Current.I;
	SecondCntr[a]+=1;
	if(SecondCntr[a]>10)
	{

		if((Axis[a].Current.I2T_integrator)>Axis[a].Current.Icont*10)
			Axis[a].Current.I2T_64 += ((Axis[a].Current.I2T_integrator)-Axis[a].Current.Icont*10)*((Axis[a].Current.I2T_integrator)-Axis[a].Current.Icont*10);
		else
			Axis[a].Current.I2T_64 -= ((Axis[a].Current.I2T_integrator)-Axis[a].Current.Icont*10)*((Axis[a].Current.I2T_integrator)-Axis[a].Current.Icont*10);

		if(Axis[a].Current.I2T_64<0)Axis[a].Current.I2T_64=0;
		Axis[a].Current.I2T_integrator=0;
		SecondCntr[a]=0;
	}
	Axis[a].Current.I2T_Value=Axis[a].Current.I2T_64>>30;//10*10 from integrator, /100//100 from current scaling/1000 for mili sec




}
