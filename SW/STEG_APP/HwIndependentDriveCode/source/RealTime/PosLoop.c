#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(PosLoopFunc, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(PosLoopFunc);
#endif
int32_t PosLoopFunc(int16_t a, int32_t posCMD, int32_t posFBK)
{


    int64_t sum64b;
    int32_t sum32b;

    float adaptiveI=0,adaptiveP=0;
    float FeedForward=0;


    Axis[a].Position.Pe = (posCMD-posFBK);
    OverFlowPosDerivOnDeg(&Axis[a].Position.Pe);
    Axis[a].Position.AbsPe=labs(Axis[a].Position.Pe);

    Axis[a].Position.PosLoopSaturated=0;

    Axis[a].Position.LimitedPe=  Axis[a].Position.Pe;
    Axis[a].Position.AbsLimitedPe=labs(Axis[a].Position.LimitedPe);


/*
    if ((Axis[a].Position.PlimMax!=-1)&&( Axis[a].Position.LimitedPe >  DistanceToPositiveLimmit[a]))
    {
        Axis[a].Position.LimitedPe = DistanceToPositiveLimmit[a];
        Axis[a].Position.PosLoopSaturated=1;
    }
    else if((Axis[a].Position.PlimMin!=-1)&&(Axis[a].Position.LimitedPe < - DistanceToNegativeLimmit[a]))
    {
        Axis[a].Position.PosLoopSaturated=1;
        Axis[a].Position.LimitedPe = -DistanceToNegativeLimmit[a];
    }
    else
    {

        //FeedForward onl if not in limits
        if((Stabilization.GyroStabilization == 1 )&& (Stabilization.InMotionCounter>0))
            FeedForward = -Axis[a].Feedback.GyroVelocty  * Axis[a].Position.Param.f_Gff + Axis[a].Position.JogInPosVelocityOutput  *Axis[a].Position.Param.f_Gff;
        else
            FeedForward = Axis[a].Position.JogInPosVelocityOutput  *Axis[a].Position.Param.f_Gff;
    }
    OverFlowPosDerivOnDeg(&Axis[a].Position.LimitedPe);
*/

    adaptiveP =  Axis[a].Position.Param.f_GpAdap *(int32_t) Axis[a].Velocity.MotorEncVfbAbs  ;
    adaptiveI =  Axis[a].Position.Param.f_GiAdap *(int32_t) Axis[a].Velocity.MotorEncVfbAbs ;

    adaptiveP=f_CustomIQsat(adaptiveP, Axis[a].Position.Param.f_Gp ,-Axis[a].Position.Param.f_Gp);

    sum64b = Axis[a].Position.LimitedPe   *( Axis[a].Position.Param.f_Gp+adaptiveP)
			                + Axis[a].Position.PeInteg *( Axis[a].Position.Param.f_Gi+adaptiveI)

			                + (Axis[a].Position.LimitedPe  - Axis[a].Position.PrevError ) * Axis[a].Position.Param.f_Gd
			                + FeedForward;






    /// limits handeler
    if ( sum64b > (int64_t) Axis[a].Velocity.Vmax)
    {
        sum64b = Axis[a].Velocity.Vmax;
        Axis[a].Position.PosLoopSaturated=1;
    }
    else if(sum64b < -(int64_t) Axis[a].Velocity.Vmax)
    {
        Axis[a].Position.PosLoopSaturated=1;
        sum64b = -Axis[a].Velocity.Vmax;
    }

    sum32b =sum64b;

/*
    if((sum32b - Axis[a].Position.Output)> (Axis[a].Ptp.ScaledAcc_f*500))
    {
        sum32b = Axis[a].Position.Output+(Axis[a].Ptp.ScaledAcc_f*500);
        Axis[a].Position.PosLoopSaturated=1;
    }

    if((sum32b - Axis[a].Position.Output)<-(Axis[a].Ptp.ScaledAcc_f*500))
    {
        sum32b = Axis[a].Position.Output-(Axis[a].Ptp.ScaledAcc_f*500);
        Axis[a].Position.PosLoopSaturated=1;
    }
*/

    if ((Axis[a].Position.PosLoopSaturated==0) && Axis[a].Velocity.VelLoopSaturated==0)
    {
        Axis[a].Position.PeInteg +=CustomIQsat( Axis[a].Position.LimitedPe ,Axis[a].Position.Param.GiSat,-Axis[a].Position.Param.GiSat);
        Axis[a].Position.PosLoopSaturated=0;
    }
    else
    {
        if ((Axis[a].Position.PeInteg<0 && Axis[a].Position.LimitedPe >0)||(Axis[a].Position.PeInteg>0 && Axis[a].Position.LimitedPe <0))
            Axis[a].Position.PeInteg += Axis[a].Position.LimitedPe ;
        Axis[a].Position.PosLoopSaturated=1;

    }


    if (Axis[a].Position.Param.Gi==0 ||   (Axis[a].Drive.State == fault) || ( Axis[a].Drive.State == disable ))
    {
        Axis[a].Position.PeInteg=0;
    }

    Axis[a].Position.PrevCommand=posCMD;


    Axis[a].Position.PrevError = 	Axis[a].Position.LimitedPe ;


    return((int32_t)sum32b);
}
