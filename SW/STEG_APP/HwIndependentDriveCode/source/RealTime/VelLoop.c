

#include "GlobalIncludes.h"

#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(CalcVel_func, "ramfuncs");
#pragma CODE_SECTION(VelLoop_func, "ramfuncs");

#pragma FUNC_ALWAYS_INLINE(CalcVel_func);
#pragma FUNC_ALWAYS_INLINE(VelLoop_func);

#endif


#define IntergratorSaturation  (int32_t)300000000
int32_t AdiTest4;

int32_t V_MotorCountsPerTs[NumberOfAxes];
int32_t V_LoadCountsPerTs[NumberOfAxes];


void CalcVel_func(int16_t a)
{
    int32_t temp,velocetyChange;

    //****************************** calculate load velocity******************************//
    V_LoadCountsPerTs[a] = (Axis[a].Feedback.Load.DegPosition-Axis[a].Velocity.LoadEncPositionCapturedValue);

    OverFlowPosDerivOnDeg(&V_LoadCountsPerTs[a]);


    Axis[a].Velocity.LoadEncVDegPS = V_LoadCountsPerTs[a] * Axis[a].Velocity.DegVelocityScaling;
    velocetyChange = Axis[a].Velocity.LoadEncVDegPS- Axis[a].Velocity.LoadEncVfb;
    if (velocetyChange>0)     temp =  (1024-(int16_t)Axis[a].Velocity.Filter);
    else                        temp = -(1024-(int16_t)Axis[a].Velocity.Filter);
    Axis[a].Velocity.LoadEncVfb +=(((int64_t)((int64_t)velocetyChange * Axis[a].Velocity.Filter)+temp)>>10);

    Axis[a].Velocity.LoadEncPositionCapturedValue = Axis[a].Feedback.Load.DegPosition;

    //****************************** calculate motor velocity******************************//

    V_MotorCountsPerTs[a] = (Axis[a].Feedback.Motor.DegPosition-Axis[a].Velocity.MotorEncPositionCapturedValue);

    OverFlowPosDerivOnDeg(&V_MotorCountsPerTs[a]);


    Axis[a].Velocity.MotorEncVDegPS = V_MotorCountsPerTs[a] * Axis[a].Velocity.DegVelocityScaling;

    velocetyChange = Axis[a].Velocity.MotorEncVDegPS - Axis[a].Velocity.MotorEncVfb;
    if (velocetyChange>0)
    {
        temp =  (1024-(int16_t)Axis[a].Velocity.Filter);
    }
    else
    {
        temp = -(1024-(int16_t)Axis[a].Velocity.Filter);
    }
    Axis[a].Velocity.MotorEncVfb +=(((int64_t)((int64_t)velocetyChange * Axis[a].Velocity.Filter)+temp)>>10);

    Axis[a].Velocity.MotorEncPositionCapturedValue = Axis[a].Feedback.Motor.DegPosition;

    //****************************************************************************************//


    Axis[a].Velocity.MotorEncVfbAbs =labs(Axis[a].Velocity.MotorEncVfb);
}
void VelLoop_func(int16_t a)
{


    int32_t v_error;
    static int32_t Prev_v_error[NumberOfAxes];

    float adaptiveP,adaptiveI,FeedForward=0;


    int32_t temp;


    Axis[a].Velocity.Command = *Axis[a].Velocity.CommandPtr;
    /*
    if( Axis[a].Velocity.Command<0)
    {
        if( DistanceToNegativeLimmit[a] <-Axis[a].Velocity.Command)Axis[a].Velocity.Command= - DistanceToNegativeLimmit[a] ;

    }
    else
    {
        if(DistanceToPositiveLimmit[a]<Axis[a].Velocity.Command)Axis[a].Velocity.Command=  DistanceToPositiveLimmit[a];

    }
*/
    if(Motor_FBK_FailedDelay[a]==0)
    {
        v_error =  (Axis[a].Velocity.Command - Axis[a].Velocity.MotorEncVfb);
    }
    else
    {
        v_error =  (Axis[a].Velocity.Command - Axis[a].Velocity.LoadEncVfb);
        Axis[a].Velocity.MotorEncVfbAbs = labs (Axis[a].Velocity.LoadEncVfb );
    }

    if((Stabilization.GyroStabilization == 1 )&& (Stabilization.InMotionCounter>0))
        FeedForward = -Axis[a].Feedback.GyroVeloctyChange  * Axis[a].Velocity.Param.f_Gff;
    else
        FeedForward=0;

    adaptiveP = Axis[a].Velocity.Param.f_GpAdap * Axis[a].Velocity.MotorEncVfbAbs;
    adaptiveI = Axis[a].Velocity.Param.f_GiAdap * Axis[a].Velocity.MotorEncVfbAbs;


    temp =  (Axis[a].Velocity.Param.f_Gp+adaptiveP) * v_error+
            Axis[a].Velocity.Param.f_Gd *( v_error - Prev_v_error[a])+
            (Axis[a].Velocity.Param.f_Gi+adaptiveI) * Axis[a].Velocity.Param.ErrorInteg +
            Axis[a].Velocity.Param.f_Gd * (v_error - Prev_v_error[a])
            +FeedForward;

    if (temp > Axis[a].Current.Imax || temp < -Axis[a].Current.Imax )
    {

        Axis[a].Velocity.VelLoopSaturated = 1;
        if ((Axis[a].Velocity.Param.ErrorInteg > 0 && v_error < 0 )|| (Axis[a].Velocity.Param.ErrorInteg < 0 && v_error > 0 ))
        {
            Axis[a].Velocity.Param.ErrorInteg += v_error;
        }
    }
    else
    {
        Axis[a].Velocity.Param.ErrorInteg += v_error;
        Axis[a].Velocity.VelLoopSaturated = 0;
    }
    Prev_v_error[a] = v_error;
    Axis[a].Velocity.Param.ErrorInteg  = CustomIQsat(Axis[a].Velocity.Param.ErrorInteg ,IntergratorSaturation,-IntergratorSaturation);


    Axis[a].Velocity.Output = CustomIQsat( temp ,Axis[a].Current.Imax,-Axis[a].Current.Imax);


    if(Axis[a].Velocity.Output>500)
    {
        temp++;
    }

    return;
}
