

#include "GlobalIncludes.h"


 Record_tag 	Record;

#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(Record_func, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(Record_func);
#endif

void Record_func(void)
{
    uint16_t i,temp=0,j;
	static int32_t prevTrigRecChannel;
	static uint16_t postSampleCNT,DelayCntr=0;
	int32_t TrigRecChannel=0;

//static int16_t internal_rec_add=0;
	if ( DelayCntr>=Record.Sample_Period )
	{
		DelayCntr = 0;
	}
	else
	{
		DelayCntr ++;
		if(Record.State!=initRAM && Record.State!=initRecord) return;
	}

	switch (Record.State)
	{
	case initRAM:

		DelayCntr = 0;
		Record.Address=0;
		prevTrigRecChannel=0;

		SetupExternalRAM_ForRecord(1);



		Record.State = initRecord;


		break;

	case initRecord:

		Record.Done = 0;
		postSampleCNT=0;


        InitializeExternalRAM_ForRecord();

		Record.State = preSample;

		break;

	case preSample:

        SetSPI_A_messageSize(0xF);


		for (i = 0; i < Record.NumberOfChannels; i++)
		{
			for(j=0;j<(ParamStruct[Record.Channel_Index[i]].Length);j++)
			{
				SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j*2)) & 0xffff));
			//	SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff)<<8);
				TrigRecChannel+=(uint32_t)(*(int16_t*)(ParamStruct[Record.Trig_Channel].ParamPointer+j*2))<<(j*16);
		/*		Record.Memory[internal_rec_add]= ((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j)>>8) & 0xff);
					internal_rec_add++;
					Record.Memory[internal_rec_add]=((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff);
					internal_rec_add++;
*/
			}
		}
		Record.Address ++;


		if(((Record.Address)>=Record.TrigPosition))
		{
			Record.State = waitForTriger;
		}


		break;

	case waitForTriger:


		for (i = 0; i < Record.NumberOfChannels; i++)
		{
			for(j=0;j<(ParamStruct[Record.Channel_Index[i]].Length);j++)
			{
				SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j*2)) & 0xffff));
				//SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff)<<8);
				TrigRecChannel+=(uint32_t)(*(int16_t*)(ParamStruct[Record.Trig_Channel].ParamPointer+j*2))<<(j*16);
			/*	Record.Memory[internal_rec_add]= ((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j)>>8) & 0xff);
				internal_rec_add++;
				Record.Memory[internal_rec_add]=((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff);
				internal_rec_add++;
*/
			}
		}
		Record.Address ++;

		switch (Record.Trig_Mod)
		{
		case 	ImmTrigger :
			Record.State = postSample;
			break;
		case RisingTrigger :
			if(TrigRecChannel>Record.Trig_Value && prevTrigRecChannel<Record.Trig_Value) 			 Record.State = postSample;
			break;

		case FallingTrigger:
			if(TrigRecChannel<Record.Trig_Value && prevTrigRecChannel>Record.Trig_Value) 			 Record.State = postSample;

			break;

		}
		break;

		case postSample:



			for (i = 0; i < Record.NumberOfChannels; i++)
			{
				for(j=0;j<(ParamStruct[Record.Channel_Index[i]].Length);j++)
				{
					SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j*2)) & 0xffff));
				//	SendDataToRAM(((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff)<<8);
				/*	Record.Memory[internal_rec_add]= ((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j)>>8) & 0xff);
						internal_rec_add++;
						Record.Memory[internal_rec_add]=((int32_t)(*(int16_t*)(ParamStruct[Record.Channel_Index[i]].ParamPointer+j))& 0xff);
						internal_rec_add++;
						*/
				}

			}
			Record.Address ++;

			postSampleCNT++;
			if (postSampleCNT >=(Record.Length-Record.TrigPosition))
			{
				Record.State = finishRecord;
			}



			break;

		case finishRecord:

			if( (GetSPIA_TX_Buffer_LVL()) ==0)
			{

		        SetupExternalRAM_ForRecord(0);

				temp += GetSPIA_RX_Buffer();
				Record.State = idleRecord;

			}

			break;

		case idleRecord:




			Record.Done = 1;

			break;

	}


}
