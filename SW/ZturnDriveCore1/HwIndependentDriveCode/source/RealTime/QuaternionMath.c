/**
 * <code>fromAngles</code> builds a Quaternion from the Euler rotation
 * angles (y,r,p). Note that we are applying in order: roll, pitch, yaw but
 * we've ordered them in x, y, and z for convenience.
 * @see <a href="http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm">http://www.euclideanspace.com/maths/geometry/rotations/conversions/eulerToQuaternion/index.htm</a>
 *
 * @param yaw
 *            the Euler yaw of rotation (in radians). (aka Bank, often rot
 *            around x)
 * @param roll
 *            the Euler roll of rotation (in radians). (aka Heading, often
 *            rot around y)
 * @param pitch
 *            the Euler pitch of rotation (in radians). (aka Attitude, often
 *            rot around z)
 */
#include "GlobalIncludes.h"
#include <math.h>
#define HALF_PI 1.5708

#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(fromAngles, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(fromAngles);


#pragma CODE_SECTION(toAngles, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(toAngles);
#pragma CODE_SECTION(fromAngleNormalAxis, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(fromAngleNormalAxis);
#pragma CODE_SECTION(Quaternion_mult, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(Quaternion_mult);
#endif

Quaternion_struct  fromAngles(   float32_t yaw, float32_t roll, float32_t pitch) {
    float32_t angle;
    float32_t sinRoll, sinPitch, sinYaw, cosRoll, cosPitch, cosYaw;
    float32_t num,inv;
    float32_t cosRollXcosPitch=0,sinRollXsinPitch=0,cosRollXsinPitch=0,sinRollXcosPitch=0;
    Quaternion_struct Quaternion;
    angle = pitch * 0.5*0.00000174532925199;
    sinPitch = sinf(angle);
    cosPitch = cosf(angle);
    angle = roll * 0.5*0.00000174532925199;
    sinRoll = sinf(angle);
    cosRoll = cosf(angle);
    angle = yaw * 0.5f*0.00000174532925199;
    sinYaw = sinf(angle);
    cosYaw = cosf(angle);

    // variables used to reduce multiplication calls.
     cosRollXcosPitch = cosRoll * cosPitch;
     sinRollXsinPitch = sinRoll * sinPitch;
     cosRollXsinPitch = cosRoll * sinPitch;
     sinRollXcosPitch = sinRoll * cosPitch;

 /*   Quaternion.w = (cosRollXcosPitch * cosYaw - sinRollXsinPitch * sinYaw);
    Quaternion.x = (cosRollXcosPitch * sinYaw + sinRollXsinPitch * cosYaw);
    Quaternion.y = (sinRollXcosPitch * cosYaw + cosRollXsinPitch * sinYaw);
    Quaternion.z = (cosRollXsinPitch * cosYaw - sinRollXcosPitch * sinYaw);
    q.w() = cy * cp * cr + sy * sp * sr;
    x = cy * cp * sr - sy * sp * cr;
    y = sy * cp * sr + cy * sp * cr;
    x = sy * cp * cr - cy * sp * sr;
    */
    Quaternion.w = (cosRollXcosPitch * cosYaw + sinRollXsinPitch * sinYaw);
    Quaternion.x = (sinRollXcosPitch * cosYaw - cosRollXsinPitch * sinYaw);
    Quaternion.y = (sinRollXcosPitch * sinYaw + cosRollXsinPitch * cosYaw);
    Quaternion.z = (cosRollXcosPitch * sinYaw - sinRollXsinPitch * cosYaw);
    num = (((Quaternion.x * Quaternion.x) + (Quaternion.y * Quaternion.y)) + (Quaternion.z * Quaternion.z)) + (Quaternion.w * Quaternion.w);
    inv = 1.0f / (sqrtf(num));
    Quaternion.w *=inv;
    Quaternion.x *=inv;
    Quaternion.y *=inv;
    Quaternion.z *=inv;
    return Quaternion;
}
/**
 * <code>toAngles</code> returns this quaternion converted to Euler
 * rotation angles (yaw,roll,pitch).<br/>
 * @see <a href="http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm">http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToEuler/index.htm</a>
 *
 * @param angles
 *            the float32_t[] in which the angles should be stored, or null if
 *            you want a new float32_t[] to be created
 * @return the float32_t[] in which the angles are stored.
 */
/*
Angles_struct toAngles(float32_t w,float32_t x,float32_t y,float32_t z)
{
    float32_t sqw = w * w;
    float32_t sqx = x * x;
    float32_t sqy = y * y;
    float32_t sqz = z * z;
    Angles_struct angles;
    float32_t unit = sqx + sqy + sqz + sqw; // if normalized is one, otherwise
    // is correction factor
    float32_t test = x * y + z * w;
    if (test > 0.499 * unit) { // singularity at north pole
        angles.roll = 2 * atan2f(x, w);
        angles.pitch = HALF_PI;
        angles.yaw = 0;
    } else if (test < -0.499 * unit) { // singularity at south pole
        angles.roll = -2 * atan2f(x, w);
        angles.pitch = -HALF_PI;
        angles.yaw = 0;
    } else {
        angles.roll = atan2f(2 * y * w - 2 * x * z, sqx - sqy - sqz + sqw); // roll or heading
        angles.pitch = asinf(2 * test / unit); // pitch or attitude
        angles.yaw = atan2f(2 * x * w - 2 * y * z, -sqx + sqy - sqz + sqw); // yaw or bank
    }
    angles.roll*=572957.8;
    angles.yaw*=572957.8;
    angles.pitch*=572957.8;

    return angles;
}
*/
Angles_struct toAngles(float32_t w,float32_t x,float32_t y,float32_t z)
{
    float32_t sinp=0,sinr_cosp,cosr_cosp,siny_cosp,cosy_cosp;
    Angles_struct angles;

    // roll (x-axis rotation)
     sinr_cosp = +2.0 * (w * x + y * z);
     cosr_cosp = +1.0 - 2.0 * (x * x + y * y);
    angles.roll = atan2f(sinr_cosp, cosr_cosp);

    // pitch (y-axis rotation)
     sinp = +2.0 * (w * y - z * x);
    if (fabs(sinp) >= 1)
        angles.pitch = copysignf(HALF_PI, sinp); // use 90 degrees if out of range
    else
        angles.pitch = asinf(sinp);

    // yaw (z-axis rotation)
     siny_cosp = +2.0 * (w * z + x * y);
     cosy_cosp = +1.0 - 2.0 * (y * y + z * z);
    angles.yaw = atan2f(siny_cosp, cosy_cosp);
    angles.roll*=572957.8;
    angles.yaw*=572957.8;
    angles.pitch*=572957.8;

    return angles;
}


/**
 * <code>fromAngleNormalAxis</code> sets this quaternion to the values
 * specified by an angle and a normalized axis of rotation.
 *
 * @param angle
 *            the angle to rotate (in radians).
 * @param axis
 *            the axis of rotation (already normalized).
 */

//   So to rotate around the x axis for example, you could create a quaternion with createFromAxisAngle(1, 0, 0, M_PI/2) and multiply it by the current rotation quaternion of your model.

 Quaternion_struct fromAngleNormalAxis(float32_t angle, Angles_struct axis)
 {
     Quaternion_struct  Quaternion;
        float32_t sin = sinf(angle*0.5*1.745329252E-06);
        Quaternion.w = cosf(angle*0.5*1.745329252E-06);
        Quaternion.x = sin * axis.roll;
        Quaternion.y = sin * axis.pitch;
        Quaternion.z = sin * axis.yaw;

    return Quaternion;
}


/**
 * <code>mult</code> multiplies this quaternion by a parameter quaternion.
 * The result is returned as a new quaternion. It should be noted that
 * quaternion multiplication is not commutative so q * p != p * q.
 *
 * It IS safe for q and res to be the same object.
 * It IS safe for this and res to be the same object.
 *
 * @param q
 *            the quaternion to multiply this quaternion by.
 * @param res
 *            the quaternion to store the result in.
 * @return the new quaternion.
 */

 Quaternion_struct  Quaternion_mult(Quaternion_struct a, Quaternion_struct b)
 {
     Quaternion_struct Quaternion;

             Quaternion.w=   a.w * b.w - a.x * b.x - a.y * b.y - a.z * b.z;  // 1
             Quaternion.x=         a.w * b.x + a.x * b.w + a.y * b.z - a.z * b.y;  // i
             Quaternion.y=        a.w * b.y - a.x * b.z + a.y * b.w + a.z * b.x;  // j
             Quaternion.z=        a.w * b.z + a.x * b.y - a.y * b.x + a.z * b.w ;  // k



    return Quaternion;
}



