#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(FaultHandler_func, "ramfuncs");
#pragma CODE_SECTION( WarningHandler_func, "ramfuncs");

#pragma FUNC_ALWAYS_INLINE(WarningHandler_func);
#pragma FUNC_ALWAYS_INLINE(FaultHandler_func);
#endif

#define mSEC_DELAY_BEFORE_FAULT 20
int16_t FaultIgnoreFlag=0;
int16_t Eeprom_Read_Fault_Flag=0;
int16_t Eeprom_Write_Fault_Flag=0;
int16_t SCI_Buff_OF_Fault=0;

uint16_t Motor_FBK_Failed[NumberOfAxes]={0,0};
uint16_t Load_FBK_Failed[NumberOfAxes]={0,0};
uint16_t Halls_FBK_Failed[NumberOfAxes]={0,0};

int16_t Motor_FBK_FailedDelay[NumberOfAxes]={0,0};

int16_t General_FBK_FailedDelay[NumberOfAxes]={0,0};
long FBK_FLT_DELAY = 5000;

int16_t AdiEstopTest=0;
int16_t FltAdiTest=0;
int16_t  Illegal_halls_Flag[NumberOfAxes]={0,0};

VN_INS_Status_tag VN_INS_Status;
uint16_t VN_GNSS_FIX_Status=0;

static int16_t AbsEncReadingFaultFlag[NumberOfAxes]={0,0};
int16_t AbsEncFirstReadingRecievedFlag[NumberOfAxes]={0,0};
int16_t PrevAbsEncFirstReadingRecievedFlag[NumberOfAxes]={0,0};
int16_t CPLD_SPI_FaultFlag=0;
int16_t LowPriorityInterruptFault=0;



void WarningHandler_func(int16_t a)
{



    if(InitFinishedFlag==0) return;

    if (Illegal_halls_Flag[a] && (Axis[a].InitHallsNeededFlag==0))
    {
        Axis[a].Drive.Warnings|=illegal_halls_warning ;


        Halls_FBK_Failed[a]=1;
    }

    if (Halls_FBK_Failed[a] )
    {
        Axis[a].Drive.Warnings|=illegal_halls_warning ;
    }


    if (Motor_FBK_FailedDelay[a] )
    {
        Axis[a].Drive.Warnings|=IncrEnc_warning ;
    }
    if (Load_FBK_Failed[a] )
    {
        Axis[a].Drive.Warnings|=AbsEncNotInUse_warning ;
    }


    if ( Axis[a].Drive.DriveTemp_sense > Axis[a].Drive.OverTemp)
    {
        Axis[a].Drive.Warnings|=IPM_over_temp_warning ;
    }

    if (Axis[a].Position.AbsPe > ((long)MaxSystemPe*3))
    {
        Axis[a].Drive.Warnings|=PositionError_warning ;
    }

    if ((Axis[a].Current.I2T_Value > Axis[a].Current.I2T_lim)&&(Axis[a].Current.I2T_lim>0))
    {
        Axis[a].Drive.Warnings|=i2lim_warning ;
    }

    if(Axis[0].Drive.VBus > Axis[0].Drive.VBusMax)
    {
        Axis[a].Drive.Warnings|=over_voltage_warning ;
    }

    if(SCI_Buff_OF_Fault)
    {
        SCI_Buff_OF_Fault=0;
        Axis[a].Drive.Warnings|=sci_buff_OF_warning ;
    }


    if( Stabilization.GyroStabilizationWarning)
    {
        Axis[a].Drive.Warnings|=GYRO_warning ;
    }

    if( Axis[a].Feedback.VN_Warning)
    {
        Axis[a].Drive.Warnings|=VN_warning ;
    }


    if((Axis[a].Feedback.Load.EncPositionStatus!=0 ) && FirstSpiRead==0)
    {
        Axis[a].Drive.Warnings|=AbsEncMagnetDistance_warning;
        Load_FBK_Failed[a]=1;
    }


    if(CPLD_Status & CPLD_STATUS_SSI_axis_0_flt)

        AbsEncReadingFaultFlag[0]=1;

    else AbsEncReadingFaultFlag[0]=0;

    if(CPLD_Status & CPLD_STATUS_SSI_axis_1_flt)

        AbsEncReadingFaultFlag[1]=1;

    else
        AbsEncReadingFaultFlag[1]=0;


    if((AbsEncReadingFaultFlag[a]) && FirstSpiRead==0)
    {
        Axis[a].Drive.Warnings|=AbsEncCommunicaion_warning;
        Load_FBK_Failed[a]=1;
    }

    if( (Axis[a].Feedback.UseLoadEncoder==0))
    {
        Axis[a].Drive.Warnings|=AbsEncNotInUse_warning;
    }

    if(CPLD_SPI_FaultFlag && FirstSpiRead==0)
    {
        Axis[0].Drive.Warnings|=CPLD_SPI_warning ;
        Axis[1].Drive.Warnings|=CPLD_SPI_warning ;
        Axis[0].Drive.Warnings|=GYRO_warning ;
        Axis[1].Drive.Warnings|=GYRO_warning ;
        Axis[0].Drive.Warnings|=AbsEncNotInUse_warning;
        Axis[1].Drive.Warnings|=AbsEncNotInUse_warning;


        Stabilization.GyroStabilization = 0;
        Load_FBK_Failed[1]=1;
        Load_FBK_Failed[0]=1;


    }

    if((SPI_Data_checkSumDiffCNTR>10 ) && FirstSpiRead==0)
    {
        SPI_Data_checkSumDiffCNTR=0;

        Axis[0].Drive.Warnings|=GYRO_warning ;
        Axis[1].Drive.Warnings|=GYRO_warning ;
        Axis[0].Drive.Warnings|=CPLD_SPI_CHK_SUM_warning ;
        Axis[1].Drive.Warnings|=CPLD_SPI_CHK_SUM_warning ;
        Axis[0].Drive.Warnings|=AbsEncNotInUse_warning;
        Axis[1].Drive.Warnings|=AbsEncNotInUse_warning;

        Stabilization.GyroStabilization = 0;
        Load_FBK_Failed[0]=1;
        Load_FBK_Failed[1]=1;


    }

    if(AbsEncFirstReadingRecievedFlag[a]==0)
    {
        Axis[a].Drive.Warnings|=no_abs_position_warning;
    }

    if(Stabilization.GyroStabilization && ((( Axis[a].Feedback.GyroPosChange >50000)|| ( Axis[a].Feedback.GyroPosChange <-50000)||FirstSpiRead))) //if the change is more that 5 deg/40 msec (90deg/sec should be operation limit)
    {
        Axis[a].Drive.Warnings|=GYRO_warning ;
        Stabilization.GyroStabilization = 0;
    }

    if(LowPriorityInterruptFault)
    {
        LowPriorityInterruptFault=0;
        Axis[a].Drive.Warnings|=low_priority_int_warning ;

    }


    if(Axis[0].Drive.VBus < Axis[0].Drive.VBusMin)
    {
        Axis[a].Drive.Warnings|=under_voltage_warning ;

    }




}
long HallsPFB_to_MotorPfb_Error[NumberOfAxes],HallsPFB_to_LoadPfb_Error[NumberOfAxes],MotorPFB_to_LoadPfb_Error[NumberOfAxes];

void FaultHandler_func(int16_t a)
{

    ClearSCIB_ERRORS();

    ClearSCIA_ERRORS();


    if(InitFinishedFlag==0)
    {
        CPLD_SPI_FaultFlag=0;
        AbsEncReadingFaultFlag[a]=0;
        LowPriorityInterruptFault=0;
        SPI_Data_checkSumDiffCNTR=0;
        return;
    }



    HallsPFB_to_MotorPfb_Error[a] = Axis[a].Feedback.Halls.DegPosition -  Axis[a].Feedback.Motor.DegPosition;
    if(PrevAbsEncFirstReadingRecievedFlag[a])  HallsPFB_to_LoadPfb_Error[a] = Axis[a].Feedback.Halls.DegPosition -  Axis[a].Feedback.Load.DegPosition;
    if(PrevAbsEncFirstReadingRecievedFlag[a])   MotorPFB_to_LoadPfb_Error[a]= Axis[a].Feedback.Motor.DegPosition -  Axis[a].Feedback.Load.DegPosition;

    OverFlowPosDerivOnDeg(&HallsPFB_to_MotorPfb_Error[a]);




    OverFlowPosDerivOnDeg(&HallsPFB_to_LoadPfb_Error[a]);
    OverFlowPosDerivOnDeg(&MotorPFB_to_LoadPfb_Error[a]);

    HallsPFB_to_MotorPfb_Error[a]=labs(HallsPFB_to_MotorPfb_Error[a]);
    HallsPFB_to_LoadPfb_Error[a]=labs(HallsPFB_to_LoadPfb_Error[a]);
    MotorPFB_to_LoadPfb_Error[a]=labs(MotorPFB_to_LoadPfb_Error[a]);

    //if more that 1 feedback failed
    if((HallsPFB_to_MotorPfb_Error[a]>(MaxSystemPe)) && (HallsPFB_to_LoadPfb_Error[a]>MaxSystemPe)&& (MotorPFB_to_LoadPfb_Error[a]>MaxSystemPe))
    {
        Axis[a].Drive.Faults|=general_pos_fbk_flt;
        Halls_FBK_Failed[a]=1;
        Motor_FBK_Failed[a]=1;
        Load_FBK_Failed[a]=1;


        Motor_FBK_FailedDelay[a]=FBK_FLT_DELAY;


    }
    else
    {
        if(HallsPFB_to_MotorPfb_Error[a]>(MaxSystemPe)) //halls or motor incorrect
        {//now test witch one
            if((HallsPFB_to_LoadPfb_Error[a]>MaxSystemPe)) // may be halls
            {
                Halls_FBK_Failed[a]=1;

            }
            else if (Halls_FBK_Failed[a]==0)
            {
                Motor_FBK_Failed[a]=1;
                Motor_FBK_FailedDelay[a]=FBK_FLT_DELAY;
            }

        }
        else if((MotorPFB_to_LoadPfb_Error[a]>MaxSystemPe)&&(Motor_FBK_FailedDelay[a]==0)) //hallls or motor incorrect
        {//now test witch one
            Load_FBK_Failed[a]=1;



        }

    }

    if(Motor_FBK_FailedDelay[a]>0)Motor_FBK_FailedDelay[a]--;


    if(((Halls_FBK_Failed[a]>0)+(Motor_FBK_FailedDelay[a]>0)+(Load_FBK_Failed[a]>0))>1)
    {
        Axis[a].Drive.Faults|=general_pos_fbk_flt;


    }



    if (Illegal_halls_Flag[a] && (Axis[a].InitHallsNeededFlag!=0))
    {
        Axis[a].Drive.Faults|=illegal_halls_flt ;
        Axis[a].InitHallsNeededFlag=10;
        Halls_FBK_Failed[a]=1;
    }



    if (Axis[a].Position.AbsPe > (MaxSystemPe*4))
    {
        Axis[a].Drive.Faults|=position_error_flt ;
    }
    else        Axis[a].Drive.Faults&=~position_error_flt ;


    if (Axis[a].Current.I > Axis[a].Current.Iflt)
    {
        Axis[a].Drive.Faults|=over_current ;

    }
    else 		Axis[a].Drive.Faults&=~over_current ;

    if(Axis[0].Drive.VBus >(Axis[0].Drive.VBusMax+20))
       {
           Axis[a].Drive.Faults|=over_voltage_flt ;
       }
    else       Axis[a].Drive.Faults&=~over_voltage_flt ;



    if(Axis[0].Drive.VBus < 100)
    {
        Axis[0].Drive.Faults|=under_voltage_flt ;
        Axis[1].Drive.Faults|=under_voltage_flt ;

    }
    else       Axis[a].Drive.Faults&=~under_voltage_flt ;

    if((CPDL_Version!=(unsigned long)0xDFA51001)&&InitFinishedFlag)
    {
        Axis[0].Drive.Faults|=Incorect_CPLD_Version ;
        Axis[1].Drive.Faults|=Incorect_CPLD_Version ;

    }
    else       Axis[a].Drive.Faults&=~Incorect_CPLD_Version ;

    if(GetMotorOverTemperatureState(a))
    {
        Axis[a].Drive.Faults|=motor_over_temp_flt ;

    }
    else       Axis[a].Drive.Faults&=~motor_over_temp_flt ;

    if(FirstSpiRead)
    {
        Axis[a].Drive.Faults|=CPLD_spi_not_started ;

    }
    else       Axis[a].Drive.Faults&=~CPLD_spi_not_started ;



    if(((WatchDog<=0) || WatchDogFlag==0)&& WatchDogTime )
    {

        Axis[1].Drive.Faults_history|=watchdog_flt ;
        Axis[0].Drive.Faults_history|=watchdog_flt ;
        Axis[0].Serial.Jcmd =0;
        Axis[1].Serial.Jcmd=0;
        Axis[1].Ptp.DistanceLeft_f =(float) Axis[1].Ptp.Vel_f*Axis[1].Ptp.Vel_f / (Axis[1].Ptp.ScaledDec_f*2) ;
        Axis[0].Ptp.DistanceLeft_f =(float) Axis[0].Ptp.Vel_f*Axis[0].Ptp.Vel_f / (Axis[0].Ptp.ScaledDec_f*2) ;

    }

    if(OverCurrentErrorDetected())
    {
        Axis[1].Drive.Faults|=hw_over_current_flt ;
        Axis[0].Drive.Faults|=hw_over_current_flt ;

        OverCurrentErrorClear();
    }

    if(Eeprom_Read_Fault_Flag)
    {
        Axis[1].Drive.Faults|=Eeprom_read_flt ;
        Axis[0].Drive.Faults|=Eeprom_read_flt ;

        Eeprom_Read_Fault_Flag=0;
    }

    if(Eeprom_Write_Fault_Flag)
    {
        Axis[1].Drive.Faults|=Eeprom_write_flt ;
        Axis[0].Drive.Faults|=Eeprom_write_flt ;

        Eeprom_Read_Fault_Flag=0;
    }




    if (Axis[a].Drive.Faults != no_faults)
    {
        Axis[a].Drive.State= fault;
    }
    else if (Axis[a].Drive.Faults == no_faults && Axis[a].Drive.Faults_history == no_faults && Axis[a].Drive.State == fault ) Axis[a].Drive.State = disable;
    Axis[a].Drive.Faults_history |= Axis[a].Drive.Faults;

}
