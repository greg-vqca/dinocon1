

// This is an independent project of an individual developer. Dear PVS-Studio, please check it.

// PVS-Studio Static Code Analyzer for C, C++, C#, and Java: http://www.viva64.com


#include "GlobalIncludes.h"


int32_t volatile Max_PE_in_Jog =20000;
int32_t volatile MaxSystemPe= 20000;

void EncHandler_func(int16_t a);
void PTPGeneratorFPUFunc(int16_t a);
volatile int16_t	EEPROM_Delay=0;
uint32_t startTime =0;
uint32_t CurrentTime =0;
float32_t RT_Perioud =0;


uint32_t endtime=0;
uint32_t LPI_endtime=0;
uint32_t LPI_startTime =0;

float32_t LPI_runTime =0;
float32_t MaxLPI_runTime =0;
int16_t posloopdevider=0;
int32_t DistanceToLimmit;

uint32_t Test_endTime=0;
uint32_t Test_timePeriod=0;

uint32_t Test_startTime =0;

float32_t Test_runTime =0;
float32_t MaxTest_runTime =0;

int16_t ACS_LimitProximity =4000;


int32_t runTime =0;

int32_t WatchDog=5000;
int16_t WatchDogFlag=0;
int32_t WatchDogTime;
int16_t  MaxRealTimeExecutionTime[16] ={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
int16_t  RealTimeExecutionPeriod =0;
int16_t  MaxRealTimeExecutionPeriod =0;

int16_t  PWM_Freq_scaling;


int16_t mSecDivider=0;
int16_t FirstSpiRead=100;
uint16_t  Led_CNTR[NumberOfAxes]={0,0};
int32_t mSecCounter=0;
int16_t	HeartBeatCounter=0;

volatile int16_t UserIntRunning=0,UserIntMissed=0;

int16_t  ExecutionSlot4=0;


#if TI_DSP_Pragma ==1


#pragma CODE_SECTION(HighPriorityInterupt, "ramfuncs");
#pragma CODE_SECTION(UserInt1Func, "ramfuncs");
#pragma CODE_SECTION(CustomIQsat, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(CustomIQsat);
#pragma CODE_SECTION(f_CustomIQsat, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(f_CustomIQsat);
#pragma CODE_SECTION(OverFlowPosOnDeg, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(OverFlowPosOnDeg);

#pragma CODE_SECTION(OverFlowPosDerivOnDeg, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(OverFlowPosDerivOnDeg);
#pragma CODE_SECTION(CheckPosPlim, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(CheckPosPlim);
#pragma CODE_SECTION(CheckNegPlim, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(CheckNegPlim);
#endif



float32_t CLAoutput=0;   // filter delay chain
float32_t CLAinput=0;   // filter delay chain
int32_t interpolationCTR=0;
float TimeDiv=100000 / NUMBER_OF_COUNTS_FOR_INTERRUPT ; //100mghz clock (10 usec for realtme)

int32_t Time_msec=0;
int32_t Time_msecCTR=0;

int16_t FirstRealTimeFlag=1;
int32_t  DistanceToPositiveLimmit[NumberOfAxes];

int32_t  DistanceToNegativeLimmit[NumberOfAxes];

int16_t CPLD_SPI_Data_Recieved_Flag=0;
int16_t CPLD_FIRST_SPI_Data_Recieved_Flag=0;
int32_t MaxPos=0,MinPos=(long)4000000;


int32_t AdiTest=0;
int32_t SPI_Data_checkSum=0;
int32_t SPI_Data_checkSumDiffCNTR=0;

int16_t CPLD_SPI_FaultCtr=0;


#if TI_DSP_Pragma
__interrupt void HighPriorityInterupt(void)   // ADC  (Can also be ISR for INT10.1 when enabled)

#endif

#if Xilinx_Pragma
void HighPriorityInterupt(void *data, uint8_t TmrCtrNumber)
#endif

{
    int OnNegLimit,OnPosLimit;
    int16_t i=0;
    volatile int16_t temp;
    volatile float32_t temp_f=0;
    int32_t temp32b;
	

#if TI_DSP_Pragma
    AcknowledgeHighPriority_Interupt();
#endif
    Time_msecCTR++;
    if (Time_msecCTR>=TimeDiv)
    {
        Time_msecCTR=0;
        Time_msec++;
        if(WatchDog>0)	WatchDog--;
               BIT_Timer[0]++;
               BIT_Timer[1]++;
    }
    CurrentTime = GetCpuTimer1Value();
    RT_Perioud=  ( startTime-CurrentTime)*0.0111083984375;//in usec;
    startTime =  CurrentTime;





    //service Watchdog
    ServiceWatchDog();

    //  EPwm7Regs.ETCLR.bit.int16_t=1;
    Led_CNTR[0]++;
    if(EEPROM_Delay>0)EEPROM_Delay--;
    else EEPROM_Delay=0;
    if (Led_CNTR[0]>6000) Led_CNTR[0]=0;

    Led_CNTR[1]++;
    if (Led_CNTR[1]>6000)
    {
        Led_CNTR[1]=0;

    }

    EnableInterupts(1);


    //toggle CPLD Watchdog

    // Test =CustomIQmpy((int32_t) Axis[0].Current.Param.GpAdap ,(int32_t) labs(Axis[0].Velocity.EncVfb )/**(Velocity.Vfb )*/, 20) ;





    //First realtime after Init
    if(FirstRealTimeFlag==1)
    {
        ExecutionSlot4=0;
        EncHandler_func(0);
        EncHandler_func(1);
        InitHalls(0);
        InitHalls(1);
        FirstSpiRead =100;
        FirstRealTimeFlag=0;

    }

    //single run executions
    switch (ExecutionSlot4)
    {

    case 0:


        if(DMA_Transfer_finished())
        {
            CPLD_SPI_FaultFlag=0;
            CPLD_SPI_FaultCtr=0;
            Get_DMA_Data();


        }
        else
        {
            CPLD_SPI_Data_Recieved_Flag=0;
            CPLD_SPI_FaultCtr++;
        }
        if(CPLD_FIRST_SPI_Data_Recieved_Flag && CPLD_SPI_FaultCtr>32)
        {
            CPLD_SPI_FaultCtr=32;
            CPLD_SPI_FaultFlag =1;

        }

        FpgaSyncSignalSet(1);
        Do_DMA_Transfer();


        break;
    case 1:
        break;
    case 2:
        //read SPI from FPGA
        FpgaSyncSignalSet(0);


        break;
    case 3:
        break;
    }


    ExecutionSlot4++;
    if(  ExecutionSlot4>3)         ExecutionSlot4=0;


    for(i=0;i<2;i++)
    {

        EncHandler_func(i);
        if( (InitFinishedFlag))
        {
            CheckPosPlim(Axis[i].Feedback.IntegratedDegPosition, Axis[i].Position.PlimMax);
            DistanceToPositiveLimmit[i] = labs(DistanceToLimmit);

            CheckNegPlim(Axis[i].Feedback.IntegratedDegPosition, Axis[i].Position.PlimMin);
            DistanceToNegativeLimmit[i] = labs(DistanceToLimmit);


        }
        else
        {
            Axis[i].Drive.State = disable;
        }

        if(ExecutionSlot4==0)
        {

            switch (Axis[i].Drive.Opmode)
            {
            case torque:
                Axis[i].Current.CommandPtr = &Axis[i].Serial.Icmd;
                Axis[i].Position.SystemCommand=Axis[i].Feedback.IntegratedDegPosition;
                Axis[i].Position.LimitedCommand=Axis[i].Feedback.IntegratedDegPosition;
                Axis[i].Velocity.CommandPtr = &Axis[i].Velocity.MotorEncVfbAbs;
                break;

            case position:
                JogInPosition_Func(i);


                //if we at the limit dont alow jog
                /*         if(((Axis[i].Position.LimitedToSystemDistance>Max_PE_in_Jog) && (Axis[i].Position.JogInPosVelocityOutput>0))
                        ||((Axis[i].Position.LimitedToSystemDistance<-Max_PE_in_Jog) && (Axis[i].Position.JogInPosVelocityOutput<0)))
                {
                    Axis[i].Position.JogInPosVelocityOutput=0;
                    Axis[i].Position.JogInPosAcceleration=0;
                }
                 */

                //fix rounding error
                Axis[i].Position.JogInPosVelocityOutputFixedPoint = Axis[i].Position.JogInPosVelocityOutput + Axis[i].Position.JogInPosVelocityOutputFix;
                Axis[i].Position.JogInPosVelocityOutputFix+= Axis[i].Position.JogInPosVelocityOutput-Axis[i].Position.JogInPosVelocityOutputFixedPoint;


                Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.SystemCommand- Axis[i].Position.LimitedCommand ;
                OverFlowPosDerivOnDeg(&Axis[i].Position.LimitedToSystemDistance);

                if((Axis[i].Position.AbsPe<Max_PE_in_Jog)||(Axis[i].Position.Pe*Axis[i].Position.JogInPosVelocityOutputFixedPoint<0))
                {
                    if(((Axis[i].Position.LimitedToSystemDistance<Max_PE_in_Jog )&& (Axis[i].Position.LimitedToSystemDistance>-Max_PE_in_Jog ) )
                            ||(Axis[i].Position.LimitedToSystemDistance*Axis[i].Position.JogInPosVelocityOutputFixedPoint<0))
                    {
                    Axis[i].Position.SystemCommand+=(int32_t)(Axis[i].Position.JogInPosVelocityOutputFixedPoint) ;
                    Axis[i].Ptp.EndCommand+=(int32_t)(Axis[i].Position.JogInPosVelocityOutputFixedPoint);
                    }
                }
                OverFlowPosOnDeg(&Axis[i].Position.SystemCommand);
                OverFlowPosOnDeg(&Axis[i].Ptp.EndCommand);

                if(Stabilization.GyroStabilization && (Stabilization.InMotionCounter>0))
                {
                    Axis[i].Position.SystemCommand-= Axis[i].Feedback.GyroPosChange  ;
                    Axis[i].Ptp.EndCommand-=Axis[i].Feedback.GyroPosChange;
                    OverFlowPosOnDeg(&Axis[i].Position.SystemCommand);
                    OverFlowPosOnDeg(&Axis[i].Ptp.EndCommand);
                }

                Axis[i].Position.SystemCommand += Axis[i].Ptp.Output;
                Axis[i].Ptp.Output=0;

                OverFlowPosOnDeg(&Axis[i].Position.SystemCommand);


                if(i==1)
                {
                    if(CheckPosPlim(Axis[i].Position.SystemCommand, Axis[i].Position.PlimMax))
                    {
                        OnPosLimit=1;
                    }
                    else
                    {
                        OnPosLimit=0;
                    }
                    Axis[i].Position.LimitedToSystemDistanceMaxPos = labs(DistanceToLimmit);

                    if( CheckNegPlim(Axis[i].Position.SystemCommand, Axis[i].Position.PlimMin))                                     {
                        OnNegLimit=1;
                    }
                    else
                    {
                        OnNegLimit=0;
                    }
                    Axis[i].Position.LimitedToSystemDistanceMaxNeg = labs(DistanceToLimmit);


                    Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.SystemCommand- Axis[i].Position.LimitedCommand ;
                    OverFlowPosDerivOnDeg(&Axis[i].Position.LimitedToSystemDistance);


                    if(OnPosLimit)
                    {

                        if((Axis[i].Position.PlimMax!=Axis[i].Position.PlimMin))
                        {
                            //try to move out if possibel
                            // Axis[i].Serial.Jcmd =Axis[i].Position.PlimMax- ACS_LimitProximity - Axis[i].Position.LimitedCommand ;
                            Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.PlimMax- ACS_LimitProximity - Axis[i].Position.LimitedCommand ;

                        }
                        else
                        {
                            Axis[i].Position.LimitedToSystemDistance=0;


                        }
                    }
                    else  if(OnNegLimit)
                    {
                        Axis[i].Position.LimitedToSystemDistanceMaxNeg = labs(DistanceToLimmit);

                        if((Axis[i].Position.PlimMax!=Axis[i].Position.PlimMin))
                        {
                            Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.PlimMin+ ACS_LimitProximity- Axis[i].Position.LimitedCommand ;

                        }
                        else
                        {
                            Axis[i].Position.LimitedToSystemDistance=0;

                        }
                    }
                    else
                    {
                        Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.SystemCommand- Axis[i].Position.LimitedCommand ;
                    }
                    OverFlowPosDerivOnDeg(&Axis[i].Position.LimitedToSystemDistance);

                }
                else
                {
                    CheckPosPlim(Axis[i].Position.LimitedCommand, Axis[i].Position.PlimMax) ;
                    Axis[i].Position.LimitedToSystemDistanceMaxPos = labs(DistanceToLimmit);
                    /*        CheckPosPlim(Axis[i].Position.SystemCommand, Axis[i].Position.PlimMax);
                    if( Axis[i].Position.LimitedToSystemDistanceMaxPos >  labs(DistanceToLimmit))
                    {
                        Axis[i].Position.LimitedToSystemDistanceMaxPos = labs(DistanceToLimmit);
                    }
                     */

                    CheckNegPlim(Axis[i].Position.LimitedCommand, Axis[i].Position.PlimMin) ;
                    Axis[i].Position.LimitedToSystemDistanceMaxNeg = labs(DistanceToLimmit);



                    ///*************FOR DEBUG TEST ONLY***************
                    if( MaxPos<Axis[0].Feedback.IntegratedDegPosition )MaxPos= Axis[0].Feedback.IntegratedDegPosition;
                    if( MinPos>Axis[0].Feedback.IntegratedDegPosition )MinPos= Axis[0].Feedback.IntegratedDegPosition;
                    //*****************************************************************

                    Axis[i].Position.LimitedToSystemDistance =Axis[i].Position.SystemCommand- Axis[i].Position.LimitedCommand ;
                    OverFlowPosDerivOnDeg(&Axis[i].Position.LimitedToSystemDistance);

                    Axis[i].Position.LimitedToSystemDistance=CustomIQsat(Axis[i].Position.LimitedToSystemDistance, Axis[i].Position.LimitedToSystemDistanceMaxPos, -Axis[i].Position.LimitedToSystemDistanceMaxNeg);
                    /*
                   if((Axis[i].Position.PlimMax==Axis[i].Position.PlimMin))
                    {
                        Axis[i].Position.LimitedToSystemDistance=0;
                    }
                     */
                }


                /*
                if(((float32_t)Axis[i].Position.Pe*Axis[i].Position.LimitedToSystemDistance)<0)
                {
                    Axis[i].Position.LimitedCommand +=CustomIQsat(Axis[i].Position.LimitedToSystemDistance,Max_PE_in_Jog,-Max_PE_in_Jog);
                }
                else if((Axis[i].Position.AbsPe<Max_PE_in_Jog ))
                {
                    Axis[i].Position.LimitedCommand +=CustomIQsat(Axis[i].Position.LimitedToSystemDistance,(Max_PE_in_Jog-Axis[i].Position.AbsPe),-(Max_PE_in_Jog-Axis[i].Position.AbsPe));

                }
                 */

                if( Axis[i].Position.PosLoopSaturated)
                {
                Axis[i].Position.PosCommandSaturated=1;
                }
                else
                {
                    Axis[i].Position.PosCommandSaturated=0;

                }



                if(Axis[i].Position.LimitedToSystemDistance>(MaxSystemPe>>6))
                {
                    Axis[i].Position.LimitedToSystemDistance=(MaxSystemPe>>6);
                    Axis[i].Position.PosCommandSaturated=1;
                }
                else if(Axis[i].Position.LimitedToSystemDistance<-(MaxSystemPe>>6))
                {
                    Axis[i].Position.LimitedToSystemDistance=-(MaxSystemPe>>6);
                    Axis[i].Position.PosCommandSaturated=1;

                }

                Axis[i].Position.LimitedToSystemDistanceAcc=Axis[i].Position.LimitedToSystemDistance-Axis[i].Position.LimitedToSystemDistanceOut;

                if (Axis[i].Position.LimitedToSystemDistanceAcc*Axis[i].Position.LimitedToSystemDistance>0) //acceleration
                {
                    if(Axis[i].Position.LimitedToSystemDistanceAcc> Axis[i].Ptp.ScaledAcc_f*4)
                    {
                        Axis[i].Position.LimitedToSystemDistanceAcc =  Axis[i].Ptp.ScaledAcc_f*4;
                    }
                    if(Axis[i].Position.LimitedToSystemDistanceAcc<-Axis[i].Ptp.ScaledAcc_f*4)
                    {
                        Axis[i].Position.LimitedToSystemDistanceAcc =  -Axis[i].Ptp.ScaledAcc_f*4;
                    }
                }

                Axis[i].Position.LimitedToSystemDistanceOut+=  Axis[i].Position.LimitedToSystemDistanceAcc;
                Axis[i].Position.LimitedCommand +=(Axis[i].Position.LimitedToSystemDistanceOut);




                OverFlowPosOnDeg(&Axis[i].Position.LimitedCommand);
/*
                temp32b =Axis[i].Position.SystemCommand- Axis[i].Position.LimitedCommand ;
                OverFlowPosDerivOnDeg(&temp32b);
                if(temp32b> Axis[i].Position.FOV)//limit position so it wont move out of the screen
                {
                    Axis[i].Position.SystemCommand = Axis[i].Position.LimitedCommand +Axis[i].Position.FOV;
                }
                if(temp32b<- Axis[i].Position.FOV)
                {
                    Axis[i].Position.SystemCommand = Axis[i].Position.LimitedCommand -Axis[i].Position.FOV;
                }

                OverFlowPosOnDeg(&Axis[i].Position.SystemCommand);
*/
                Axis[i].Velocity.CommandPtr = &Axis[i].Position.Output;
                Axis[i].Serial.Vcmd=Axis[i].Velocity.Command ;
                Axis[i].Current.CommandPtr = &Axis[i].Velocity.Output;


                break;

            }
        }


        switch (ExecutionSlot4)
        {

        case 0:
            break;
        case 1:
            CalcVel_func(i);

            VelLoop_func(i);
            break;
        case 2:
            PTPGeneratorFPUFunc(i);

            posloopdevider++;
            if(posloopdevider>0)
            {
                Axis[i].Position.Output = PosLoopFunc(i,Axis[i].Position.LimitedCommand, Axis[i].Feedback.IntegratedDegPosition);
                posloopdevider=0;
            }

            break;
        case 3:
            CalcVel_func(i);

            VelLoop_func(i);
            break;


        }
        HallsHandlerfunc(i);
        EnableInterupts(0);

        Test_startTime = GetCpuTimer1Value();


        CurrLoopDQ_Func(i);
        Test_endTime = GetCpuTimer1Value();

        Test_runTime = (Test_startTime - Test_endTime) *0.0111083984375;//in usec
        EnableInterupts(1);
        if(MaxTest_runTime<Test_runTime)MaxTest_runTime=Test_runTime;





        if (Axis[i].Drive.State == enable )
        {


            HWD_EnablePwm(i);

            Axis[i].Current.Phase_A_output =  CustomIQsat(Axis[i].Current.Phase_A_output,(int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);
            Axis[i].Current.Phase_B_output = CustomIQsat(Axis[i].Current.Phase_B_output,(int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);
            Axis[i].Current.Phase_C_output =  CustomIQsat(Axis[i].Current.Phase_C_output,(int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);

        }
        else if  ( (Axis[i].Drive.State == fault) || ( Axis[i].Drive.State == disable ) )
        {
            HWD_DisablePwm(i);
            Axis[i].Position.LimitedToSystemDistanceOut=0;
            Axis[i].Current.Commutation_timer=1000000;
            Axis[i].Current.Command=0;
            //	Axis[i].Feedback.GyroItegrated=0;
            Axis[i].Feedback.GyroShiftedoutItegrated=0;
            Axis[i].Position.JogInPosVelocityOutputFix=0;
            Axis[i].Serial.Vcmd=0;
            Axis[i].Serial.Jcmd=0;
            Axis[i].Serial.Icmd=0;
            Axis[i].Position.LimitedCommand= Axis[i].Feedback.IntegratedDegPosition;
            Axis[i].Position.LastCommand =Axis[i].Position.LimitedCommand;
            Axis[i].Position.SystemCommand= Axis[i].Position.LimitedCommand;
            Axis[i].Ptp.EndCommand = Axis[i].Position.SystemCommand;
            Axis[i].Ptp.Output=0;
            Axis[i].Ptp.DistanceLeft_f=0;
            Axis[i].Position.JogInPosVelocityOutput=0;
            Axis[i].Position.JogInPosAcceleration=0;

            Axis[i].Ptp.Vel_f=0;
            Axis[i].Current.Command=0;
            Axis[i].Velocity.Command=0;
            Axis[i].Ptp.Command =0;
            Axis[i].Position.Output=0;
            Axis[i].Position.PeInteg=0;
            Axis[i].Velocity.Param.ErrorInteg=0;
            Axis[i].Current.Phase_A_output = 0;
            Axis[i].Current.Phase_B_output = 0;
            Axis[i].Current.Phase_C_output = 0;
            Axis[i].Current.Commutation_timer=4000;
            Axis[i].Current.Effort=0;
            Axis[i].Feedback.GyroOffset =Axis[i].Feedback.BaseGyroPos ;


        }

        if( Axis[i].BuidInTest.State == IdleBIT) //allow BIT to take over PWM to analize the phases
        {
            PWM_out(i,1,(uint16_t)(PWM_TBPRD)+Axis[i].Current.Phase_A_output);
            PWM_out(i,2,(uint16_t)(PWM_TBPRD)+Axis[i].Current.Phase_B_output);
            PWM_out(i,3,(uint16_t)(PWM_TBPRD)+Axis[i].Current.Phase_C_output);
        }
    }


    writeSCIB();
    Record_func();



    endtime = GetCpuTimer1Value();

    runTime = (startTime - endtime) *0.0111083984375;//in usec


    if(MaxRealTimeExecutionTime[ExecutionSlot4]<runTime)
    {
        MaxRealTimeExecutionTime[ExecutionSlot4] = runTime;
    }
 /*   if(ExecutionSlot4==2)
    {
        if(UserIntRunning)
        {
            UserIntMissed ++;
        }
        else
        {
            TriggerLowPriorityInterupt();
            UserInt1Func();
        }
    }

*/
}



int16_t aditest=0;
void I2T_Handle(int16_t a);

#if TI_DSP_Pragma
__interrupt
#endif
void UserInt1Func(void *data, uint8_t TmrCtrNumber)

{
    static int16_t test=0;
    int16_t i;
    float32_t tempf, tempf1,tempf2;

    UserIntRunning=1;
    SetCpuTimer1Value( 0xFFFFFFFF);
    LPI_startTime =GetCpuTimer1Value();
    EnableInterupts(1);

    AlowSCIBandLowPriorityInterupts();

    ReadSCIA();

    HandleSCIB();


    if(CPLD_SPI_Data_Recieved_Flag)
    {
        Handle_SPI_Data();
        CPLD_SPI_Data_Recieved_Flag=0;
        Handle_Stabilization();

    }


    for(i=0;i<NumberOfAxes;i++)
    {



        Axis[i].Drive.VBus =GetBusVoltage();
        Axis[i].Drive.VLogic =GetLogicVoltage();
        tempf1= Axis[i].Current.SinComponent*Axis[i].Current.SinComponent;
        tempf2=Axis[i].Current.CosComponent*Axis[i].Current.CosComponent;
        tempf= sqrtf(tempf1+ tempf2 );

        Axis[i].Current.I= (int16_t)tempf;
        if(BufferOF) tempf++;

        I2T_Handle(i);

        FaultHandler_func(i);
        WarningHandler_func(i);
    }


    if(EEPROM_Delay>0)EEPROM_Delay--;
    else EEPROM_Delay=0;




    test++;

    LPI_endtime = GetCpuTimer1Value();

    LPI_runTime = (LPI_startTime - LPI_endtime) *0.0111083984375;//in usec
    if(MaxLPI_runTime<LPI_runTime)MaxLPI_runTime=LPI_runTime;

    AcknowledgeLowPriorityInterupt();



    UserIntRunning =0;



}




int32_t CustomIQsat(int32_t d, int32_t max, int32_t min) {
    const int32_t t = d < min ? min : d;
    return t > max ? max : t;
}


float32_t f_CustomIQsat(float32_t a, float32_t max, float32_t min)
{
    if(a>max) a=max;
    if(a<min) return min;
    else return a;

}

void OverFlowPosOnDeg (int32_t * d)
{

    if(PositionOverflowValue!=0)
    {
        if(*d   > PositionOverflowValue)
        {
            *d -= PositionOverflowValue;
        }
        if(*d   <      0)
        {
            *d += PositionOverflowValue;
        }
    }

}

void OverFlowPosDerivOnDeg (int32_t * d)
{
    if(PositionOverflowValue!=0)
    {
        if(*d   > PositionOverflowValue)
        {
            *d -= PositionOverflowValue;
        }
        if(*d   <      0)
        {
            *d += PositionOverflowValue;
        }


        if(  *d> (PositionOverflowValue/2))
        {
            *d -= PositionOverflowValue;
        }
        if( *d<-(PositionOverflowValue/2))
        {
            *d += PositionOverflowValue;
        }
    }
}

int16_t CheckPosPlim(int32_t Position,int32_t PosPlim)
{
    if(PosPlim==-1)
    {
        DistanceToLimmit = (int32_t)(PositionOverflowValue/2);

        return 0;
    }
    DistanceToLimmit= Position+ACS_LimitProximity-PosPlim;
    OverFlowPosDerivOnDeg(&DistanceToLimmit);
    if(DistanceToLimmit<0) return 0;
    else
    {
        DistanceToLimmit=0;
        return 1;
    }

}


int16_t CheckNegPlim(int32_t Position,int32_t NegPLim)
{

    if(NegPLim==-1)
    {
        DistanceToLimmit = (int32_t)(PositionOverflowValue/2);

        return 0;

    }
    DistanceToLimmit= Position-ACS_LimitProximity-NegPLim;
    OverFlowPosDerivOnDeg(&DistanceToLimmit);
    if(DistanceToLimmit>0) return 0;
    else
    {
        DistanceToLimmit=0;

        return 1;
    }

}
