
#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(PTPGeneratorFPUFunc, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(PTPGeneratorFPUFunc);
#endif
void PTPGeneratorFPUFunc(int16_t a)
{
    //Int64 breakDistance = 0;
    int16_t   direction;
    static int32_t PrevOut[NumberOfAxes]={0,0};
    float32_t breakDistance_f;
    float32_t currentAcc[NumberOfAxes]={0,0};




    OverFlowPosDerivOnDeg(&Axis[a].Ptp.Command);


    if(Axis[a].Ptp.State !=  PTP_INIT )
    {
        Axis[a].Ptp.DistanceLeft_f+=Axis[a].Ptp.Command;
        Axis[a].Ptp.EndCommand-=    Axis[a].Ptp.Command;

        OverFlowPosOnDeg(&Axis[a].Ptp.EndCommand);



        Axis[a].Ptp.Command=0;



        if (Axis[a].Ptp.DistanceLeft_f >0)
        {
            direction = 1;
            Axis[a].Ptp.State = PTP_IN_MOTION;
        }
        else if(Axis[a].Ptp.DistanceLeft_f < 0)
        {
            direction = -1;
            Axis[a].Ptp.State = PTP_IN_MOTION;
        }
        else
        {
            direction = 0;
        }
    }
    switch (Axis[a].Ptp.State)
    {
    case PTP_IN_MOTION:

        breakDistance_f  =fabsf((float32_t) Axis[a].Ptp.Vel_f*Axis[a].Ptp.Vel_f / (Axis[a].Ptp.ScaledDec_f*2) );

        if (((breakDistance_f >= (Axis[a].Ptp.DistanceLeft_f * direction) )|| ( Axis[a].Ptp.Vel_f * direction <0)))
        {
            currentAcc[a] = 0 - Axis[a].Ptp.Vel_f;
            currentAcc[a] =  f_CustomIQsat(currentAcc[a],Axis[a].Ptp.ScaledDec_f,-Axis[a].Ptp.ScaledDec_f);
        }
        else
        {

            if(breakDistance_f<0)
            {
                breakDistance_f=0;
            }

            if ((breakDistance_f <= (Axis[a].Ptp.DistanceLeft_f* direction) ))
            {
                currentAcc[a] = Axis[a].Ptp.ScaledVCruise_f* direction - Axis[a].Ptp.Vel_f;
                currentAcc[a] =  f_CustomIQsat(currentAcc[a],Axis[a].Ptp.ScaledAcc_f,-Axis[a].Ptp.ScaledAcc_f);

            }
            else
            {

                currentAcc[a] = 0;//Axis[a].Ptp.ScaledVCruise_f* direction - Axis[a].Ptp.Vel_f;
                currentAcc[a] =  f_CustomIQsat(currentAcc[a],Axis[a].Ptp.ScaledDec_f,-Axis[a].Ptp.ScaledDec_f);

            }
        }

        if(fabsf(Axis[a].Ptp.DistanceLeft_f) <=1  )
        {
            Axis[a].Ptp.State = PTP_IDLE;
            Axis[a].Ptp.DistanceLeft_f=0;
            Axis[a].Ptp.Vel_f = 0;
            Axis[a].Ptp.FF_ACC=0;
        }
        else
        {
            Axis[a].Ptp.Vel_f += currentAcc[a];

            Axis[a].Ptp.FF_ACC = currentAcc[a];
            PrevOut[a] = (int32_t)(Axis[a].Ptp.DistanceLeft_f);
            if(( Axis[a].Ptp.Vel_f * direction >0))
            {
                if(Axis[a].Ptp.Vel_f>0 && Axis[a].Ptp.Vel_f > Axis[a].Ptp.DistanceLeft_f)
                    {
                    Axis[a].Ptp.Vel_f = Axis[a].Ptp.DistanceLeft_f;
                    }
                else if(Axis[a].Ptp.Vel_f<0 && Axis[a].Ptp.Vel_f < Axis[a].Ptp.DistanceLeft_f)
                    {
                    Axis[a].Ptp.Vel_f = Axis[a].Ptp.DistanceLeft_f;
                    }
            }

            if(((Axis[a].Position.LimitedToSystemDistance>Max_PE_in_Jog) &&(Axis[a].Ptp.Vel_f>0))
                    ||((Axis[a].Position.LimitedToSystemDistance<-Max_PE_in_Jog) &&(Axis[a].Ptp.Vel_f<0)))
            {
                   // dont advance position command if stuck in limits
            }
            else
            {
                Axis[a].Ptp.DistanceLeft_f = Axis[a].Ptp.DistanceLeft_f -(Axis[a].Ptp.Vel_f);
            }
        }
        Axis[a].Ptp.Output =(int32_t)(Axis[a].Ptp.DistanceLeft_f)-PrevOut[a];


        break;


    case PTP_IDLE:

        currentAcc[a] = 0;
        Axis[a].Ptp.FF_ACC = currentAcc[a];
        Axis[a].Ptp.DistanceLeft_f=0;

        Axis[a].Ptp.Vel_f = 0;
        Axis[a].Ptp.Output =0;
        PrevOut[a]=0;

        Axis[a].Ptp.FF_ACC=0;
        Axis[a].Position.SystemCommand = Axis[a].Ptp.EndCommand;
        break;

    case PTP_INIT:

        Axis[a].Ptp.DistanceLeft_f=0;
        Axis[a].Position.SystemCommand = Axis[a].Feedback.IntegratedDegPosition;
        Axis[a].Ptp.EndCommand=Axis[a].Position.SystemCommand ;
        Axis[a].Ptp.State = PTP_IN_MOTION;
        Axis[a].Ptp.Output =0;
        PrevOut[a]=0;
        break;
    }

}
