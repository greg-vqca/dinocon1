
#include "GlobalIncludes.h"
/*The variables t1, t2 and t3, representing switching duty-cycle 
ratios of the respective 3-phase system
t_1 and t_2 are duty-cycle ratios of basic space vectors, given for the respective sector

 */
#define 	t1  (-(t_1+t_2)>>1)
#define 	t2	(t1+t_1)
#define 	t3	 (t2+t_2)



int32_t Uref1[NumberOfAxes];
int32_t Uref2[NumberOfAxes];
int32_t Uref3[NumberOfAxes];
int16_t sector[NumberOfAxes];
int16_t Support;
int16_t AdiTest3;
long Runtime;
float32_t   direct_part ,quadr_part;
float32_t Qterm_Error ,Dterm_Error ;
float32_t qError,dError , Ub ,Ua, ia,ic;
//static float32_t ErrorVector;

int32_t    t_1,   t_2;

int16_t Current_Amp;
int32_t X;

int32_t Y;
int32_t Z;
int32_t D_ProportionalPart, Q_ProportionalPart,D_IntegralPart,Q_IntegralPart;
#if TI_DSP_Pragma ==1
#pragma CODE_SECTION(CurrLoopDQ_Func, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(CurrLoopDQ_Func);
#endif

void CurrLoopDQ_Func(int16_t a)
{

    float32_t adaptiveP,adaptiveI;


    static float32_t id_error_integrator[NumberOfAxes]={0,0},iq_error_integrator[NumberOfAxes]={0,0};


    Axis[a].Current.Command= *Axis[a].Current.CommandPtr;
    if(     Axis[a].Current.ForcedCommutFlag)   Axis[a].Current.Command=Axis[a].Serial.Icmd;

    ia = GetCurrentReadingPhaseA(a);
    ic = GetCurrentReadingPhaseC(a);

    Axis[a].Current.Ia =( ia - Axis[a].Current.Ia_OffSet) ;
    Axis[a].Current.Ic =-( ic - Axis[a].Current.Ic_OffSet);
    Axis[a].Current.Ib = (-Axis[a].Current.Ia-Axis[a].Current.Ic);



    //optional filter on current command
    /*if((HWD_AntiBacklash_On_Output && a) ==1)
		Current_Amp=CustomIQsat((int32_t)Axis[a].Current.Command -Axis[a].Current.AntiBachlashCurrent, (int32_t)Axis[a].Current.Imax, -(int32_t)Axis[a].Current.Imax);//(int32_t)((int32_t)Current_Amp*16 +(int32_t)Current_CMD*16)>>5;

else*/

    Current_Amp=CustomIQsat((int32_t)Axis[a].Current.Command, (int32_t)Axis[a].Current.Imax, -(int32_t)Axis[a].Current.Imax);//(int32_t)((int32_t)Current_Amp*16 +(int32_t)Current_CMD*16)>>5;

    adaptiveP =Axis[a].Current.Param.f_GpAdap * Axis[a].Velocity.MotorEncVfbAbs;
    adaptiveI =Axis[a].Current.Param.f_GiAdap * Axis[a].Velocity.MotorEncVfbAbs ;

    Axis[a].Motor.PhaseAdvance =   ((int64_t)Axis[a].Motor.PhaseAdvanceScale*Axis[a].Velocity.MotorEncVfb>>16);

    Axis[a].Motor.PhaseAdvance  = CustomIQsat( Axis[a].Motor.PhaseAdvance , Axis[a].Motor.PhaseAdvanceLim,-Axis[a].Motor.PhaseAdvanceLim);
    Axis[a].Motor.ComutationAngle=Axis[a].Feedback.MechPos*(Axis[a].Motor.Poles>>1);
    Axis[a].Current.Phase = Axis[a].Motor.ComutationAngle +Axis[a].Motor.Phaseoffset+(Axis[a].Motor.Phase*182)+(Axis[a].Motor.PhaseAdvance*182);// 182*360 =  2^16
    if(     Axis[a].Current.ForcedCommutFlag)      Axis[a].Current.Phase = Axis[a].Current.ForcedCommutCntr>>16;
    //*****************************************************************************************
    //separate the phase into sin and cosin
    Axis[a].Motor.SinComponent = (sin_table[((uint16_t) Axis[a].Current.Phase +63)>>6]); //sine
    Axis[a].Motor.CosComponent = (sin_table[((uint16_t) Axis[a].Current.Phase +63+(90*182))>>6]);//cosine


    Axis[a].Current.SinComponent =	Axis[a].Current.Ia ; //current_Y
    Axis[a].Current.CosComponent = 0.57735027 * (-Axis[a].Current.Ia-(Axis[a].Current.Ic*2)) ;

    direct_part =Axis[a].Motor.SinComponent * Axis[a].Current.CosComponent + Axis[a].Motor.CosComponent * Axis[a].Current.SinComponent;

    quadr_part = Axis[a].Motor.CosComponent * Axis[a].Current.CosComponent - Axis[a].Motor.SinComponent * Axis[a].Current.SinComponent;




    ///*********************** current control loop DQ ***********************//
    // q is the phase of the current vector and should be with on
    //the principal x axis thus it is controlled to zero
    Qterm_Error=0-quadr_part;

    Q_ProportionalPart = Qterm_Error * (Axis[a].Current.Param.f_Gp+adaptiveP);
    Q_IntegralPart = iq_error_integrator[a]*(Axis[a].Current.Param.f_Gi+adaptiveI);


    qError=Q_ProportionalPart+Q_IntegralPart ;



    Dterm_Error=Current_Amp-direct_part;

    D_ProportionalPart = Dterm_Error * (Axis[a].Current.Param.f_Gp+adaptiveP);
    D_IntegralPart =  id_error_integrator[a]*(Axis[a].Current.Param.f_Gi+adaptiveI);


    dError=D_ProportionalPart+D_IntegralPart;

    // now transform back to Uref1 and Uref2 asses of the stator
    //Uref2= Cos * D - Sin * Q
    //Uref1=Sin*D + Cos * Q
    Ua=Axis[a].Motor.CosComponent* dError - Axis[a].Motor.SinComponent * qError;
    Ub=Axis[a].Motor.SinComponent* dError + Axis[a].Motor.CosComponent * qError;




    //DQ based on:
    //  http://www.freescale.com/files/dsp/doc/app_note/AN3301.pdf

    //transform 90->120 coordinates
    Uref1[a] = Ub;
    //	Uref2 = 1/2 * (-Ub + sqrt(3)* Ua);
    Uref2[a]  =  Ua * 0.866 - Ub * 0.5 ;
    //	Uref3 = 1/2 * (-Ub - sqrt(3)* Ua);
    Uref3[a]  =(int32_t) -Ub - Uref2[a];

    /*
	Three auxiliary variables:
	X = Ub
	Y= 1/2 (Ub + sqrt(3) *Ua)
	Z= 1/2 (Ub - sqrt(3) * Ua)
     */

    X = Uref1[a];
    Y = Uref2[a] + Uref1[a];
    Z = X-Y;



    //Sector Identification Tree

    if (Uref3[a]>0)
    {
        if(Uref2[a]>0) sector[a] = 5;
        else
        {
            if(Uref1[a]>0) sector[a] = 3;
            else sector[a] = 4;
        }
    }
    else
    {
        if(Uref2[a]>0)
        {
            if(Uref1[a]>0) sector[a] = 1;
            else sector[a] = 6;
        }
        else sector[a] = 2;
    }



    /*
	  Sectors 	U0,U60 	U60,U120 	U120,U180 	U180,U240 	U240,U300 	U300,U0
					1		2			3			4			5			6
Determination of the Expressions t_1 and t_2 for All Sectors
Sectors 	1 		2 		3 		4 		5 		6
	t_1		X	 	Z 		-Y 		-X 		-Z 		Y
	t_2 	-Z 		Y 		X 		Z 		-Y 		-X




// Assignment of the Duty-Cycle Ratios to the Right Motor Phase
Sectors 	1 		2 		3 		4 		5 		6
pwm_a		t3 		t2 		t1 		t1 		t2 		t3
pwm_b 		t2 		t3 		t3 		t2 		t1 		t1
pwm_c 		t1 		t1 		t2 		t3 		t3 		t2

     */

    switch (sector[a])
    {


    case 1:
    {
        t_1=X;
        t_2=-Z;

        Axis[a].Current.Phase_A_output=t3;
        Axis[a].Current.Phase_B_output=t2;
        Axis[a].Current.Phase_C_output=t1;

        break;
    }
    case 2:
    {
        t_1=Z;
        t_2=Y;
        Axis[a].Current.Phase_A_output=-t2;
        Axis[a].Current.Phase_B_output=t3;
        Axis[a].Current.Phase_C_output=t1;
        break;
    }
    case 3:
    {
        t_1=-Y;
        t_2=X;
        Axis[a].Current.Phase_A_output=t1;
        Axis[a].Current.Phase_B_output=t3;
        Axis[a].Current.Phase_C_output=t2;
        break;
    }
    case 4:
    {
        t_1=-X;
        t_2=Z;
        Axis[a].Current.Phase_A_output=t1;
        Axis[a].Current.Phase_B_output=-t2;
        Axis[a].Current.Phase_C_output=t3;
        break;
    }

    case 5:
    {
        t_1=-Z;
        t_2=-Y;
        Axis[a].Current.Phase_A_output=t2;
        Axis[a].Current.Phase_B_output=t1;
        Axis[a].Current.Phase_C_output=t3;
        break;
    }
    case 6:
    {
        t_1=Y;
        t_2=-X;
        Axis[a].Current.Phase_A_output=t3;
        Axis[a].Current.Phase_B_output=t1;
        Axis[a].Current.Phase_C_output=-t2;

        break;
    }
    }


    if  ( (Axis[a].Drive.State == fault) || ( Axis[a].Drive.State == disable ) )
    {
        id_error_integrator[a]=0;
        iq_error_integrator[a]=0;
    }



    //calculate the magnitude of the error vector
    // ErrorVector=Ub * Ub + Ua * Ua;
    if ((abs(Axis[a].Current.Phase_A_output)> (int32_t)HWD_MAX_PWM )||(abs(Axis[a].Current.Phase_B_output) > (int32_t)HWD_MAX_PWM )||(abs(Axis[a].Current.Phase_C_output) > (int32_t)HWD_MAX_PWM ))
    {

        if(( (id_error_integrator[a] * Dterm_Error)<0 ))//if error and integrator are of difirent sign
        {
            id_error_integrator[a]+=((int32_t)Dterm_Error);
        }
        if((( iq_error_integrator[a] * Qterm_Error) <0))//if error and integrator are of difirent sign
        {
            iq_error_integrator[a]+=((int32_t)Qterm_Error);
        }
        Axis[a].CurrLoopSaturated=1;//set current loop saturated flag on for the velocity loop
    }
    else
    {
        if(labs(Q_ProportionalPart)< labs(Q_IntegralPart) )
        {

            if((( iq_error_integrator[a] * Qterm_Error) <0))//if error and integrator are of difirent sign
            {
                iq_error_integrator[a]+=((int32_t)Qterm_Error);
            }
            else
            {
                iq_error_integrator[a] -= iq_error_integrator[a]/1000;
            }
        }
        else
        {
            iq_error_integrator[a]+=((int32_t)Qterm_Error);

        }

        if(labs(D_ProportionalPart)< labs(D_IntegralPart) )
        {
            if((( id_error_integrator[a] * Dterm_Error) <0))//if error and integrator are of difirent sign
            {
                id_error_integrator[a]+=((int32_t)Dterm_Error);
            }
            else
            {
                id_error_integrator[a]-= id_error_integrator[a]/1000;
            }
        }
        else
        {

            id_error_integrator[a]+=((int32_t)Dterm_Error);
        }
        //  if(abs(Axis[a].Current.Command) > Axis[a].Current.Imax*2) Axis[a].CurrLoopSaturated=1;
        Axis[a].CurrLoopSaturated=0;
    }
    //limit the error integrator

    id_error_integrator[a]=f_CustomIQsat(id_error_integrator[a], 65536,-65536);
    //limit the integrator error
    iq_error_integrator[a]=f_CustomIQsat(iq_error_integrator[a], 65536,-65536);




    //saturation of the PWM values to maximum PWM duty cycle and output it
    Axis[a].Current.Phase_A_output=CustomIQsat((int32_t)Axis[a].Current.Phase_A_output, (int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);
    Axis[a].Current.Phase_B_output=CustomIQsat((int32_t)Axis[a].Current.Phase_B_output, (int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);
    Axis[a].Current.Phase_C_output=CustomIQsat((int32_t)Axis[a].Current.Phase_C_output, (int32_t)HWD_MAX_PWM,-(int32_t)HWD_MAX_PWM);

}
