

#include "GlobalIncludes.h"


#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(Handle_SPI_Data, "ramfuncs");

#pragma FUNC_ALWAYS_INLINE(Handle_SPI_Data);
#endif

CPLD_STATUS_tag          CPLD_Status;

void SPIB_Transmit(uint16_t cmdData);

int32_t SPI_RX_DATA[SPI_TO_CPLD_LENGHT+1];


int32_t SPI_test=0;



volatile int32_t LocalSPI_RX_DATA[2*SPI_TO_CPLD_LENGHT+1];




int32_t GyroOffsetToBaseAngle=60000;


void Handle_SPI_Data(void)
{


    int32_t temp32;
    float32_t temp32f;


    /*
     * 0. CAFE
     * 1. CAFE
     * 2. SSI Motor2 HI
     * 3. SSI Motor2 LOW
     * 4. SSI Motor1 HI
     * 5. SSI Motor1 LOW
     * 6. ACC Y
     * 7. ACC X
     * 8. YAW rate
     * 9. roll rate
     * 10. pitch rate
     * 11. heading
     * 12. roll
     * 13. pitch
     * 14. Status
     * 15. VN reserved
     * 16. vn yaw Hi
     * 17  vn yaw low
     * 18. vn pithc hi
     * 19. vn pitch low
     * 20. vn roll hi
     * 21. vn roll low
     * 22. vn quturnion hi
     * 23. vn quaturnion low
     * 24 vn quturnion hi
     * 25 vn quaturnion low
     * 26 vn quturnion hi
     * 27 vn quaturnion low
     * 28 vn quturnion hi
     * 29 vn quaturnion low
     * 30 vn Acc X low
     * 31 vn Acc X hi
     * 32 vn Acc Y low
     * 33 vn Acc Y hi
     * 34 vn Acc Z low
     * 35 vn Acc Z hi
     * 36 vn INS status
     * 37 vn gnss fix 8 bit 8 reserved
     * 38 VN  16 reserved
     * 39 16 reserved
     * 40 checksum
     */


    // fix all gyro reading to 0 ... 360


    Stabilization.BaseGyro.ACC_X = LocalSPI_RX_DATA[6];
    Stabilization.BaseGyro.ACC_Y = LocalSPI_RX_DATA[7];


    LocalSPI_RX_DATA[11]= LocalSPI_RX_DATA[11]+((float)((int16_t)LocalSPI_RX_DATA[8])*0.05);
    LocalSPI_RX_DATA[12]= (int16_t)LocalSPI_RX_DATA[12]+((float)((int16_t)LocalSPI_RX_DATA[9])*0.06);
    LocalSPI_RX_DATA[13]= (int16_t)LocalSPI_RX_DATA[13]+((float)((int16_t)LocalSPI_RX_DATA[10])*0.06);



    Stabilization.BaseGyro.Rate.pitch= (int32_t)((int16_t)LocalSPI_RX_DATA[9])*10;
    Stabilization.BaseGyro.Rate.yaw= (int32_t)((int16_t)LocalSPI_RX_DATA[8])*10;
    Stabilization.BaseGyro.Rate.roll= (int32_t)((int16_t)LocalSPI_RX_DATA[10])*10;


    Axis[0].Feedback.Load.EncPositionStatus =       (LocalSPI_RX_DATA[5] & 0xFFF);

    Axis[1].Feedback.Load.EncPositionStatus =       (LocalSPI_RX_DATA[3] & 0xFFF);


    if(Axis[0].Feedback.Load.EncPositionStatus==0)
    {
        Axis[0].Feedback.Load.ShadowEncPosition=   (int64_t) ((int32_t)1<<20) -  ((LocalSPI_RX_DATA[4]<<4)+(LocalSPI_RX_DATA[5]>>12));
        Axis[0].Feedback.Load.EncPosition= Axis[0].Feedback.Load.ShadowEncPosition;
        AbsEncFirstReadingRecievedFlag[0]=1;
    }

    if(Axis[1].Feedback.Load.EncPositionStatus==0)
    {
        Axis[1].Feedback.Load.ShadowEncPosition=       (LocalSPI_RX_DATA[2]<<4)+(LocalSPI_RX_DATA[3]>>12);
        Axis[1].Feedback.Load.EncPosition=Axis[1].Feedback.Load.ShadowEncPosition;
        AbsEncFirstReadingRecievedFlag[1]=1;
    }


    CPLD_Status = (CPLD_STATUS_tag) LocalSPI_RX_DATA[14];


    if((CPLD_Status & CPLD_STATUS_BaseGyro_flt))
    {


        Stabilization.GyroStabilization = 0;
        Stabilization.GyroStabilizationWarning=1;

    }
    else
    {
        Stabilization.GyroStabilizationWarning=0;

    }

    if((CPLD_Status & CPLD_STATUS_VN_Data_flt))
    {


        Axis[0].Feedback.VN_Warning=1;
        Axis[1].Feedback.VN_Warning=1;
        Axis[0].Feedback.VN_GPS_StabilizationDelay=5000;
        Axis[1].Feedback.VN_GPS_StabilizationDelay=5000;


    }
    else
    {
        Axis[0].Feedback.VN_Warning=0;
        Axis[1].Feedback.VN_Warning=0;
    }


    if((CPLD_Status & CPLD_STATUS_VN_Data_flt) && ((VN_INS_Status.Bits.GNSS_Fix==0)|| (VN_INS_Status.Bits.GNSS_Compas==0)||(VN_INS_Status.Bits.GNSS_HeadingIns==0)||(VN_INS_Status.Bits.mode!=2)))
    {


        Axis[0].Feedback.VN_GPS_StabilizationDelay=5000;
        Axis[1].Feedback.VN_GPS_StabilizationDelay=5000;


    }


    if( Axis[0].Feedback.VN_GPS_StabilizationDelay>0)
    {
        Axis[0].Feedback.VN_GPS_StabilizationDelay--;
    }
    if( Axis[1].Feedback.VN_GPS_StabilizationDelay>0)
    {
        Axis[1].Feedback.VN_GPS_StabilizationDelay--;
    }

    temp32= (float)LocalSPI_RX_DATA[11]*100;
    OverFlowPosOnDeg(&temp32);
    Stabilization.BaseGyro.Angle.yaw  = temp32;

    temp32=  -(float)LocalSPI_RX_DATA[13]*100;
    OverFlowPosOnDeg(&temp32);
    Stabilization.BaseGyro.Angle.roll=temp32;

    temp32=  (float)LocalSPI_RX_DATA[12]*100;
    OverFlowPosOnDeg(&temp32);
    Stabilization.BaseGyro.Angle.pitch =temp32;


    /*
     * 16. vn yaw Hi
     * 17  vn yaw low
     * 18. vn pithc hi
     * 19. vn pitch low
     * 20. vn roll hi
     * 21. vn roll low
     * 22. vn quturnion hi
     * 23. vn quaturnion low
     * 24 vn quturnion hi
     * 25 vn quaturnion low
     * 26 vn quturnion hi
     * 27 vn quaturnion low
     * 28 vn quturnion hi
     * 29 vn quaturnion low
     * 30. vn Acc X low
     * 31 vn Acc X hi
     * 32 vn Acc Y low
     * 33 vn Acc Y hi
     * 34 vn Acc Z low
     * 35 vn Acc Z hi
     * 36 vn INS status
     * 37 vn gnss fix 8 bit 8 reserved
     */
 /*   temp32 = (((uint32_t)LocalSPI_RX_DATA[17]<<16)|LocalSPI_RX_DATA[16]);
    temp32f = *((float32_t*)&temp32);
    VN_Angles.yaw= temp32f;

    temp32 = (((uint32_t)LocalSPI_RX_DATA[19]<<16)|LocalSPI_RX_DATA[18]);
    temp32f = *((float32_t*)&temp32);
    VN_Angles.pitch= temp32f;

    temp32 = (((uint32_t)LocalSPI_RX_DATA[21]<<16        )|LocalSPI_RX_DATA[20]);
    temp32f = *((float32_t*)&temp32);
    VN_Angles.roll= temp32f;



    temp32 = (((uint32_t)LocalSPI_RX_DATA[23]<<16)|LocalSPI_RX_DATA[22]);
    temp32f = *((float32_t*)&temp32);
    Quaternion_VN.x= temp32f;


    temp32 = (((uint32_t)LocalSPI_RX_DATA[25]<<16)|LocalSPI_RX_DATA[24]);
    temp32f = *((float32_t*)&temp32);
    Quaternion_VN.y= temp32f;


    temp32 = (((uint32_t)LocalSPI_RX_DATA[27]<<16)|LocalSPI_RX_DATA[26]);
    temp32f = *((float32_t*)&temp32);
    Quaternion_VN.z= temp32f;


    temp32 = (((uint32_t)LocalSPI_RX_DATA[29]<<16)|LocalSPI_RX_DATA[28]);
    temp32f = *((float32_t*)&temp32);
    Quaternion_VN.w= temp32f;
*/

#ifdef KerenVer


    VN_INS_Status.Word=LocalSPI_RX_DATA[30];
    VN_GNSS_FIX_Status=LocalSPI_RX_DATA[31];

#else


/*
    temp32 = (((uint32_t)LocalSPI_RX_DATA[31]<<16)|LocalSPI_RX_DATA[30]);
    temp32f = *((float32_t*)&temp32);

    VN_Acceleration.pitch = temp32f;

    temp32 = (((uint32_t)LocalSPI_RX_DATA[33]<<16)|LocalSPI_RX_DATA[32]);
    temp32f = *((float32_t*)&temp32);

    VN_Acceleration.roll = temp32f;
    temp32=  (((uint32_t)LocalSPI_RX_DATA[35]<<16)|LocalSPI_RX_DATA[34]);
    temp32f = *((float32_t*)&temp32);

    VN_Acceleration.yaw = temp32f;
*/

    VN_INS_Status.Word=LocalSPI_RX_DATA[36];
    VN_GNSS_FIX_Status=LocalSPI_RX_DATA[37];

#endif

    if (FirstSpiRead>0  )
    {
        FirstSpiRead--;
    }

}

