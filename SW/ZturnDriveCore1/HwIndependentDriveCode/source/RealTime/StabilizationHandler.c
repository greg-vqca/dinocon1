


#include "GlobalIncludes.h"
#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(Handle_Stabilization, "ramfuncs");

#pragma FUNC_ALWAYS_INLINE(Handle_Stabilization);
#endif

Quaternion_struct/* Quaternion_VN,Quaternion_VN_body,Quaternion_VN_base,Quaternion_Vn,*/ Quaternion_base,Quaternion_base_rate,Quaternion_weapon,Quaternion_body,Quaternion_body_rate,Quaternion_rotation,Qt1, Qt2;

//Angles_struct VN_Angles, VN_BaseAngles, VN_Acceleration;
Angles_struct WeaponAngles,BodyAngles,BodyRate,RotationAxis;
Angles_struct At1, At2;
/*
int32_t BaseGyroToVN_Az_Diff=0;
int32_t BaseGyroToVN_El_Diff=0;
int32_t BaseGyroToVN_Roll_Diff=0;
*/
StabilizationTag        Stabilization;

int16_t rateDetectLIm = 3000;
int16_t accDetectLim =300;
void Handle_Stabilization(void)
{




    // azimuth position transfered directly
    Axis[0].Feedback.BaseGyroPos= Axis[0].Feedback.ShadowGyroPos;
    OverFlowPosOnDeg(&Axis[0].Feedback.BaseGyroPos);

    //azimuth velocity
    Axis[0].Feedback.GyroVelocty  += (Stabilization.BaseGyro.Rate.yaw- Axis[0].Feedback.GyroVelocty)/8;// filtered velocity

    if( (Stabilization.BaseGyro.Rate.yaw -Axis[0].Feedback.GyroPrevVelocty)!=0)
    {
        Axis[0].Feedback.ShadowGyroVeloctyChange +=        (float) ( (float)( Stabilization.BaseGyro.Rate.yaw-Axis[0].Feedback.GyroPrevVelocty) -  Axis[0].Feedback.GyroVeloctyChange)*0.1;
        Axis[0].Feedback.GyroVeloctyChange=  Axis[0].Feedback.ShadowGyroVeloctyChange;
    }

    Axis[0].Feedback.GyroPrevVelocty=  Stabilization.BaseGyro.Rate.yaw ;

    Quaternion_base =  fromAngles(Stabilization.BaseGyro.Angle.yaw, Stabilization.BaseGyro.Angle.roll,Stabilization.BaseGyro.Angle.pitch );

    Quaternion_base_rate =  fromAngles(Stabilization.BaseGyro.Rate.yaw, Stabilization.BaseGyro.Rate.roll,Stabilization.BaseGyro.Rate.pitch );

    //   Quaternion_baseRotated = Quaternion_mult(Quaternion_base, Quaternion_Gyro_BASE_rotation );

    RotationAxis.pitch=0;
    RotationAxis.roll=0;
    RotationAxis.yaw=1;




    Quaternion_rotation = fromAngleNormalAxis(Axis[0].Feedback.IntegratedDegPosition, RotationAxis);

    Quaternion_body = Quaternion_mult(Quaternion_base, Quaternion_rotation );
    Quaternion_body_rate = Quaternion_mult(Quaternion_base_rate, Quaternion_rotation );

    RotationAxis.pitch=1;
    RotationAxis.roll=0;
    RotationAxis.yaw=0;

    Quaternion_rotation = fromAngleNormalAxis((Axis[1].Feedback.IntegratedDegPosition-(int32_t)900000), RotationAxis);

    Quaternion_weapon= Quaternion_mult(Quaternion_body, Quaternion_rotation );
    //  Quaternion_weapon= Quaternion_base;
    WeaponAngles= toAngles(Quaternion_weapon.w, Quaternion_weapon.x, Quaternion_weapon.y, Quaternion_weapon.z);

    BodyAngles= toAngles(Quaternion_body.w, Quaternion_body.x, Quaternion_body.y, Quaternion_body.z);
    BodyRate= toAngles(Quaternion_body_rate.w, Quaternion_body_rate.x, Quaternion_body_rate.y, Quaternion_body_rate.z);


    Axis[1].Feedback.BaseGyroPos=BodyAngles.pitch;
    OverFlowPosOnDeg(&Axis[1].Feedback.BaseGyroPos);

    Axis[1].Feedback.GyroVelocty=BodyRate.pitch;
    if (FirstSpiRead>0  )
    {
        EnableInterupts(0);

        Axis[0].Feedback.GyroPrevPos=Axis[0].Feedback.BaseGyroPos;
        Axis[1].Feedback.GyroPrevPos=Axis[1].Feedback.BaseGyroPos;
        EnableInterupts(1);

        Axis[0].Feedback.GyroPosChange=0;
        Axis[1].Feedback.GyroPosChange=0;

    }
    else
    {
        Axis[0].Feedback.GyroPosChange  = Axis[0].Feedback.BaseGyroPos  - Axis[0].Feedback.GyroPrevPos;
        OverFlowPosDerivOnDeg(& Axis[0].Feedback.GyroPosChange);

        Axis[0].Feedback.GyroPrevPos=      Axis[0].Feedback.BaseGyroPos;

        Axis[1].Feedback.GyroPosChange  = Axis[1].Feedback.BaseGyroPos  - Axis[1].Feedback.GyroPrevPos;
        OverFlowPosDerivOnDeg(& Axis[1].Feedback.GyroPosChange);

        Axis[1].Feedback.GyroPrevPos=      Axis[1].Feedback.BaseGyroPos;

    }

    //calculate VN300 from weapon to base frame
/*
    RotationAxis.pitch=1;
    RotationAxis.roll=0;
    RotationAxis.yaw=0;

    Quaternion_rotation = fromAngleNormalAxis(-(Axis[1].Feedback.IntegratedDegPosition-(int32_t)900000), RotationAxis);


    Quaternion_VN_body = Quaternion_mult(Quaternion_VN, Quaternion_rotation );


    RotationAxis.pitch=0;
    RotationAxis.roll=0;
    RotationAxis.yaw=1;

    Quaternion_rotation = fromAngleNormalAxis(-Axis[0].Feedback.IntegratedDegPosition, RotationAxis);



   Quaternion_VN_base = Quaternion_mult(Quaternion_VN_body, Quaternion_rotation );
*/
/*

    VN_BaseAngles= toAngles(Quaternion_VN_base.w, Quaternion_VN_base.x, Quaternion_VN_base.y, Quaternion_VN_base.z);
    BaseGyroToVN_Az_Diff=VN_BaseAngles.yaw - Stabilization.BaseGyro.Angle.yaw;
    OverFlowPosDerivOnDeg(&BaseGyroToVN_Az_Diff);
    BaseGyroToVN_El_Diff=VN_BaseAngles.pitch - Stabilization.BaseGyro.Angle.pitch;
    OverFlowPosDerivOnDeg(&BaseGyroToVN_El_Diff);

    BaseGyroToVN_Roll_Diff=VN_BaseAngles.roll - Stabilization.BaseGyro.Angle.roll;
    OverFlowPosDerivOnDeg(&BaseGyroToVN_Roll_Diff);
*/


    //in motion check
    Stabilization.BaseGyro.AbsRateYaw=labs( Stabilization.BaseGyro.Rate.yaw) ;
    Stabilization.BaseGyro.AbsRatePitch =labs(Stabilization.BaseGyro.Rate.pitch);
    Stabilization.BaseGyro.AbsRateRoll = labs( Stabilization.BaseGyro.Rate.roll);
    Stabilization.BaseGyro.AbsACC_X=abs(Stabilization.BaseGyro.ACC_X);
    Stabilization.BaseGyro.AbsACC_Y=abs( Stabilization.BaseGyro.ACC_Y);

#define InMotionTimmerPreset 60000

    if((Stabilization.BaseGyro.AbsRateYaw+Stabilization.BaseGyro.AbsRatePitch+Stabilization.BaseGyro.AbsRateRoll)>rateDetectLIm)
    {
        if( Stabilization.InMotionCounter<InMotionTimmerPreset)
        {
            Stabilization.InMotionCounter=InMotionTimmerPreset;
        }
    }
    else if((Stabilization.BaseGyro.AbsACC_X+Stabilization.BaseGyro.AbsACC_Y)>accDetectLim)
    {
        if(  Stabilization.InMotionCounter<(InMotionTimmerPreset>>2))
        {
            Stabilization.InMotionCounter=(InMotionTimmerPreset>>2);
        }

    }
    else
    {
        if( Stabilization.InMotionCounter>0)  Stabilization.InMotionCounter--;
    }

}

