

#include "GlobalIncludes.h"

#if TI_DSP_Pragma ==1

#pragma CODE_SECTION(JogInPosition_Func, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(JogInPosition_Func);

#endif


void JogInPosition_Func(int16_t a)
{

    Axis[a].Serial.Jcmd=CustomIQsat( Axis[a].Serial.Jcmd, Axis[a].Drive.MaxCmd,-Axis[a].Drive.MaxCmd);
    Axis[a].Position.JogInPosAcceleration =  (float32_t)Axis[a].Serial.Jcmd/ Axis[a].Velocity.DegVelocityScaling*2  - Axis[a].Position.JogInPosVelocityOutput;
    if((Axis[a].Position.JogInPosVelocityOutput) >0 && (Axis[a].Position.Pe>0))
    {
        if(Axis[a].Position.Pe>Max_PE_in_Jog)
        {
            if(Axis[a].Position.JogInPosAcceleration>0)
            {
                Axis[a].Position.JogInPosAcceleration=0 ;
            }
            Axis[a].Position.JogInPosVelocityOutput -=Axis[a].Position.JogInPosVelocityOutput *0.01;

        }

    }
    if((Axis[a].Position.JogInPosVelocityOutput <0 )&& (Axis[a].Position.Pe<0))
    {
        if(Axis[a].Position.Pe<-Max_PE_in_Jog)
        {
            if(Axis[a].Position.JogInPosAcceleration<0)
            {
                Axis[a].Position.JogInPosAcceleration=  0 ;
            }
            Axis[a].Position.JogInPosVelocityOutput -=Axis[a].Position.JogInPosVelocityOutput *0.01;


        }

    }

    if(  Axis[a].Position.JogInPosAcceleration*Axis[a].Position.JogInPosVelocityOutput>0) //acceleration
    {

        if(Axis[a].Position.JogInPosAcceleration > Axis[a].Ptp.ScaledAcc_f     )
        {
            Axis[a].Position.JogInPosAcceleration= Axis[a].Ptp.ScaledAcc_f ;
        }
        if(Axis[a].Position.JogInPosAcceleration < -Axis[a].Ptp.ScaledAcc_f   )
        {
            Axis[a].Position.JogInPosAcceleration= -Axis[a].Ptp.ScaledAcc_f ;
        }
    }
    else//deceleration
    {
        if(Axis[a].Position.JogInPosAcceleration > Axis[a].Ptp.ScaledDec_f     )
        {
            Axis[a].Position.JogInPosAcceleration= Axis[a].Ptp.ScaledDec_f ;
        }
        if(Axis[a].Position.JogInPosAcceleration < -Axis[a].Ptp.ScaledDec_f   )
        {
            Axis[a].Position.JogInPosAcceleration= -Axis[a].Ptp.ScaledDec_f ;
        }
    }
    Axis[a].Position.JogInPosVelocityOutput+= Axis[a].Position.JogInPosAcceleration ;


    if( Axis[a].Position.JogInPosVelocityOutput<0)
    {
        if( Axis[a].Position.LimitedToSystemDistanceMaxNeg <-Axis[a].Position.JogInPosVelocityOutput)

            Axis[a].Position.JogInPosVelocityOutput= -Axis[a].Position.LimitedToSystemDistanceMaxNeg ;

    }
    else
    {
        if(Axis[a].Position.LimitedToSystemDistanceMaxPos<Axis[a].Position.JogInPosVelocityOutput)
            Axis[a].Position.JogInPosVelocityOutput=  Axis[a].Position.LimitedToSystemDistanceMaxPos;

    }


}

