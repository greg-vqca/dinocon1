

#include "GlobalIncludes.h"


int16_t SCIA_BufferInPointer=0;

char SCIA_BufferIn[SERIAL_BUFFER_SIZE];
char SerCommandBufferA[SERIAL_BUFFER_SIZE];
char SerCommandBufferOut[SERIAL_BUFFER_SIZE];

int16_t  	NewCmdFlagSCIA=0;
CMD_Src_tag CMD_Src;



#if TI_DSP_Pragma==1
#pragma CODE_SECTION(ReadSCIA, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(ReadSCIA);



#pragma FUNC_ALWAYS_INLINE(PrintChar);

#pragma CODE_SECTION(PrintChar, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE(PrintStr);

#pragma CODE_SECTION(PrintStr, "ramfuncs");
#endif



void ReadSCIA(void)
{
    int16_t i;
    static int16_t receivedChar[3]={0,0,0};
    int recived_car=0;
	
    if ( GetSCIA_RX_Buffer_LVL()<=0)
	{
	return;
	}
    recived_car =GetSCIA_RX_Buffer();


    if ( recived_car!=0)
    {
        receivedChar[2] =receivedChar[1];
        receivedChar[1] =receivedChar[0];
        receivedChar[0] =recived_car;

        switch( receivedChar[0])
        {
        case '\b'://backspace

            //	PrintStr("\b \b\0");

            SCIA_BufferInPointer--;
            if (SCIA_BufferInPointer<0)
            {
                SCIA_BufferInPointer=0;
            }

            SCIA_BufferIn[SCIA_BufferInPointer]=0;
            break;

        case 65: //this is arrow up case

            if (receivedChar[2] == 27 && receivedChar[1]== 91)
            {
                SCIA_BufferInPointer =0;
                for (i = 0; SerCommandBufferA[i]!='\0';i++)
                {
                    SCIA_BufferIn[i] = SerCommandBufferA[i];
                    PrintCharSerial(SerCommandBufferA[i]);
                }
            }
            break;

        case '\r':

            for (i = 0; i<=(SCIA_BufferInPointer+1);i++)
            {
                SerCommandBufferA[i]=SCIA_BufferIn[i];
                SCIA_BufferIn[i] = '\0';
            }
            SerCommandBufferA[i] ='\0';

            SCIA_BufferInPointer = 0;

            NewCmdFlagSCIA = 1;
            CMD_Src = SCI_A;


            break;
        default:
            if (receivedChar[0] > 31 && receivedChar[0] <123 )//in the legal int8_t range
            {
                SCIA_BufferIn[SCIA_BufferInPointer]=tolower(receivedChar[0]);
                SCIA_BufferInPointer++;
                if (SCIA_BufferInPointer > (SERIAL_BUFFER_SIZE-1))
                {
                    SCIA_BufferInPointer = SERIAL_BUFFER_SIZE;
                    SCI_Buff_OF_Fault=1;

                }
            }
            break;
        }
    }
}

#define SCI_BUFFER_SIZE 320

// INT9.3



void PrintCharSerial(char character)
{


        while (GetSCIA_TX_Buffer_LVL() >= HWD_SERIAL_BUFFER_SIZE )
        {
            uSleep(1);;
        }

        SetSCIA_TX_Buffer(character);


}


void PrintStr(char* Str)
{

    int16_t i=0;
    {
        while (Str[i]!='\0')
        {
            PrintCharSerial(Str[i]);
            i++;
        }
    }
}
