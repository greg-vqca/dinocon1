#include "GlobalIncludes.h"



int dummyvar;



void DummyFunc (void)
{
	return;
}
const ParamStruct_tag ParamStruct[NUMBER_OF_PARAMETERS]   ={
		//	 variable,								min/						max/				Default/			name, 		signed,	length	,Saved to EEprom
		{(long)&dummyvar							,0,							(int16_t)10000,			0,				"",						1,	1,	0},
		{(long)&Axis[0].Serial.Jcmd					,(int32_t)-200000,			(int32_t)200000,			0,				"j",					1,	2,	0},
		{(long)&Axis[0].Current.I					,0,							(int16_t)HWD_MaxCurrent,	0,				"i",					1,	1,	0},
		{(long)&Axis[0].Drive.Opmode				,0,							(int16_t)6,				3,				"opmode",				1,	1,	1},
		{(long)&Axis[0].Motor.Poles					,0,							(int16_t)200,				12,				"mpoles",				1,	1,	1},
		{(long)&Axis[0].Ptp.Acc						,1,							(int16_t)MaxInt,			100,			"acc",					1,	1,	1},
		{(long)&Axis[0].Ptp.Dec						,1,							(int16_t)MaxInt,			100,			"dec",					1,	1,	1},
        {(long)&CPDL_Version                         ,(int32_t)-MaxLong,                 (int32_t)MaxLong,          0,              "cpldver",              1,  2,  0},
        {(long)&Stabilization.BaseGyro.ACC_X          ,0,                 (int32_t)MaxLong,          0,              "baseaccx",              1,  1,  0},
        {(long)&Stabilization.BaseGyro.ACC_Y          ,0,                 (int32_t)MaxLong,          0,              "baseaccy",              1,  1,  0},


		{(long)&Axis[0].Drive.DriveTemp_sense						,0,							(int16_t)10000,			0,				"drvtemp",				1,	1,	0},
        {(long)&Axis[0].Feedback.Load.DegPosition            ,0,                 (int32_t)MaxLong,          0,              "loadpfb",              1,  2,  0},
        {(long)&Axis[0].Feedback.IntegratedDegPosition            ,0,                 (int32_t)MaxLong,          0,              "pfb",              1,  2,  0},
        {(long)&Axis[0].Feedback.Load.DegPositionOffset            ,0,                 (int32_t)MaxLong,          0,              "absencoffset",              1,  2,  1},

        {(long)&Axis[0].Feedback.Motor.DegPosition   ,0,                         (int32_t)MaxInt,          0,              "motorpfb",             1,  2,  0},
		{(long)&Axis[0].Feedback.Motor.EncPosition	,0,							(int32_t)MaxInt,			0,				"mencpfb",				1,	2,	0},
        {(long)&Axis[0].Feedback.Load.EncPosition   ,0,                          (int32_t)MaxInt,          0,              "lencpfb",              1,  2,  0},
        {(long)&Axis[0].Feedback.Halls.DegPosition   ,0,                         (int32_t)MaxInt,          0,              "hallspfb",             1,  2,  0},

		{(long)&Axis[0].Current.Param.Gp			,0,							(int32_t)MaxLong,			(long)5000000,		"igp",					1,	2,	1},
		{(long)&Axis[0].Current.Param.Gi			,0,							(int32_t)MaxLong,			5000,			"igi",					1,	2,	1},
		{(long)&Axis[0].Velocity.Param.Gp			,0,							(int32_t)MaxLong,			(long)80000,			"vgp",					1,	2,	1},
		{(long)&Axis[0].Velocity.Param.Gi			,0,							(int32_t)MaxLong,			100,			"vgi",					1,	2,	1},
		{(long)&Axis[0].Velocity.Param.Gd			,0,							(int32_t)MaxLong,			0,				"vgd",					1,	2,	1},
		{(long)&Axis[0].Velocity.Param.GpAdap		,0,							(int32_t)MaxLong,			0,				"vgpadap",				1,	2,	1},
		{(long)&Axis[0].Velocity.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"vgiadap",				1,	2,	1},
        {(long)&Axis[0].Velocity.Param.Gff          ,(int32_t)-MaxLong,           (int32_t)MaxLong,         0,              "vgff",                 1,  2,  1},

		{(long)&Axis[0].Current.Param.GpAdap		,0,							(int32_t)MaxLong,			10000,			"igpadap",				1,	2,	1},
		{(long)&Axis[0].Current.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"igiadap",				1,	2,	1},
		{(long)&Axis[0].Position.Plim		    	,0,							(int32_t)1,				0,				"plim",					1,	1,	1},
		{(long)&Axis[0].Position.PlimMax	    	,(int32_t)-MaxLong,			(int32_t)MaxLong,			(int32_t)MaxLong,	"plimmax",				1,	2,	1},
		{(long)&Axis[0].Position.PlimMin	    	,(int32_t)-MaxLong,			(int32_t)MaxLong,			(int32_t)-MaxLong,"plimmin",				1,	2,	1},
		{(long)&Axis[0].Position.Param.Gp			,0,							(int32_t)MaxLong,			(long)6000000,		"pgp",					1,	2,	1},
		{(long)&Axis[0].Position.Param.Gi			,0,							(int32_t)MaxLong,			100,			"pgi",					1,	2,	1},
		{(long)&Axis[0].Position.Param.GiSat		,0,							(int32_t)MaxLong,			(long)100000,			"pgisat",				1,	2,	1},
		{(long)&Axis[0].Position.Param.GpAdap		,(int32_t)-MaxLong,			(int32_t)MaxLong,		    0,				"pgpadap",				1,	2,	1},
		{(long)&Axis[0].Position.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"pgiadap",				1,	2,	1},
		{(long)&Axis[0].Position.Param.Gd			,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,				"pgd",					1,	2,	1},
		{(long)&Axis[0].Position.Param.Gff			,(int32_t)-MaxLong,			(int32_t)MaxLong,			(long)50000,			"pgff",					1,	2,	1},
		{(long)&Axis[0].Serial.Vcmd					,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,				"v",					1,	2,	0},
		{(long)&Axis[0].Velocity.Command			,(int32_t)-200000,			(int32_t)200000,			0,				"vcmd",					1,	2,	0},

		{(long)&Axis[0].Feedback.BaseGyroPos			,0,							(int32_t)MaxLong,			0,				"gyropfb",				1,	2,	0},
        {(long)&Axis[0].Feedback.GyroVelocty            ,0,                         (int32_t)MaxInt,         0,              "gyrovel",               1,  2,  0},

		{(long)&Stabilization.GyroStabilization  ,0,			                            1 ,			0,		    	"gyroen",		    	1,	1,	1},
		{(long)&Axis[0].Velocity.LoadEncVfb			,0,							(int32_t)MaxLong,			0,				"loadvfb",					1,	2,	0},
        {(long)&Axis[0].Velocity.MotorEncVfb        ,0,                         (int32_t)MaxLong,         0,              "motorvfb",                  1,  2,  0},

		{(long)&Axis[0].Ptp.Vcruise					,0,							(int32_t)MaxLong,			(long)500000,			"vcruise",				1,	2,	1},
		{(long)&Axis[0].Feedback.Motor.EncResolution ,0,							(int32_t)MaxLong,			4500,			"mencres",				1,	2,	1},
        {(long)&Axis[0].Feedback.Load.EncResolution  ,0,                         (int32_t)MaxLong,         (long)1<<18,            "lencres",               1,  2,  1},

		{(long)&Axis[0].Position.Pe					,0,							(int32_t)MaxLong,			0,				"pe",					1,	2,	0},
		{(long)&Axis[0].Position.SystemCommand			,0,							(int32_t)MaxLong,			0,				"syspcmd",					1,	2,	0},
        {(long)&Axis[0].Position.LimitedCommand          ,0,                         (int32_t)MaxLong,         0,              "limpcmd",                  1,  2,  0},

		{(long)&Axis[0].Drive.MaxCmd				,0,							(long)360000,			(long)50000,			"cmdmax",				1,	2,	1},

		{(long)&Axis[0].Current.Param.Gd			,0,							(int32_t)MaxLong,			0,				"igd",					1,	2,	1},
		{(long)&Axis[0].Serial.ExternCmdScale		,(int32_t)-20000000,			(int32_t)20000000,		1,			    "externscale",			1,	2,	1},
		{(long)&Axis[0].Velocity.Vmax				,0,							(int32_t)MaxLong,			(long)1000000,		"vmax",					1,	2,	1},
		{(long)&Axis[0].Velocity.Vmin				,600,						(int32_t)MaxLong,			600,			"vmin",					1,	2,	1},
		{(long)&Axis[0].Velocity.Vflt				,0,							(int32_t)MaxLong,			(long)1000000, 	"vflt",					1,	2,	1},
		{(long)&Axis[0].Current.Va					,0,							(int16_t)10000,			0,				"va",					1,	1,	0},
		{(long)&Axis[0].Current.Vb					,0,							(int16_t)10000,			0,				"vb",					1,	1,	0},
		{(long)&Axis[0].Current.Vc					,0,							(int16_t)10000,			0,				"vc",					1,	1,	0},
		{(long)&Axis[0].Current.Ia					,0,							(int16_t)10000,			0,				"ia",					1,	1,	0},
		{(long)&Axis[0].Current.Ib					,0,							(int16_t)10000,			0,				"ib",					1,	1,	0},
		{(long)&Axis[0].Current.Ic					,0,							(int16_t)10000,			0,				"ic",					1,	1,	0},
		{(long)&Axis[0].Current.I2T_Value			,0,							(int16_t)HWD_MaxCurrent,	0,				"i2t",					1,	2,	0},
		{(long)&Axis[0].Current.Command				,0,							(int16_t)10000,			0,				"icmd",					1,	1,	0},
		{(long)&Axis[0].Current.ia_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"iacmd",				1,	1,	0},
		{(long)&Axis[0].Current.ib_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"ibcmd",				1,	1,	0},
		{(long)&Axis[0].Current.ic_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"iccmd",				1,	1,	0},
		{(long)&Axis[0].Motor.HallsPosition			,0,							(int16_t)10000,			0,				"hallsp",				1,	1,	0},
		{(long)&Axis[0].Motor.Rotor_pos				,0,							(int16_t)10000,			0,				"rotp",					1,	1,	0},
		{(long)&Axis[0].Motor.ComutationAngle			,(int16_t)-10000,				(int16_t)HWD_MaxCurrent,	0,			"eprd",					1,	1,	0},

		{(long)&Axis[0].Feedback.MechPos			,(int16_t)-10000,				(int16_t)HWD_MaxCurrent,	0,				"prd",					1,	1,	0},
		{(long)&Axis[0].Drive.CmdSrc				,0,							(int16_t)4,				0,				"cmdsrc",				1,	1,	1},
		{(long)&Axis[0].Motor.Phase					,0,							(int16_t)360,				215,			"mphase",				1,	1,	1},
        {(long)&Axis[0].Motor.PhaseAdvanceLim       ,0,                         (int16_t)120,             60,              "phaseadvancelim",               1,  1,  1},
        {(long)&Axis[0].Motor.PhaseAdvanceScale     ,0,                         (int16_t)10000,           30,              "phaseadvance",               1,  1,  1},

		{(long)&Axis[0].Motor.Cmd_pol				,-1,						(int16_t)1,				1,				"cmdpol",				1,	1,	1},
		{(long)&Axis[0].Motor.GearDenominator		,1,							(int16_t)MaxInt,			69,				"geard",				1,	1,	1},
		{(long)&Axis[0].Motor.GearNominator			,1,							(int16_t)MaxInt,			1,				"gearn",				1,	1,	1},
		{(long)&Axis[0].Motor.Fbk_pol				,-1,						(int16_t)1,				0 ,				"fbkpol",				1,	1,	1},

        {(long)&Axis[0].Feedback.UseLoadEncoder              ,0,                        (int16_t)1,               1,              "fbktype",               1,  1,  0},

		{(long)&Axis[0].Current.Phase_A_output		,-1500,						(int16_t)1500,			0,				"pwma",					1,	1,	0},
		{(long)&Axis[0].Current.Phase_B_output		,-1500,						(int16_t)1500,			0,				"pwmb",					1,	1,	0},
		{(long)&Axis[0].Current.Phase_C_output		,-1500,						(int16_t)1500,			0,				"pwmc",					1,	1,	0},
		{(long)&Axis[0].Current.Imax				,0,							(int16_t)HWD_MaxCurrent,	1000,			"imax",					1,	1,	1},
		{(long)&Axis[0].Current.Iflt				,0,							(int16_t)HWD_MaxCurrent,	1500,			"iflt",					1,	1,	1},
		{(long)&Axis[0].Current.Icont				,0,							(int16_t)HWD_MaxCurrent,	380,			"icont",				1,	1,	1},
		{(long)&Axis[0].Current.I2T_lim				,0,							(int16_t)HWD_MaxCurrent,	0,	    		"i2tlim",				1,	2,	1},
		{(long)&Axis[0].Velocity.FilterFc			,0,							(int16_t)10000,			    200,			"vlpf",					1,	1,	1},
		{(long)&Axis[0].Drive.VBus					,0,							(int16_t)MaxInt,			50,				"vbus",					1,	1,	0},
		{(long)&Axis[0].Drive.VLogic				,0,							(int16_t)MaxInt,			50,			    "vlogic",				1,	1,	0},
		{(long)&Axis[0].Drive.VBusMax				,0,							(int16_t)MaxInt,			340,	       	"vbusmax",				1,	1,	1},
		{(long)&Axis[0].Drive.VBusMin				,0,							(int16_t)MaxInt,			20,				"vbusmin",				1,	1,	1},
		{(long)&Axis[0].Drive.OverTemp				,0,							(int16_t)MaxInt,			90,				"tempflt",				1,	1,	1},
		{(long)&Axis[0].Serial.Icmd					,(int16_t)-HWD_MaxCurrent,	(int16_t)HWD_MaxCurrent,	0,				"t",					1,	1,	0},
		{(long)&Axis[0].Feedback.GyroOffset			,(int16_t)-MaxInt,			(int16_t)MaxInt,			0,				"gyrooffset",			1,	1,	1},

   //     {(long)&Axis[0].BuidInTest.Phase_resistance,0,                         (int16_t)MaxInt,  360,           "bitresistance",                 1,  1,  1},
        {(long)&Axis[0].Drive.State                 ,(int16_t)0,    (int16_t)MaxInt,  0,              "stat",                 1,  1,  0},


		{(long)&Axis[1].Serial.Jcmd					,(int32_t)-200000,			(int32_t)200000,			0,				"ax2j",					1,	2,	0},
		{(long)&Axis[1].Current.I					,0,							(int16_t)HWD_MaxCurrent,	0,				"ax2i",					1,	1,	0},
		{(long)&Axis[1].Drive.Opmode				,0,							(int16_t)6,				3,				"ax2opmode",			1,	1,	1},
		{(long)&Axis[1].Motor.Poles					,0,							(int16_t)200,				12,				"ax2mpoles",			1,	1,	1},
		{(long)&Axis[1].Ptp.Acc						,1,							(int16_t)MaxInt,			100,			"ax2acc",				1,	1,	1},
		{(long)&Axis[1].Ptp.Dec						,1,							(int16_t)MaxInt,			100,			"ax2dec",				1,	1,	1},

        {(long)&Axis[1].Feedback.Load.DegPosition    ,0,                         (int32_t)MaxInt,          0,              "ax2loadpfb",                  1,  2,  0},
        {(long)&Axis[1].Feedback.IntegratedDegPosition            ,0,                 (int32_t)MaxInt,          0,              "ax2pfb",              1,  2,  0},
        {(long)&Axis[1].Feedback.Load.DegPositionOffset            ,0,                 (int32_t)MaxLong,          0,              "ax2absencoffset",              1,  2,  1},

        {(long)&Axis[1].Feedback.Motor.DegPosition   ,0,                         (int32_t)MaxInt,          0,              "ax2motorpfb",                  1,  2,  0},
        {(long)&Axis[1].Feedback.Motor.EncPosition   ,0,                         (int32_t)MaxInt,          0,              "ax2mencpfb",            1,  2,  0},
        {(long)&Axis[1].Feedback.Load.EncPosition    ,0,                         (int32_t)MaxInt,          0,              "ax2lencpfb",            1,  2,  0},
        {(long)&Axis[1].Feedback.Halls.DegPosition   ,0,                         (int32_t)MaxInt,          0,              "ax2hallspfb",                  1,  2,  0},

        {(long)&Axis[1].Drive.DriveTemp_sense					,0,							(int16_t)10000,			0,				"ax2drvtemp",			1,	1,	0},
		{(long)&Axis[1].Current.Param.Gp			,0,							(int32_t)MaxLong,			(long)2000000,		"ax2igp",				1,	2,	1},
		{(long)&Axis[1].Current.Param.Gi			,0,							(int32_t)MaxLong,			5000,			"ax2igi",				1,	2,	1},
		{(long)&Axis[1].Velocity.Param.Gp			,0,							(int32_t)MaxLong,			(long)30000,			"ax2vgp",				1,	2,	1},
		{(long)&Axis[1].Velocity.Param.Gi			,0,							(int32_t)MaxLong,			100,		    "ax2vgi",				1,	2,	1},
		{(long)&Axis[1].Velocity.Param.Gd			,0,							(int32_t)MaxLong,			0,				"ax2vgd",				1,	2,	1},
		{(long)&Axis[1].Velocity.Param.GpAdap		,0,							(int32_t)MaxLong,			0,				"ax2vgpadap",			1,	2,	1},
		{(long)&Axis[1].Velocity.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"ax2vgiadap",			1,	2,	1},
        {(long)&Axis[1].Velocity.Param.Gff          ,(int32_t)-MaxLong,           (int32_t)MaxLong,         0,             "ax2vgff",                 1,  2,  1},

		{(long)&Axis[1].Current.Param.GpAdap		,0,							(int32_t)MaxLong,			0,				"ax2igpadap",			1,	2,	1},
		{(long)&Axis[1].Current.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"ax2igiadap",			1,	2,	1},
		{(long)&Axis[1].Position.Plim		    	,0,							(int32_t)1,				0,				"ax2plim",				1,	1,	1},
		{(long)&Axis[1].Position.PlimMax	       	,(int32_t)-MaxLong,			(int32_t)MaxLong,			(int32_t)MaxLong,	"ax2plimmax",			1,	2,	1},
		{(long)&Axis[1].Position.PlimMin	    	,(int32_t)-MaxLong,			(int32_t)MaxLong,			(int32_t)-MaxLong,"ax2plimmin",			1,	2,	1},
		{(long)&Axis[1].Position.Param.Gp			,0,							(int32_t)MaxLong,			(long)6000000,		"ax2pgp",				1,	2,	1},
		{(long)&Axis[1].Position.Param.Gi			,0,							(int32_t)MaxLong,			100,			"ax2pgi",				1,	2,	1},
		{(long)&Axis[1].Position.Param.GiSat		,0,							(int32_t)MaxLong,			10000,			"ax2pgisat",			1,	2,	1},
		{(long)&Axis[1].Position.Param.GpAdap		,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,				"ax2pgpadap",			1,	2,	1},
		{(long)&Axis[1].Position.Param.GiAdap		,0,							(int32_t)MaxLong,			0,				"ax2pgiadap",			1,	2,	1},
		{(long)&Axis[1].Position.Param.Gd			,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,				"ax2pgd",				1,	2,	1},
		{(long)&Axis[1].Position.Param.Gff			,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,			"ax2pgff",				1,	2,	1},
		{(long)&Axis[1].Serial.Vcmd					,(int32_t)-MaxLong,			(int32_t)MaxLong,			0,				"ax2v",					1,	2,	0},
		{(long)&Axis[1].Velocity.Command			,(int32_t)-200000,			(int32_t)200000,			0,				"ax2vcmd",				1,	2,	0},

		{(long)&Axis[1].Feedback.BaseGyroPos 			,0,							(int32_t)MaxLong,			0,				"ax2gyropfb",				1,	2,	0},
        {(long)&Axis[1].Feedback.GyroVelocty            ,0,                         (int32_t)MaxInt,         0,              "ax2gyrovel",               1,  2,  0},

		{(long)&Axis[1].Velocity.LoadEncVfb			,0,							(int32_t)MaxLong,			0,				"ax2loadvfb",			1,	2,	0},
        {(long)&Axis[1].Velocity.MotorEncVfb        ,0,                         (int32_t)MaxLong,         0,              "ax2motorvfb",          1,  2,  0},

		{(long)&Axis[1].Ptp.Vcruise					,0,							(int32_t)MaxLong,			(long)500000,   	"ax2vcruise",			1,	2,	1},
		{(long)&Axis[1].Feedback.Motor.EncResolution	,0,							(int32_t)MaxLong,			4500,			"ax2mencres",			1,	2,	1},
        {(long)&Axis[1].Feedback.Load.EncResolution  ,0,                         (int32_t)MaxLong,         (long)1<<18,            "ax2lencres",           1,  2,  1},

		{(long)&Axis[1].Position.Pe					,0,							(int32_t)MaxLong,			0,				"ax2pe",				1,	2,	0},
		{(long)&Axis[1].Position.SystemCommand          ,0,                         (int32_t)MaxLong,         0,              "ax2syspcmd",                  1,  2,  0},
		{(long)&Axis[1].Position.LimitedCommand          ,0,                         (int32_t)MaxLong,         0,              "ax2limpcmd",                  1,  2,  0},


		{(long)&Axis[1].Drive.MaxCmd				,0,							(long)360000,			(long)50000,			"ax2cmdmax",			1,	2,	1},
		{(long)&Axis[1].Current.Param.Gd			,0,							(int32_t)MaxLong,			0,				"ax2igd",				1,	2,	1},
		{(long)&Axis[1].Serial.ExternCmdScale		,(int32_t)-20000000,			(int32_t)20000000,		1,			    "ax2externscale",		1,	2,	1},
		{(long)&Axis[1].Velocity.Vmax				,0,							(int32_t)MaxLong,			(long)1000000,		    "ax2vmax",				1,	2,	1},
		{(long)&Axis[1].Velocity.Vmin				,600,						(int32_t)MaxLong,			600,			"ax2vmin",				1,	2,	1},
		{(long)&Axis[1].Velocity.Vflt				,0,							(int32_t)MaxLong,			(long)1000000,    	"ax2vflt",				1,	2,	1},
		{(long)&Axis[1].Current.Va					,0,							(int16_t)10000,			0,				"ax2va",				1,	1,	0},
		{(long)&Axis[1].Current.Vb					,0,							(int16_t)10000,			0,				"ax2vb",				1,	1,	0},
		{(long)&Axis[1].Current.Vc					,0,							(int16_t)10000,			0,				"ax2vc",				1,	1,	0},
		{(long)&Axis[1].Current.Ia					,0,							(int16_t)10000,			0,				"ax2ia",				1,	1,	0},
		{(long)&Axis[1].Current.Ib					,0,							(int16_t)10000,			0,				"ax2ib",				1,	1,	0},
		{(long)&Axis[1].Current.Ic					,0,							(int16_t)10000,			0,				"ax2ic",				1,	1,	0},
		{(long)&Axis[1].Current.I2T_Value			,0,							(int16_t)HWD_MaxCurrent,	0,				"ax2i2t",				1,	2,	0},
		{(long)&Axis[1].Current.Command				,0,							(int16_t)10000,			0,				"ax2icmd",				1,	1,	0},
		{(long)&Axis[1].Current.ia_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"ax2iacmd",				1,	1,	0},
		{(long)&Axis[1].Current.ib_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"ax2ibcmd",				1,	1,	0},
		{(long)&Axis[1].Current.ic_cmd				,(int16_t)-10000,				(int16_t)10000,			0,				"ax2iccmd",				1,	1,	0},
		{(long)&Axis[1].Motor.HallsPosition			,0,							(int16_t)10000,			0,				"ax2hallsp",			1,	1,	0},
		{(long)&Axis[1].Motor.Rotor_pos				,0,							(int16_t)10000,			0,				"ax2rotp",				1,	1,	0},
		{(long)&Axis[1].Feedback.MechPos			,(int16_t)-10000,				(int16_t)HWD_MaxCurrent,	0,				"ax2prd",				1,	1,	0},
		{(long)&Axis[1].Drive.CmdSrc				,0,							(int16_t)4,				0,				"ax2cmdsrc",			1,	1,	1},
		{(long)&Axis[1].Motor.Phase					,0,							(int16_t)360,				215,			"ax2mphase",			1,	1,	1},
        {(long)&Axis[1].Motor.PhaseAdvanceLim       ,0,                         (int16_t)120,             60,             "ax2phaseadvancelim",   1,  1,  1},
        {(long)&Axis[1].Motor.PhaseAdvanceScale     ,0,                         (int16_t)10000,           30,             "ax2phaseadvance",      1,  1,  1},

		{(long)&Axis[1].Motor.Cmd_pol				,-1,						(int16_t)1,				1,				"ax2cmdpol",			1,	1,	1},
		{(long)&Axis[1].Motor.GearDenominator		,1,							(int16_t)MaxInt,			75,				"ax2geard",				1,	1,	1},
		{(long)&Axis[1].Motor.GearNominator			,1,							(int16_t)MaxInt,			1,				"ax2gearn",				1,	1,	1},
		{(long)&Axis[1].Motor.Fbk_pol				,-1,						(int16_t)1,				1,				"ax2fbkpol",			1,	1,	1},

        {(long)&Axis[1].Feedback.UseLoadEncoder               ,0,                        (int16_t)1,               1,              "ax2fbktype",               1,  1,  0},

		{(long)&Axis[1].Current.Phase_A_output		,-1500,						(int16_t)1500,			0,				"ax2pwma",				1,	1,	0},
		{(long)&Axis[1].Current.Phase_B_output		,-1500,						(int16_t)1500,			0,				"ax2pwmb",				1,	1,	0},
		{(long)&Axis[1].Current.Phase_C_output		,-1500,						(int16_t)1500,			0,				"ax2pwmc",				1,	1,	0},
		{(long)&Axis[1].Current.Imax				,0,							(int16_t)HWD_MaxCurrent,	1000,			"ax2imax",				1,	1,	1},
		{(long)&Axis[1].Current.Iflt				,0,							(int16_t)HWD_MaxCurrent,	1200,			"ax2iflt",				1,	1,	1},
		{(long)&Axis[1].Current.Icont				,0,							(int16_t)HWD_MaxCurrent,	380,			"ax2icont",				1,	1,	1},
		{(long)&Axis[1].Current.I2T_lim				,0,							(int16_t)HWD_MaxCurrent,	0,	    		"ax2i2tlim",			1,	2,	1},
		{(long)&Axis[1].Velocity.FilterFc			,0,							(int16_t)10000,			200,			"ax2vlpf",				1,	1,	1},
		{(long)&Axis[1].Drive.VBus					,0,							(int16_t)MaxInt,			50,				"ax2vbus",				1,	1,	0},
		{(long)&Axis[1].Drive.VLogic				,0,							(int16_t)MaxInt,			50,				"ax2vlogic",			1,	1,	0},
		{(long)&Axis[1].Drive.OverTemp				,0,							(int16_t)MaxInt,			90,				"ax2tempflt",			1,	1,	1},
		{(long)&Axis[1].Serial.Icmd					,(int16_t)-HWD_MaxCurrent,	(int16_t)HWD_MaxCurrent,	0,				"ax2t",					1,	1,	0},
		{(long)&Axis[1].Feedback.GyroOffset			,(int16_t)-MaxInt,			(int16_t)MaxInt,			1,				"ax2gyrooffset",		1,	1,	1},
 //       {(long)&Axis[1].BuidInTest.Phase_resistance,0,                         (int16_t)MaxInt,  398,           "ax2bitresistance",                 1,  1,  1},
        {(long)&Axis[1].Drive.State                 ,(int16_t)0,    (int16_t)MaxInt,  0,              "ax2stat",                 1,  1,  0},




		{(long)&WatchDogTime						,0,							(int32_t)MaxLong,			1000,			"wdtime",				1,	2,	1},
		{(long)&Runtime								,0,							(int32_t)MaxInt,			0,				"time",					1,	2,	0},

		{(long)&HWD_BootStrap						,0,							(int16_t)10000,			50,				"dt",					1,	1,	1},
		{(long)&PWM_Freq_scaling					,0,							(int16_t)3,				1,				"pwmfreq",				1,	1,	1},
		{(long)&Record.Done							,0,							(int16_t)2,				0,				"recdone",				1,	1,	0},
		{(long)&FaultIgnoreFlag						,0,							(int16_t)1,				0,				"faultignore",			1,	1,	1},

};

const FuncStruct_tag FuncStruct[NUMBER_OF_FUNCTIONS]   =
{//		function												name
		{(long)DummyFunc							, 				""},
		{(long)printVersion							, 				"ver"},
		{(long)parserRecTrig						, 				"rectrig"},
		{(long)parserGet							, 				"get"},
		{(long)MoveIncremental						, 				"mi"},
		{(long)MoveAbsolute							, 				"ma"},
		{(long)RecordCommand						, 				"record"},
		{(long)StopRecorder							, 				"recoff"},
		{(long)ReadFaults							, 				"flt"},
        {(long)ReadWarnings                           ,               "wrn"},

		{(long)ReadBothPFB						, 				"bothpfb"},
		{(long)ReadBothENC						, 				"bothenc"},

		{(long)ReadFaultsHistory					, 				"flthst"},
		{(long)ClearFaults							, 				"clrflt"},
		{(long)ClearFaultsHistory					, 				"clrhst"},
		{(long)PrintParametersSavedToEEPROM			, 				"dump"},
		{(long)EnablePower							, 				"en"},
		{(long)SetPwmFreq							, 				"setpwmfreq"},

		{(long)DisablePower							, 				"k"},
		{(long)PrintHallsState						, 				"halls"},

		{(long)writememory							, 				"wm"},
		{(long)readmemory							, 				"rm"},

		{(long)DummyFunc							, 				""},
		{(long)JumpToBoot							,			"bootloader"},
		{(long)SaveParameters							,			"save"},

		{(long)BothEnablePower							, 				"bothen"},
        {(long)BothMoveAbs                          ,               "bothma"},


		{(long)LoadSavedParameters							,			"load"}

};



