#ifndef FAULTHANDLER_H
#define FAULTHANDLER_H
void WarningHandler_func(int16_t a);
void FaultHandler_func(int16_t a);
extern int16_t FaultIgnoreFlag;

extern int16_t CPLD_SPI_FaultFlag;

extern int16_t Eeprom_Read_Fault_Flag;
extern int16_t Eeprom_Write_Fault_Flag;
extern int16_t SCI_Buff_OF_Fault;
extern uint16_t Motor_FBK_Failed[NumberOfAxes];
extern uint16_t Load_FBK_Failed[NumberOfAxes];
extern uint16_t Halls_FBK_Failed[NumberOfAxes];

extern int16_t Motor_FBK_FailedDelay[NumberOfAxes];

extern int16_t  Illegal_halls_Flag[NumberOfAxes];
extern int16_t Motor_FBK_FailedDelay[NumberOfAxes];

extern int16_t General_FBK_FailedDelay[NumberOfAxes];

extern VN_INS_Status_tag VN_INS_Status;
extern uint16_t VN_GNSS_FIX_Status;
extern int16_t AbsEncFirstReadingRecievedFlag[NumberOfAxes];
extern int16_t PrevAbsEncFirstReadingRecievedFlag[NumberOfAxes];

#endif
