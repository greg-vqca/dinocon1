#ifndef INIT_H
#define INIT_H
void Init(void);
void InitVar(void);
void InitParamsArray(void);
extern int16_t InitFinishedFlag;

#endif
