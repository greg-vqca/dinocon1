#ifndef EEHANDLER_H
#define  EEHANDLER_H



void SaveEeprom(void);
int16_t LoadEeprom(void);
void PrepareSaveEeprom(void);

void PerformSaveEeprom(void);
int16_t CheckSum(int16_t buffLong[], int16_t buffSize);
extern int16_t  SaveFlag;
extern  int16_t     DoSaveParameters;

#endif


