
#ifndef COMBINARYPARSER_H
#define COMBINARYPARSER_H


#define PDO_DOWN_MSG_SIZE 120
#define PDO_UP_MSG_SIZE 184


#define MSG_TYPE_PDO 0x26
#define MSG_TYPE_SDO 0x36



// data structure:          controll32b     controll32b2    j   j2      mi      mi2     XX      XX2     chksum
// return data structure:   status32b       status32b2      pfb pfb2    encpfb  encpfb2 vfb     vfb2    chksum


// PDO down Packet Definition
typedef struct
{
    uint32_t//global  //1
    MsgType:      16,
    PDO_down_GBL_reserved1:      16;

    uint32_t//global //2
    PDO_down_GBL_reserved2:      32;
    uint32_t//3
    PDO_down_GBL_reserved3:           32;
    uint32_t//4
    PDO_down_GBL_reserved4:           32;


    uint32_t//Axis1 //1
    DriveControlEn:   8,
    PDO_down_Ax1_reserved2:   8,
    PDO_down_Ax1_reserved3:   8,

    DriveControlReserved:   8;
    uint32_t //2
    GyroEn:         8,
    Opmode:         8,
    Plim:    8,
    SetCmd:      8;

    uint32_t //3
    DriveControlFltClr :          32;
    uint32_t //4
    DriveControlFltHstClr: 32;
    uint32_t//5
    PlimMax:           16,
    PDO_down_Ax1_reserved11:        16;
    uint32_t//6
    PlimMin:        16,
    PDO_down_Ax1_reserved12:        16;
    uint32_t//7
    MoveAbs:       32;
    uint32_t//8
    MoveInc:        32;
    uint32_t//9
    Jog:       32;
    uint32_t//10
    DriveControlWrnClr:        32;
    uint32_t//11
    FOV_Asimuth:16,
    PDO_down_Ax1_reserved5:        16;
    uint32_t//12
    PDO_down_Ax1_reserved6:       32;
    uint32_t//13
    PDO_down_Ax1_reserved7:  32;



    uint32_t//Axis1 //1
    Ax2DriveControlEn:   8,
    PDO_down_Ax2_reserved2 :   8,
    PDO_down_Ax2_reserved3:   8,

    Ax2DriveControlReserved:   8;
    uint32_t//2

    Ax2GyroEn:         8,
    Ax2Opmode:         8,
    Ax2Plim:    8,
    Ax2SetCmd:      8;
    uint32_t//3
    Ax2DriveControlFltClr:          32;
    uint32_t//4
    Ax2DriveControlFltHstClr :          32;

    uint32_t//5
    Ax2PlimMax:           16,
    Ax2PlimMaxAtLimit:           16;
    uint32_t//6
    Ax2PlimMin:        16,
    Ax2PlimMinAtLimit:           16;
    uint32_t//7
    Ax2MoveAbs:       32;
    uint32_t//8
    Ax2MoveInc:        32;
    uint32_t//9
    Ax2Jog:       32;
    uint32_t//10
    Ax2DriveControlWrnClr:        32;
    uint32_t//11
    FOV_Elevation:16,
    PDO_down_Ax2_reserved5:        16;
    uint32_t//12
    PDO_down_Ax2_reserved6:       32;
    uint32_t//13
    PDO_down_Ax2_reserved7:  32;



}Drive_PDO_Dwn_Bits_Tag;

// PDO UP Packet Definition
typedef struct
{
    uint32_t//4
    MsgType:           16,
    Drive_PDO_Up_GBL_reserved1:           16;

    uint32_t//global  //2
    Runtime:      32;
    uint32_t//3
    VBus :          16,
    VLogic:         16;

    uint32_t//5
    CPLD_Status:                        16,
    GyroYaw:           16;
    uint32_t//6
    GyroPitch:                        16,
    GyroRoll:           16;
    uint32_t//global  //1
    Drive_PDO_Up_GBL_reserved4:      32;
    uint32_t//global  //1
     Drive_PDO_Up_GBL_reserved5:      32;
     uint32_t//global  //1
      Drive_PDO_Up_GBL_reserved6:      32;
      uint32_t//global  //1
       Drive_PDO_Up_GBL_reserved7:      32;
       uint32_t//global  //1
        Drive_PDO_Up_GBL_reserved8:      32;


    uint32_t//Axis1  //1
    DriveState:     8,
    GyroEn:         8,
    Fbk_Type:         8,
    Temperature:    8;

    uint32_t//2
    Faults:           32;
    uint32_t//3
    FaultsHst:           32;

    uint32_t//4
    Current:        16,
    CurrentCommand: 16;
    uint32_t//5
    GyroVfb:        32;
    uint32_t//6
    LoadPfb:        32;
    uint32_t//7
    MotorPfb:       32;
    uint32_t//8
    LoadVfb:        32;
    uint32_t//9
    MotorVfb:       32;
    uint32_t//10
    GyroPfb:        32;
    uint32_t//11
    SystemCommand:        32;
    uint32_t//12
    IntegratedDegPosition:       32;
    uint32_t//13
    PositionError:  32;
    uint32_t//14
    Vcmd:           32;

    uint32_t//15
    Warnings:           32;
    uint32_t//16
    CMD_Acknowledge:           8,
    Drive_PDO_Up_AX1_reserved10:           8,
    Drive_PDO_Up_AX1_reserved11:           16;
    uint32_t//17
    Drive_PDO_Up_AX1_reserved12:      32;

    uint32_t//18
       Drive_PDO_Up_AX1_reserved13:      32;

    uint32_t//Axis1  //1
    Ax2DriveState:     8,
    Ax2GyroEn:         8,
    Ax2Fbk_Type:         8,
    Ax2Temperature:    8;

    uint32_t//2
    Ax2Faults:         32;
    uint32_t//3
    Ax2FaultsHst:           32;

    uint32_t//4
    Ax2Current:        16,
    Ax2CurrentCommand: 16;
    uint32_t//5
    Ax2GyroVfb:           32;
    uint32_t//6
    Ax2LoadPfb:        32;
    uint32_t//7
    Ax2MotorPfb:       32;
    uint32_t//8
    Ax2LoadVfb:        32;
    uint32_t//9
    Ax2MotorVfb:       32;
    uint32_t//10
    Ax2GyroPfb:        32;
    uint32_t//11
    Ax2SystemCommand:        32;
    uint32_t//12
    Ax2IntegratedDegPosition:       32;
    uint32_t//13
    Ax2PositionError:  32;
    uint32_t//14
    Ax2Vcmd:           32;
    uint32_t//15

    Ax2Warnings:           32;
    uint32_t//16
    Ax2CMD_Acknowledge:           8,
    Drive_PDO_Up_AX2_reserved10:           8,
    Drive_PDO_Up_AX2_reserved11:           16;
    uint32_t//17
    Drive_PDO_Up_AX2_reserved12:      32;

    uint32_t//18
       Drive_PDO_Up_AX2_reserved13:      32;

}Drive_PDO_Up_Bits_Tag;


typedef union
{
    uint32_t		Word[PDO_DOWN_MSG_SIZE/4];
    Drive_PDO_Dwn_Bits_Tag	Bits;
}PDO_Dwn_Packet_Tag;

typedef union
{
    uint32_t		Word[PDO_UP_MSG_SIZE/4];
    Drive_PDO_Up_Bits_Tag		Bits;
}PDO_Up_Packet_Tag ;


#endif
