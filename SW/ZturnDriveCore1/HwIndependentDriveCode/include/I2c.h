#ifndef I2C_H
#define I2C_H

void InitI2C(void);
void InitI2CGpio(void);

int16_t I2C_Write_EEPROM_Int (int16_t address, int16_t data  );
int16_t I2C_Read_EEPROM_Int(uint16_t address);




#endif
