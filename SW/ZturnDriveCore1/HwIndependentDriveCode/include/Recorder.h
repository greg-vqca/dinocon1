#ifndef RECORER_H
#define RECORER_H

 void Record_func(void);

#define RECORD_CHANNELS 4
#define MAX_RECORD_LENGTH 8000
typedef enum
{
	idleRecord ,
	initRAM ,
	initRecord ,
	preSample ,
	waitForTriger ,
	postSample,
	finishRecord
}RecorderState_tag;

typedef enum
{
	ImmTrigger ,
	RisingTrigger ,
	FallingTrigger
}	TrigMode_tag;

typedef struct
{
    uint16_t 				Done;
	uint16_t 				Address;
	int32_t 				Channel_Index[RECORD_CHANNELS];
	int32_t 				Trig_Channel;
	int32_t				Trig_Value;
	TrigMode_tag 		Trig_Mod;
	uint16_t 				TrigPosition;
	uint16_t 				NumberOfChannels;
	uint16_t 				Length;
	uint16_t 				Sample_Period;
	RecorderState_tag	State;
	int16_t 				Output;
	//int16_t 				Memory[8000];

}Record_tag;

extern  Record_tag 	Record;

#endif

