#ifndef GLOBALS
#define GLOBALS
#include <stdint.h>
#include <stdio.h>
#include "HardwareDefenitions.h"
//typedef short           int16_t;
//typedef long            int32_t;
//typedef uint16_t   uint8_t; //they are the same in TI
//typedef  int16_t           int8_t; //they are the same in TI
//typedef unsigned short  uint16_t;
//typedef unsigned long   uint32_t;
typedef float           float32_t;
//typedef long double     float64_t;
//typedef long long           int64_t;
//typedef unsigned long long           uint64_t;




#define NumberOfAxes 2

#define LoadEncoderSmoothingBaseOnMotorEncoder  1
#define AutoFeedbackFailRecovery                1

#define PositionRevolutionRes (long)3600000

#define PositionOverflowValue (long)3600000

#define MaxLong 2147483647
#define MaxInt 32767

#define NUMBER_OF_PARAMETERS    242
#define NUMBER_OF_FUNCTIONS 30

#define SERIAL_BUFFER_SIZE 1000

#define PDO_DOWN_MSG_SIZE 120
#define PDO_UP_MSG_SIZE 184


//#define KerenVer

#ifdef KerenVer
#define  SPI_TO_CPLD_LENGHT 18

#else
#define  SPI_TO_CPLD_LENGHT 21
#endif



#define MSG_TYPE_PDO 0x26
#define MSG_TYPE_SDO 0x36

typedef struct
{
    int32_t         Gi;
    int32_t         GiSat;
    int32_t             Gp;
    int32_t             Gd;
    int32_t         Gff;
    int32_t         GiAdap;
    int32_t             GpAdap;


    float32_t           f_Gi;
    float32_t           f_GiSat;
    float32_t           f_Gp;
    float32_t           f_Gd;
    float32_t           f_Gff;
    float32_t           f_GiAdap;
    float32_t           f_GpAdap;


    int32_t                 ErrorInteg;

}Params_tag;

typedef enum BITState_Tag
{
    IdleBIT,
    InitBIT,
    PerformePhaseAandC_BIT,
    PerformePhaseAandB_BIT,

    FinishBIT
}BITState;

typedef enum BITResults_Tag
{
    Noresult            =0x00000000,
    FaultDuringTest     =0x00000001,
    MotorPhaseA        =0x00000002,
    MotorPhaseB        =0x00000004,
    MotorPhaseC        =0x00000008,

    MotorPhaseA_CurrentSence        =0x00000080,
    MotorPhaseC_CurrentSence        =0x00000100,

    BitFinished        =0x80000000

}BITResults;


typedef struct BITStruct_Tag
{
    int32_t         BIT_Table[3][16];
    BITState        State;
    int32_t             BIT_timeCNT;
    BITResults           Results;
    int32_t Phase_resistance;
    int32_t Phase_inductance;

}BITStruct;

typedef enum
{
    disable = 0,
    enable = 1,
    fault = 2,
    regen_protection = 3
}Drive_state_tag;

typedef enum
{
    torque = 0,

    position = 3
}Opmode_tag;

typedef enum
{
    serial = 0,
    ext_cmd_in_0 = 1,
    preset_vel = 2
}Input_cmd_tag;




typedef enum
{
    no_warnings                 = 0x00000000,
    eeprom_write_warning        = 0x00000001,
    VN_warning                  = 0x00000002,
    over_voltage_warning        = 0x00000004,
    under_voltage_warning       = 0x00000008,
    eeprom_read_warning         = 0x00000010,
    sci_buff_OF_warning         = 0x00000020,
    eeprom_checksum_warning     = 0x00000040,
    low_priority_int_warning    = 0x00000080,
    CPLD_SPI_warning            = 0x00000100,
    CPLD_SPI_CHK_SUM_warning    = 0x00000200,
    GYRO_warning                = 0x00000400,
    ReservedWarning1            = 0x00000800,
    ReservedWarning2            = 0x00001000,
    ReservedWarning3            = 0X00002000,
    ReservedWarning4            = 0X00004000,
    ReservedWarning5            = 0x00008000,
    PositionError_warning       = 0x00010000,
    AbsEncNotInUse_warning      = 0x00020000,
    over_speed_warning          = 0x00040000,
    no_abs_position_warning     = 0x00080000,
    IPM_over_temp_warning       = 0x00100000,
    i2lim_warning               = 0x00200000,
    IncrEnc_warning             = 0x00400000,
    illegal_halls_warning       = 0x00800000,
    AbsEncMagnetDistance_warning= 0x01000000,
    AbsEncCommunicaion_warning  = 0x02000000,
    ReservedWarning13           = 0x04000000,
    ReservedWarning14           = 0x08000000,
    ReservedWarning15           = 0x10000000

}Warnings_tag;



typedef enum
{
    no_faults               = 0x00000000,
    CPLD_spi_not_started    = 0x00000001,
    under_voltage_flt       = 0x00000002,
    watchdog_flt            = 0x00000004,
    Incorect_CPLD_Version   = 0x00000008,
    Eeprom_read_flt           = 0x00000010,
    Eeprom_write_flt           = 0x00000020,
    over_voltage_flt           = 0x00000040,
    FLT_RESERVED5           = 0x00000080,
    FLT_RESERVED6           = 0x00000100,
    FLT_RESERVED7           = 0x00000200,
    FLT_RESERVED8           = 0x00000400,
    FLT_RESERVED9           = 0x00000800,
    FLT_RESERVED10          = 0x00001000,
    FLT_RESERVED11          = 0X00002000,
    FLT_RESERVED12          = 0X00004000,
    FLT_RESERVED13          = 0x00008000,
    over_current            = 0x00010000,
    motor_encoder_flt       = 0x00020000,
    position_error_flt      = 0x00040000,
    motor_over_temp_flt     = 0x00080000,
    illegal_halls_flt       = 0x00100000,
    general_pos_fbk_flt     = 0x00200000,
    hw_over_current_flt     = 0x00400000,
    FLT_RESERVED21          = 0x00800000,
    FLT_RESERVED22          = 0x01000000,
    FLT_RESERVED23          = 0x02000000,
    FLT_RESERVED24          = 0x04000000,
    FLT_RESERVED25          = 0x08000000


}Faults_tag;

typedef enum
{
    PTP_IDLE                = 0x0000,
    PTP_IN_MOTION       = 0x0001,
    PTP_INIT                = 0x0003
}PtpStates_tag;



typedef struct

{
    Drive_state_tag     State;
    Drive_state_tag     PrevState;
    Opmode_tag          Opmode;
    Input_cmd_tag       Cmd_in;
    Warnings_tag          Warnings;

    Faults_tag          Faults;
    Faults_tag          Faults_history;

    int16_t             VBusMax;
    int16_t             VBusMin;
    int16_t             VBus;
    int16_t             VLogic;
    int32_t                 MaxCmd;
    int16_t             OverTemp;
    int16_t             TemperatureA;
    int16_t             TemperatureB;
    int16_t             TemperatureC;
    int16_t             TemperatureD;
    int16_t             TemperatureE;
    int16_t             CmdSrc;
    int16_t             DriveTemp_sense;

} Drive_tag;

typedef struct
{
    Params_tag          Param;
    int16_t                 Ia;
    int16_t                 Ib;
    int16_t                 Ic;

    int16_t                 Va;
    int16_t                 Vb;
    int16_t                 Vc;
    int16_t                 ia_cmd;
    int16_t                 ib_cmd;
    int16_t                 ic_cmd;
    int16_t                 I;
    int32_t      Commutation_timer;

    int16_t             Imax;
    int16_t             Icont;
    int16_t             Iflt;
    uint16_t                Phase;
    int16_t                 Command;
    int16_t*               CommandPtr;

    uint32_t            ForcedCommutCntr;
    int16_t             ForcedCommutFlag;
    int16_t                 Effort;
    int16_t                 Ia_OffSet;
    int16_t                 Ic_OffSet;
    int16_t                 Va_offset;
    int16_t                 Vb_offset;
    int16_t                 Vc_offset;
    int16_t             Phase_A_output;
    int16_t             Phase_B_output;
    int16_t             Phase_C_output;
    int16_t             AntiBachlashCurrent;


    int16_t             Effort_A_output;
    int16_t             Effort_B_output;
    int16_t             Effort_C_output;
    float32_t               CosComponent;
    float32_t               SinComponent;
    int32_t             I2T_lim;
    int64_t         I2T_integrator;
    int64_t         I2T_64;
    int32_t     I2T_Value;
    int32_t     IMax_Value;



}Current_tag;

typedef struct
{
    int16_t                 Icmd;
    int16_t                 Bcmd;
    int32_t                 Vcmd;
    int32_t                 Jcmd;
    int32_t                 ExternCmd;
    int32_t                 ExternCmdScale;



}Serial_tag;

typedef struct
{
    Params_tag          Param;
    int32_t                 Command;
    int32_t*               CommandPtr;



    int32_t                 LoadEncVfb;
    int32_t                 MotorEncVfb;
    int32_t               MotorEncVfbAbs;

    int32_t                 Vmax;
    int32_t                 Vmin;
    int32_t             Vel_Commutation_timer;
    int32_t                 Vflt;
    int32_t             LoadEncVDegPS;
    int32_t               MotorEncVDegPS;
    float32_t              MotorEncVelocityScaling;
    float32_t              DegVelocityScaling;

    float32_t               LoadEncVelocityScaling;
    int32_t             LoadEncPositionCapturedValue;
    int32_t             MotorEncPositionCapturedValue;

    uint16_t                VelFilter;
    int16_t                 Output;
    int16_t                 FilterFc;
    int16_t                 Filter;
    int16_t             VelLoopSaturated;
}Velocity_tag;

typedef struct
{

    int32_t                 Command;
    int64_t                 Vout;
    int32_t                 Output;
  //  int32_t             ScaledAcc;
  //  int32_t             ScaledDec;


}Jog_tag;

typedef struct
{
    Params_tag          Param;
    int32_t                 SystemCommand;
    int32_t               LimitedCommand;
    int32_t               LimitedToSystemDistance;
    float32_t               LimitedToSystemDistanceAcc;
    float32_t               LimitedToSystemDistanceOut;

    int32_t               LimitedToSystemDistanceMaxPos;
    int32_t               LimitedToSystemDistanceMaxNeg;
    float32_t               JogInPosVelocityOutput;
    int32_t               JogInPosVelocityOutputFixedPoint;
    float32_t               JogInPosVelocityOutputFix;

    float32_t               JogInPosAcceleration;

    int32_t                 PrevCommand;
    int32_t               LastCommand;
    int32_t                 Pfb;
    int32_t                 Pmax;
    int32_t                 Pmin;
    int32_t                 Pemax;
    int16_t Plim;
    int32_t PlimMax;
    int32_t PlimMin;
    int32_t FOV;
     int32_t                 Pe;
    int32_t               LimitedPe;

    int32_t               AbsPe;
    int32_t AbsLimitedPe;
    int64_t             Pe64;
    int32_t             GyroP;

    int32_t                 PeInteg;
    int32_t             PrevError;
    int32_t             Output;
    int32_t             OutputEnc;

    int16_t                 StopFlag;


    int16_t     PosLoopSaturated;
    int16_t     PosCommandSaturated;

}Position_tag;



typedef struct
{
    int32_t                 Command;
    int32_t                 TempCommand;
    int32_t                 EndCommand;
    float32_t               DistanceLeft_f;
    float32_t               Vel_f;
    int32_t                 FF_ACC;
    int32_t                 Output;
    uint16_t                Acc;
    float32_t               ScaledAcc_f;
    int32_t                 Error;
    uint16_t                Dec;
    float32_t               ScaledDec_f;
    int32_t                 Vcruise;
    float32_t               ScaledVCruise_f;
    PtpStates_tag       State;
}Ptp_tag;



typedef struct
{
    int32_t   EncPosition;

    int32_t   ShadowEncPosition;

    int32_t     PrevEncPosition;
    uint16_t   EncPositionStatus;

    int32_t   DegPosition;
    int32_t   DegPositionOffset;
    int32_t   SmoothDegPosition;
    uint32_t           EncResolution;
    float32_t DegScalingFactor;

}LoadEncTag;

typedef struct
{
    float32_t           PRD_ScalingFactor;

    int32_t           EncPosition;
    int32_t           DegPosition;
    int32_t           EncPositionScaledToDeg;
    int32_t           DegPositionOffset;
    int32_t           PrevEncPositionScaledToDeg;
    uint32_t        EncResolution;

    float32_t DegScalingFactor;

}MotorEncTag;


typedef struct
{

    int32_t     Position;
    int32_t     DegPosition;
    int32_t     PositionScaledToDeg;
    int32_t     PrevPositionScaledToDeg;
    float32_t DegScalingFactor;

}HallsTag;


typedef struct
{
    int32_t     IntegratedDegPosition;


    int16_t     VN_Warning  ;
    int16_t     VN_GPS_StabilizationDelay  ;
    int32_t     GyroVelocty;
    int32_t     GyroPrevVelocty;
    float32_t   GyroVeloctyChange;
    float32_t   ShadowGyroVeloctyChange;
    int32_t     BaseGyroPos;
    int32_t     BaseVNPos;

    int32_t     ShadowGyroPos;
    int32_t     GyroPrevPos;
    int32_t     GyroPosChange;
    int32_t GyroOffset32;
    int16_t GyroOffset;
    int32_t GyroPosOffset;
    int32_t GyroFiltered;
    int32_t GyroItegrated;
    int32_t GyroShiftedoutItegrated;
    int32_t GyroFiltered64;



    uint16_t            MechPos;
    int16_t         Direction;

    HallsTag Halls;
    MotorEncTag Motor;
    LoadEncTag Load;
    int16_t           UseLoadEncoder;

}Feedback_tag;





typedef enum
{
    Star,
    Delta,
    Serial_Delta,
    Serial_Star,
    Parralell_Delta,
    Parralell_Star

}ArangementType_tag;


typedef struct
{
    int16_t         Poles;

    ArangementType_tag  Arangement;

    int16_t         GearNominator;
    int16_t         GearDenominator;
    float32_t           GearScaling;
    uint16_t            Phaseoffset;
    int16_t             Phase;
    int16_t           PhaseAdvance;
    int16_t           PhaseAdvanceScale;
    int16_t           PhaseAdvanceLim;

    int16_t             HallsState;
    int16_t             HallsPosition;
    int16_t     BEMF_HallsPosition;
    int16_t     BEMF_HallsPositiontemp;

    int16_t             Direction;
    int16_t         Cmd_pol;
    int16_t         Fbk_pol;

    uint16_t    ComutationAngle;
    float32_t   CosComponent;
    float32_t    SinComponent;
    int32_t AComponent;
    int32_t  BComponent;
    int32_t CComponent;
    int16_t Rotor_pos;
}Motor_tag;


// data structure:          controll32b     controll32b2    j   j2      mi      mi2     XX      XX2     chksum
// return data structure:   status32b       status32b2      pfb pfb2    encpfb  encpfb2 vfb     vfb2    chksum


typedef struct
{
    uint16_t//
    mode:           2,
    GNSS_Fix:           1,
    error:           4,
    reserved:           1,
    GNSS_HeadingIns:           1,
    GNSS_Compas:           1,
    reserved2:           6;

}VN_INS_Status_Bits_Tag;

typedef union
{
    uint16_t       Word;
    VN_INS_Status_Bits_Tag  Bits;
}VN_INS_Status_tag;



// variables

typedef struct
{
    Drive_tag Drive;
    Current_tag Current;
    Jog_tag Jog;

    Velocity_tag Velocity;
    Position_tag Position;
    Ptp_tag Ptp;


    Feedback_tag  Feedback;
    Motor_tag Motor;
    int16_t CurrLoopSaturated;
    int16_t InitHallsNeededFlag;
    Serial_tag Serial;
    BITStruct BuidInTest;

} Axis_tag;


typedef enum
{
    SCI_A=0,
    SDO = 1
}CMD_Src_tag;

extern CMD_Src_tag CMD_Src;


extern  Axis_tag        Axis[NumberOfAxes];

//extern int32_t CustomIQmpy(float32_t a, float32_t b, int16_t c);
//#define CustomIQmpy(a,b,c) (int32_t) ((float32_t)(a)*(float32_t)(b)/(float32_t)((int32_t)1<<(c)))
void OverFlowPosOnDeg (int32_t * d);
void OverFlowPosDerivOnDeg (int32_t * d);
int32_t CustomIQsat(int32_t a, int32_t max, int32_t min);
float32_t f_CustomIQsat(float32_t a, float32_t max, float32_t min);
int16_t CheckNegPlim(int32_t Position,int32_t NegPLim);
int16_t CheckPosPlim(int32_t Position,int32_t PosPLim);


/*
Test_startTime = GetCpuTimer1Value();

    Test_endTime = GetCpuTimer1Value();

    Test_runTime = (Test_startTime - Test_endTime) *0.0111083984375;//in usec
    if(MaxTest_runTime<Test_runTime)MaxTest_runTime=Test_runTime;
 */
#endif
