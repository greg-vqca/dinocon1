#ifndef STABILIZATIONHANDLER_H
#define STABILIZATIONHANDLER_H

typedef struct
{
    int32_t    GyroVelocty;
    int32_t   GyroPrevVelocty;

    int32_t GyroPos;

    int32_t   GyroPrevPos;
    int32_t   GyroPosChange;

    int32_t GyroOffset32;
    int16_t GyroOffset;
    int32_t GyroPosOffset;

    int32_t GyroFiltered;
    int32_t GyroItegrated;
    int32_t GyroShiftedoutItegrated;


    int32_t GyroFiltered64;


    int16_t ACC_X;
    int16_t ACC_Y;
    int16_t AbsACC_X;
    int16_t AbsACC_Y;
    Angles_struct Angle;
    Angles_struct Rate;
    int32_t AbsRatePitch;
    int32_t AbsRateYaw;
    int32_t AbsRateRoll;


}BaseGyroTag;


typedef struct
{


    int16_t     GyroStabilization  ;
    int16_t     GyroStabilizationWarning  ;


    int32_t InMotionCounter;
    int16_t InMotion;
    BaseGyroTag BaseGyro;

}StabilizationTag;


extern  StabilizationTag        Stabilization;

extern Quaternion_struct/* Quaternion_VN,Quaternion_VN_body,Quaternion_VN_base,Quaternion_Vn,*/ Quaternion_base,Quaternion_weapon,Quaternion_body,Quaternion_rotation,Qt1, Qt2;
extern Angles_struct BaseAngles;//,VN_Angles, VN_BaseAngles, VN_Acceleration;
extern Angles_struct WeaponAngles,BodyAngles,RotationAxis;
extern Angles_struct At1, At2;

void Handle_Stabilization(void);


#endif
