#ifndef SPI_HANDLER_H
#define SPI_HANDLER_H



typedef enum
{
    CPLD_STATUS_SSI_axis_0_flt  =  0x0001,
    CPLD_STATUS_SSI_axis_1_flt =   0x0002,
    CPLD_STATUS_BaseGyro_flt   = 0x0004,
    CPLD_STATUS_VN_Data_flt  =  0x0008
}CPLD_STATUS_tag;


extern CPLD_STATUS_tag          CPLD_Status;
extern volatile int32_t LocalSPI_RX_DATA[2*SPI_TO_CPLD_LENGHT+1];
extern int32_t SPI_RX_DATA[SPI_TO_CPLD_LENGHT+1];
extern volatile int32_t ShadowSPI_RX_DATA[SPI_TO_CPLD_LENGHT+1];

#endif
