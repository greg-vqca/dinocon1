################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../HwIndependentDriveCode/source/RealTime/RealTime.c 

O_SRCS += \
../HwIndependentDriveCode/source/RealTime/CurrentLoopDQ.o \
../HwIndependentDriveCode/source/RealTime/EncHandler.o \
../HwIndependentDriveCode/source/RealTime/FaultHandler.o \
../HwIndependentDriveCode/source/RealTime/HallsHandler.o \
../HwIndependentDriveCode/source/RealTime/I2T_Handler.o \
../HwIndependentDriveCode/source/RealTime/JogHandler.o \
../HwIndependentDriveCode/source/RealTime/PTPGeneratorFPU.o \
../HwIndependentDriveCode/source/RealTime/PosLoop.o \
../HwIndependentDriveCode/source/RealTime/QuaternionMath.o \
../HwIndependentDriveCode/source/RealTime/Recorder.o \
../HwIndependentDriveCode/source/RealTime/SPI_Handler.o \
../HwIndependentDriveCode/source/RealTime/StabilizationHandler.o \
../HwIndependentDriveCode/source/RealTime/VelLoop.o 

OBJS += \
./HwIndependentDriveCode/source/RealTime/RealTime.o 

C_DEPS += \
./HwIndependentDriveCode/source/RealTime/RealTime.d 


# Each subdirectory must supply rules for building sources it contributes
HwIndependentDriveCode/source/RealTime/%.o: ../HwIndependentDriveCode/source/RealTime/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM v7 gcc compiler'
	arm-none-eabi-gcc -Wall -O0 -g3 -I"D:\FreeLance\XilinxWorkspace\Dinocon1\SW\ZturnDriveCore1\HAL" -I"D:\FreeLance\XilinxWorkspace\Dinocon1\SW\ZturnDriveCore1\HW_System\include" -I"D:\FreeLance\XilinxWorkspace\Dinocon1\SW\ZturnDriveCore1\HwIndependentDriveCode\include" -c -fmessage-length=0 -MT"$@" -mcpu=cortex-a9 -mfpu=vfpv3 -mfloat-abi=hard -ID:/FreeLance/XilinxWorkspace/Dinocon1/SW/ZturnDesign_wrapper/export/ZturnDesign_wrapper/sw/ZturnDesign_wrapper/domain_ps7_cortexa9_1/bspinclude/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


