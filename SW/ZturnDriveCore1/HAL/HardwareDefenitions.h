
#ifndef HARDWAREDEFENITIONS_H
#define HARDWAREDEFENITIONS_H
#include "../HW_System/include/platform.h"
#include "xil_types.h"

#include "xtmrctr.h"
#include "xparameters.h"

#include "xil_io.h"
#include "xil_exception.h"
#include "xscugic.h"

#include "xil_printf.h"

#include "GlobalDefines.h"

#define  TI_DSP_Pragma 0
#define  Xilinx_Pragma 1

extern uint16_t ramfuncs_loadstart;
extern size_t ramfuncs_loadsize;
extern uint16_t ramfuncs_runstart;


// DSP281x Headerfile Include File
extern int16_t  PWM_TBPRD;
extern	int16_t	DeadTime;
extern const int32_t Flash_Start;
extern const int32_t Flash_End ;
extern const int32_t MaxPhaseCurrent;
extern int16_t  HWD_BootStrap;
#define NUMBER_OF_COUNTS_FOR_INTERRUPT 5000 //100mghz clock (25 usec for realtme)
#define  NUMBER_OF_COUNTS_FOR_LPINTERRUPT 50000
#define HWD_MaxCurrent 30000


#define HWD_EPWM1_MIN_DB 40
#define HWD_SSIBUFSIZE 30
#define HWD_MaxCurrent 30000
#define HWD_SERIAL_BUFFER_SIZE 4

#ifdef KerenVer
#define  SPI_TO_CPLD_LENGHT 18

#else
#define  SPI_TO_CPLD_LENGHT 21
#endif

#define HWD_MAX_PWM (int32_t)(PWM_TBPRD-HWD_BootStrap)
extern uint16_t DMA_sdata[SPI_TO_CPLD_LENGHT*2+1];    // Sent Data
extern uint16_t DMA_rdata[SPI_TO_CPLD_LENGHT*2+1];    // Received Data

int16_t I2C_Read_CPLD_Byte(uint16_t address);

void HWD_EnablePwm(uint16_t a);
void HWD_DisablePwm (uint16_t a);
void PWM_out( int16_t axis,int16_t phase,int16_t dutycycle);
void PWM_Enable( int16_t axis,int16_t phase,int16_t state);
int16_t GetHallsScensorsReading(int16_t axis);
void SetIncrementalEncoderCounterResolution(int16_t a, int32_t CntsPerRev);

uint32_t GetIncrementalEncoderCounterValue(int16_t a);
void SetIncrementalEncoderCounterDirection(int16_t a, int32_t dir);
void SetIncrementalEncoderCounterValue(int16_t a, uint32_t val);
int16_t GetBoardTemperatureReading(int16_t axis);
void EnableInterupts (int16_t cmd);
void SetupInterupts (void);
void DisableWatchDog(void);
void EnableWatchDog(void);
void WriteDacOutput(int16_t dacOutput);
void ServiceWatchDog(void);
void FpgaSyncSignalSet(int16_t val);
void StartInterupts(void);
void StopInterupts(void);

void Handle_SPI_Data(void);
uint16_t ReadRamInt(uint16_t addr);

void ClearSCIA_ERRORS(void);

uint32_t GetCpuTimer1Value(void);
void SetCpuTimer1Value(uint32_t val);
void ADC_Offset_Callculation (int16_t axis,int16_t *Ia_offset ,int16_t *Ic_offset);
int16_t DMA_Transfer_finished(void);
void Get_DMA_Data(void);
void Do_DMA_Transfer(void);
void SetSPI_A_messageSize(int16_t val);
void SetupExternalRAM_ForRecord(int16_t val);
void InitializeExternalRAM_ForRecord(void);
void AlowSCIBandLowPriorityInterupts (void);

void AcknowledgeSCIB_Interupt (void);
void AcknowledgeHighPriority_Interupt(XTmrCtr *data, uint8_t TmrCtrNumber);

void TemperatureCompensateInternalOscilator(void);
void uSleep (int16_t A);
void InitPwm(int16_t PWM_Freq);
void InitAdc(void);

void InitFlash(void);
int InitSystem(void);
void uSleep (int16_t A);

void SetRedLED(int16_t axis,int16_t state);
void SetGreenLED(int16_t axis,int16_t state);
int16_t GetLogicVoltage(void);
int16_t GetBusVoltage(void);
void TriggerLowPriorityInterupt(void);
void AcknowledgeLowPriorityInterupt(void);
int16_t GetMotorOverTemperatureState(int16_t axis);
int16_t GetCurrentReadingPhaseA(int16_t axis);
int16_t GetCurrentReadingPhaseC(int16_t axis);


uint16_t GetSPIA_TX_Buffer_LVL(void);

void ClearSCIB_ERRORS(void);
void SetSPIA_TX_Buffer(uint16_t val);
void SendDataToRAM(uint16_t cmdData);

uint16_t GetSPIA_RX_Buffer(void);
uint16_t GetSCIA_RX_Buffer(void);
uint16_t GetSCIB_RX_Buffer(void);

uint16_t GetSCIA_RX_Buffer_LVL(void);
uint16_t GetSCIB_RX_Buffer_LVL(void);

uint16_t GetSCIA_TX_Buffer_LVL(void);
uint16_t GetSCIB_TX_Buffer_LVL(void);
void SetSCIA_TX_Buffer(uint16_t val);
void SetSCIB_TX_Buffer(uint16_t val);

void OverCurrentErrorClear (void);
int16_t OverCurrentErrorDetected (void);



#endif
