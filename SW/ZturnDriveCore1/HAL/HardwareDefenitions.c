//
#include "GlobalIncludes.h"
#include "../HW_System/include/platform.h"
#include "xil_types.h"
#include "xuartps_hw.h"
#include "xtmrctr.h"
#include "xparameters.h"

#include "xil_io.h"
#include "xil_exception.h"
#include "xscugic.h"

#include "xil_printf.h"


int16_t ReplaceRegRead;
int16_t ReplaceRegWrite;


int16_t  HWD_BootStrap;
int16_t  PWM_TBPRD;
int16_t DeadTime;
uint16_t DMA_sdata[SPI_TO_CPLD_LENGHT*2+1];    // Sent Data
uint16_t DMA_rdata[SPI_TO_CPLD_LENGHT*2+1];    // Received Data
volatile int32_t ShadowSPI_RX_DATA[SPI_TO_CPLD_LENGHT+1];

uint16_t SCIB_BUFFRE_OVERFLOW=0;
uint16_t SCIA_BUFFRE_OVERFLOW=0;

#define HWD_FLASH_START ((int32_t) 0x300000)
#define HWD_FLASH_END    ((int32_t) 0x324000+ 0x02A00 )

const int32_t flash_start =HWD_FLASH_START;
const int32_t flash_end =HWD_FLASH_END;

#define HWD_AcknowledgeLPI ReplaceRegWrite
#define HWD_EncoderDirection ReplaceRegWrite
#define  HWD_EncoderCounter_Axis1 ReplaceRegRead
#define  HWD_EncoderCounter_Axis2 ReplaceRegRead


//#define HWD_REGEN_PROTICETION_MIN_VEL 1000
#define HWD_PWM_C_Out_Axis1 ReplaceRegWrite
#define HWD_PWM_B_Out_Axis1 ReplaceRegWrite
#define HWD_PWM_A_Out_Axis1 ReplaceRegWrite
#define HWD_PWM_C_Out_Axis2 ReplaceRegWrite
#define HWD_PWM_B_Out_Axis2 ReplaceRegWrite
#define HWD_PWM_A_Out_Axis2 ReplaceRegWrite


#define HWD_TrigerLPI ReplaceRegWrite


#define HWD_SetGreenLED_Axis1 ReplaceRegWrite
#define HWD_SetRedLED_Axis1 ReplaceRegWrite
#define HWD_SetGreenLED_Axis2 ReplaceRegWrite
#define HWD_SetRedLED_Axis2 ReplaceRegWrite


#define HWD_Green_LED_CLEAR_Axis1 ReplaceRegWrite
#define HWD_Red_LED_CLEAR_Axis1 ReplaceRegWrite
#define HWD_Green_LED_CLEAR_Axis2 ReplaceRegWrite
#define HWD_Red_LED_CLEAR_Axis2 ReplaceRegWrite


#define HWD_FPGA_SyncSet ReplaceRegWrite

#define HWD_FPGA_SyncClear ReplaceRegWrite


#define HWD_PosLimitAxis1  (ReplaceRegRead)
#define  HWD_NegLimitAxis1 (ReplaceRegRead)
#define HWD_PosLimitAxis2  ReplaceRegRead
#define  HWD_NegLimitAxis2 ReplaceRegRead




#define HWD_COUNTS_IN_mSEC 12



#define  HWD_DSP_Temp_sense  ReplaceRegRead

#define  HWD_Temp_sense_AXIS1  ReplaceRegRead
#define  HWD_Temp_sense_AXIS2   ReplaceRegRead


#define  HWD_CURRENT_C_RESULT_AXIS1     ReplaceRegRead
#define  HWD_CURRENT_A_RESULT_AXIS1     ReplaceRegRead

#define  HWD_CURRENT_C_RESULT_AXIS2    ReplaceRegRead
#define  HWD_CURRENT_A_RESULT_AXIS2    ReplaceRegRead
#define HWD_AX1_MOTOR_OVER_TEMP        ReplaceRegRead
#define HWD_AX2_MOTOR_OVER_TEMP        ReplaceRegRead

#define  HWD_VOLTAGE_BUS    ReplaceRegRead
#define  HWD_VOLTAGE_LOGIC  ReplaceRegRead


#define HWD_Disable_PWM_A_AXIS1    ReplaceRegWrite


#define HWD_Disable_PWM_B_AXIS1    ReplaceRegWrite

#define HWD_Disable_PWM_C_AXIS1      ReplaceRegWrite

#define HWD_Enable_PWM_A_AXIS1  ReplaceRegWrite

#define HWD_Enable_PWM_B_AXIS1  ReplaceRegWrite



#define HWD_Enable_PWM_C_AXIS1  ReplaceRegWrite





#define HWD_Disable_PWM_A_AXIS2     ReplaceRegWrite

#define HWD_Disable_PWM_B_AXIS2    ReplaceRegWrite

#define HWD_Disable_PWM_C_AXIS2      ReplaceRegWrite
#define HWD_Enable_PWM_A_AXIS2  ReplaceRegWrite

#define HWD_Enable_PWM_B_AXIS2  ReplaceRegWrite

#define HWD_Enable_PWM_C_AXIS2 ReplaceRegWrite

#define WAIT_FOR_DATA       timeout=20000;\
        while (ReplaceRegRead ==0 && timeout > 0)\
        {\
            uSleep(1);\
            timeout--;\
        }\
        if (timeout == 0)\
        {\
            Eeprom_Read_Fault_Flag = 1;\
        }


#if TI_DSP_Pragma==1

#pragma FUNC_ALWAYS_INLINE(HWD_EnablePwm);
#pragma FUNC_ALWAYS_INLINE(HWD_DisablePwm);
#pragma FUNC_ALWAYS_INLINE(SetRedLED);
#pragma FUNC_ALWAYS_INLINE(SetGreenLED);
#pragma FUNC_ALWAYS_INLINE(GetLogicVoltage);
#pragma FUNC_ALWAYS_INLINE(GetBusVoltage);
#pragma FUNC_ALWAYS_INLINE(TriggerLowPriorityInterupt);
#pragma FUNC_ALWAYS_INLINE(AcknowledgeLowPriorityInterupt);
#pragma FUNC_ALWAYS_INLINE(GetMotorOverTemperatureState);
#pragma FUNC_ALWAYS_INLINE(GetCurrentReadingPhaseA);
#pragma FUNC_ALWAYS_INLINE(GetCurrentReadingPhaseC);
#pragma FUNC_ALWAYS_INLINE(PWM_out);
#pragma FUNC_ALWAYS_INLINE(PWM_Enable);
#pragma FUNC_ALWAYS_INLINE(GetHallsScensorsReading);
#pragma FUNC_ALWAYS_INLINE(SetIncrementalEncoderCounterResolution);
#pragma FUNC_ALWAYS_INLINE(SetIncrementalEncoderCounterDirection);
#pragma FUNC_ALWAYS_INLINE(GetIncrementalEncoderCounterValue);
#pragma FUNC_ALWAYS_INLINE(SetIncrementalEncoderCounterValue);
#pragma FUNC_ALWAYS_INLINE(FpgaSyncSignalSet);
#pragma FUNC_ALWAYS_INLINE(GetBoardTemperatureReading);
#pragma FUNC_ALWAYS_INLINE(EnableInterupts);
#pragma FUNC_ALWAYS_INLINE(SetupInterupts);
#pragma FUNC_ALWAYS_INLINE(AcknowledgeSCIB_Interupt);
#pragma FUNC_ALWAYS_INLINE(AcknowledgeHighPriority_Interupt);
#pragma FUNC_ALWAYS_INLINE(StartInterupts);
#pragma FUNC_ALWAYS_INLINE(StopInterupts);
#pragma FUNC_ALWAYS_INLINE(TemperatureCompensateInternalOscilator);
#pragma FUNC_ALWAYS_INLINE(uSleep);
#pragma FUNC_ALWAYS_INLINE(OverCurrentErrorDetected);
#pragma FUNC_ALWAYS_INLINE(OverCurrentErrorClear);
#pragma FUNC_ALWAYS_INLINE(GetSCIB_TX_Buffer_LVL);
#pragma FUNC_ALWAYS_INLINE(GetSCIA_TX_Buffer_LVL);
#pragma FUNC_ALWAYS_INLINE(GetSPIA_TX_Buffer_LVL);
#pragma FUNC_ALWAYS_INLINE(GetSPIA_TX_Buffer_LVL);
#pragma FUNC_ALWAYS_INLINE(GetSCIB_RX_Buffer_LVL);
#pragma FUNC_ALWAYS_INLINE(GetSCIA_RX_Buffer);
#pragma FUNC_ALWAYS_INLINE(GetSCIB_RX_Buffer);
#pragma FUNC_ALWAYS_INLINE(GetSPIA_RX_Buffer);
#pragma FUNC_ALWAYS_INLINE(SendDataToRAM);
#pragma FUNC_ALWAYS_INLINE(SetSPIA_TX_Buffer);
#pragma FUNC_ALWAYS_INLINE(SetSCIA_TX_Buffer);
#pragma FUNC_ALWAYS_INLINE(SetSCIB_TX_Buffer);
#pragma FUNC_ALWAYS_INLINE(ClearSCIB_ERRORS);
#pragma FUNC_ALWAYS_INLINE(ClearSCIA_ERRORS);
#pragma FUNC_ALWAYS_INLINE(DMA_Transfer_finished);
#pragma FUNC_ALWAYS_INLINE(ADC_Offset_Callculation);
#pragma FUNC_ALWAYS_INLINE(SetCpuTimer1Value);
#pragma FUNC_ALWAYS_INLINE(GetCpuTimer1Value);

#pragma FUNC_ALWAYS_INLINE(SetSPI_A_messageSize);
#pragma FUNC_ALWAYS_INLINE(SetupExternalRAM_ForRecord);
#pragma FUNC_ALWAYS_INLINE( InitializeExternalRAM_ForRecord);
#pragma FUNC_ALWAYS_INLINE( ReadRamLong);
#pragma FUNC_ALWAYS_INLINE( ReadRamInt);
#pragma CODE_SECTION(I2C_Write_EEPROM_Int, "ramfuncs");
#pragma FUNC_ALWAYS_INLINE( I2C_Read_EEPROM_Int);
#pragma FUNC_ALWAYS_INLINE( I2C_Read_CPLD_Byte);
#pragma FUNC_ALWAYS_INLINE(InitSystem);
#pragma FUNC_ALWAYS_INLINE( ServiceWatchDog);
#pragma FUNC_ALWAYS_INLINE( DisableWatchDog);
#pragma FUNC_ALWAYS_INLINE( EnableWatchDog);
#endif

static int PWM_Enabled_flg[2]={1,1};
void HWD_EnablePwm(uint16_t axis)
{
    int16_t temp=0;
    if (PWM_Enabled_flg[axis])
    {
        return;
    }
    if(axis==0u)
    {
        PWM_Enabled_flg[axis]=1;

        HWD_Enable_PWM_A_AXIS1;
        HWD_Enable_PWM_B_AXIS1;
        HWD_Enable_PWM_C_AXIS1;
    }
    else if(axis==1u)
    {
        PWM_Enabled_flg[axis]=1;


        HWD_Enable_PWM_A_AXIS2;
        HWD_Enable_PWM_B_AXIS2;
        HWD_Enable_PWM_C_AXIS2;
        temp++;

    }
}


void HWD_DisablePwm(uint16_t axis)
{

    
    if(axis==0u)
    {
        PWM_Enabled_flg[axis] = 0;

        HWD_Disable_PWM_A_AXIS1;
        HWD_Disable_PWM_B_AXIS1;
        HWD_Disable_PWM_C_AXIS1;
    }
    else if (axis==1u)
    {
        PWM_Enabled_flg[axis] = 0;

        HWD_Disable_PWM_A_AXIS2;
        HWD_Disable_PWM_B_AXIS2;
        HWD_Disable_PWM_C_AXIS2;

    }



}


void SetRedLED(int16_t axis,int16_t state)
{
    if(axis==0)
    {
        if(state==1)
        {
            HWD_SetRedLED_Axis1 =1;
        }
        else
        {
            HWD_Red_LED_CLEAR_Axis1=1;
        }
    }
    else
    {
        if(state==1)
        {
            HWD_SetRedLED_Axis2 =1;
        }
        else
        {
            HWD_Red_LED_CLEAR_Axis2=1;
        }
    }
}


void SetGreenLED(int16_t axis,int16_t state)
{
    if(axis==0)
    {
        if(state==1)
        {
            HWD_SetGreenLED_Axis1 =1;
        }
        else
        {
            HWD_Green_LED_CLEAR_Axis1=1;
        }
    }
    else
    {
        if(state==1)
        {
            HWD_SetGreenLED_Axis2 =1;
        }
        else
        {
            HWD_Green_LED_CLEAR_Axis2=1;
        }
    }
}

int16_t GetLogicVoltage(void)
{
    int16_t ret=0;
    ret=(float) HWD_VOLTAGE_LOGIC*0.1669921875;
    return ret;

}

int16_t GetBusVoltage(void)
{
    int16_t ret=0;
    ret=(float) HWD_VOLTAGE_BUS*0.1669921875;
    return ret;
}
void TriggerLowPriorityInterupt(void)
{
    HWD_TrigerLPI =1;

}

void AcknowledgeLowPriorityInterupt(void)
{
    ReplaceRegWrite;
}

int16_t GetMotorOverTemperatureState(int16_t axis)
{
    int16_t ret=0;
    if(axis==0)
    {
        ret=(HWD_AX1_MOTOR_OVER_TEMP>2048);
    }
    else
    {
        ret=(HWD_AX2_MOTOR_OVER_TEMP>2048);

    }
    return ret;

}

int16_t GetCurrentReadingPhaseA(int16_t axis)
{
    int16_t ret=0;
    if(axis==0)
    {
        ret= (float)HWD_CURRENT_A_RESULT_AXIS1 * 0.70361328125;
    }
    else
    {
        ret= (float)HWD_CURRENT_A_RESULT_AXIS2 * 0.70361328125;
    }
    return ret;
}

int16_t GetCurrentReadingPhaseC(int16_t axis)
{
    int16_t ret=0;
    if(axis==0)
    {
        ret= (float)HWD_CURRENT_C_RESULT_AXIS1 * 0.70361328125;
    }
    else
    {
        ret= (float)HWD_CURRENT_C_RESULT_AXIS2 * 0.70361328125;
    }
    return ret;
}

void PWM_out( int16_t axis,int16_t phase,int16_t dutycycle)
{

    if(axis==0)
    {

        switch (phase)
        {
        case 1:
            HWD_PWM_A_Out_Axis1=dutycycle;
            break;
        case 2:
            HWD_PWM_B_Out_Axis1=dutycycle;
            break;
        case 3:
            HWD_PWM_C_Out_Axis1=dutycycle;
            break;
        default:
            break;
        }

    }
    else
    {

        switch (phase)
        {
        case 1:
            HWD_PWM_A_Out_Axis2=dutycycle;
            break;
        case 2:
            HWD_PWM_B_Out_Axis2=dutycycle;
            break;
        case 3:
            HWD_PWM_C_Out_Axis2=dutycycle;
            break;
        default:
            break;
        }

    }
}

//phase 1- A  2 - B   3 - C    State 1- enabe   0- disable
void PWM_Enable( int16_t axis,int16_t phase,int16_t state)
{

    if(axis==0)
    {
        if(state==0)
        {
            switch (phase)
            {
            case 1:
                HWD_Disable_PWM_A_AXIS1;
                break;
            case 2:
                HWD_Disable_PWM_B_AXIS1;
                break;
            case 3:
                HWD_Disable_PWM_C_AXIS1;
                break;
            default:
                break;
            }

        }
        else
        {
            switch (phase)
            {
            case 1:
                HWD_Enable_PWM_A_AXIS2;
                break;
            case 2:
                HWD_Enable_PWM_B_AXIS2;
                break;
            case 3:
                HWD_Enable_PWM_C_AXIS2;
                break;
            default:
                break;
            }
        }
    }
    else
    {
        if(state==0)
        {
            switch (phase)
            {
            case 1:
                HWD_Disable_PWM_A_AXIS2;
                break;
            case 2:
                HWD_Disable_PWM_B_AXIS2;
                break;
            case 3:
                HWD_Disable_PWM_C_AXIS2;
                break;
            default:
                break;
            }

        }
        else
        {
            switch (phase)
            {
            case 1:
                HWD_Enable_PWM_A_AXIS2;
                break;
            case 2:
                HWD_Enable_PWM_B_AXIS2;
                break;
            case 3:
                HWD_Enable_PWM_C_AXIS2;
                break;
            default:
                break;
            }
        }
    }
}


int16_t GetHallsScensorsReading(int16_t axis)
{
    int16_t ret=0;
	volatile int16_t ShadowIO_Reading;
    ShadowIO_Reading = ReplaceRegRead;
#define HWD_Halls_Axis1  (HWD_HallC_Axis1 <<2)| (HWD_HallB_Axis1 <<1)| (HWD_HallA_Axis1 )
#define HWD_Halls_Axis2  (HWD_HallC_Axis2 <<2)| (HWD_HallB_Axis2 <<1)| (HWD_HallA_Axis2 )

#define HWD_HallA_Axis1     ShadowIO_Reading
#define HWD_HallB_Axis1      ShadowIO_Reading
#define HWD_HallC_Axis1      ShadowIO_Reading

#define HWD_HallA_Axis2     ShadowIO_Reading
#define HWD_HallB_Axis2     ShadowIO_Reading
#define HWD_HallC_Axis2     ShadowIO_Reading

    if(axis==0)
    {
        ret = HWD_Halls_Axis1;
    }
    else
    {
        ret = HWD_Halls_Axis2;

    }
    return ret;
}



void SetIncrementalEncoderCounterResolution(int16_t a, int32_t CntsPerRev)
{
    if (a==0)
    {
        ReplaceRegWrite =CntsPerRev-1;
    }
    else
    {
        ReplaceRegWrite =CntsPerRev-1;
    }

}



void SetIncrementalEncoderCounterDirection(int16_t a, int32_t dir)
{
    if (a==0)
    {
        ReplaceRegWrite  = dir;
    }
    else
    {
        ReplaceRegWrite  =dir;
    }

}

uint32_t GetIncrementalEncoderCounterValue(int16_t a)
{
    uint32_t ret=0;
    if (a==0)
    {
        ret= HWD_EncoderCounter_Axis1;
    }
    else
    {
        ret= HWD_EncoderCounter_Axis2;
    }
    return ret;
}


void SetIncrementalEncoderCounterValue(int16_t a, uint32_t val)
{
    if (a==0)
    {
        HWD_EncoderCounter_Axis1=val;
    }
    else
    {
        HWD_EncoderCounter_Axis2=val;
    }

}


void FpgaSyncSignalSet(int16_t val)
{
    if(val==0)
    {
        HWD_FPGA_SyncClear;
    }
    else
    {
        HWD_FPGA_SyncSet;
    }

}

int16_t GetBoardTemperatureReading(int16_t axis)
{
    int16_t ret=0;
    if(axis==0)
    {
        ret= ( (float)(2313 - HWD_Temp_sense_AXIS1)*290.025/4096);
    }
    else
    {
        ret= ( (float)(2313 - HWD_Temp_sense_AXIS2)*290.025/4096);

    }
    return ret;

}


void SetupInterupts (void)
{
ReplaceRegWrite;
}
void EnableInterupts (int16_t cmd)
{
    if(cmd)
    {
        ReplaceRegWrite=1;
    }
    else
    {
        ReplaceRegWrite=0;
    }

}

void AlowSCIBandLowPriorityInterupts (void)
{
    ReplaceRegWrite=1;
}

void AcknowledgeSCIB_Interupt (void)
{

    ReplaceRegWrite;
}





void AcknowledgeHighPriority_Interupt (XTmrCtr *data, uint8_t TmrCtrNumber)
{
	XTmrCtr_Stop(data,TmrCtrNumber);
		XTmrCtr_Reset(data,TmrCtrNumber);
		XTmrCtr_Start(data,TmrCtrNumber);
}

void StartInterupts(void)
{
    ReplaceRegWrite;

    ReplaceRegWrite;
    ReplaceRegWrite;


}


void StopInterupts(void)
{
    ReplaceRegWrite;

    ReplaceRegWrite;

    ReplaceRegWrite;


}


void TemperatureCompensateInternalOscilator(void)
{
    ReplaceRegWrite;

}


void uSleep (int16_t A)
{
    ReplaceRegWrite;
}


int16_t OverCurrentErrorDetected (void)
{
    int16_t ret=0;
    if(ReplaceRegRead)
    {
        ret=1;
    }
    else
    {
        ret=0;
    }
    return ret;
}


void OverCurrentErrorClear (void)
{

    ReplaceRegWrite;
}



uint16_t GetSCIB_TX_Buffer_LVL(void)
{
    return (ReplaceRegRead);
}
uint16_t GetSCIA_TX_Buffer_LVL(void)
{
    return (ReplaceRegRead);
}

uint16_t GetSPIA_TX_Buffer_LVL(void)
{
    return (ReplaceRegRead);
}


uint16_t GetSCIA_RX_Buffer_LVL(void)
{

			return (XUartPs_IsReceiveData( STDOUT_BASEADDRESS) );
}


uint16_t GetSCIB_RX_Buffer_LVL(void)
{
    return (ReplaceRegRead );
}




uint16_t GetSCIA_RX_Buffer(void)
{
    int16_t  ret;   // Recieve data buffer

ret =XUartPs_RecvByte(STDOUT_BASEADDRESS);



    return (ret );

}



uint16_t GetSCIB_RX_Buffer(void)
{
    int16_t    ret;   // Recieve data buffer
    int16_t FrameErrorStatus;
    int16_t ParityErrorStatus;

    ret=ReplaceRegRead;
    FrameErrorStatus= ReplaceRegRead;
    ParityErrorStatus= ReplaceRegRead;


    return (ret );
}





uint16_t GetSPIA_RX_Buffer(void)
{
    return ReplaceRegRead;
}


void SendDataToRAM(uint16_t cmdData)
{

    ReplaceRegWrite = cmdData;

}


void SetSPIA_TX_Buffer(uint16_t val)
{

    ReplaceRegWrite =val;
}


void SetSCIA_TX_Buffer(uint16_t val)
{

	XUartPs_SendByte(STDOUT_BASEADDRESS, val);
}

void SetSCIB_TX_Buffer(uint16_t val)
{

    ReplaceRegWrite =val;
}


void ClearSCIB_ERRORS(void)
{
    if(ReplaceRegRead)
    {
        SCIB_BUFFRE_OVERFLOW++;
        ReplaceRegWrite=1;

    }
    if (ReplaceRegRead ==0) ReplaceRegWrite =1;

    else    if (ReplaceRegRead|| ReplaceRegRead|| ReplaceRegRead||ReplaceRegRead || ReplaceRegRead)
    {
        ReplaceRegWrite =0;
    }
}


void ClearSCIA_ERRORS(void)
{

    if(ReplaceRegRead)
    {
        SCIB_BUFFRE_OVERFLOW++;
        ReplaceRegWrite=1;

    }
    if (ReplaceRegRead ==0) ReplaceRegWrite =1;

    else    if (ReplaceRegRead|| ReplaceRegRead|| ReplaceRegRead||ReplaceRegRead || ReplaceRegRead)
    {
        ReplaceRegWrite =0;
    }


}



uint32_t GetCpuTimer1Value(void)
{
    return ReplaceRegRead;
}


void SetCpuTimer1Value(uint32_t val)
{
    ReplaceRegWrite=val;
}


void ADC_Offset_Callculation (int16_t axis,int16_t *Ia_offset ,int16_t *Ic_offset)
{
    int32_t temp_IA = 0,  temp_IC = 0;


    int16_t i;

    for (i=0 ; i<16;  i++)
    {
        ReplaceRegWrite = 1;
        ReplaceRegWrite = 0;


     //   while ( ReplaceRegRead== 0);


        temp_IA += GetCurrentReadingPhaseA(axis);

        temp_IC += GetCurrentReadingPhaseC(axis);


    }
    *Ia_offset=  temp_IA>>4;
    *Ic_offset=  temp_IC>>4;


}


void Do_DMA_Transfer(void)
{
   ReplaceRegWrite;
}

int16_t DMA_Transfer_finished(void)
{
    return( ReplaceRegRead==0);
}



void Get_DMA_Data(void)
{
    int16_t j;
    int32_t Calculated_SPI_Data_checkSum=0;
    int32_t SPI_Data_checkSumDiff=0;

    Calculated_SPI_Data_checkSum=0;

    for(j=0; j<SPI_TO_CPLD_LENGHT-1; j++)
    {
        ShadowSPI_RX_DATA[j]= ((uint32_t)DMA_rdata[j*2]<<16)| DMA_rdata[j*2+1];
        LocalSPI_RX_DATA[2*j] = DMA_rdata[j*2];
        LocalSPI_RX_DATA[2*j+1] = DMA_rdata[j*2+1];
        Calculated_SPI_Data_checkSum += ShadowSPI_RX_DATA[j];

    }

    //copy checksum
    ShadowSPI_RX_DATA[j]= ((uint32_t)DMA_rdata[j*2]<<16)| DMA_rdata[j*2+1];


    SPI_Data_checkSumDiff =(Calculated_SPI_Data_checkSum) -ShadowSPI_RX_DATA[SPI_TO_CPLD_LENGHT-1];

    if (SPI_Data_checkSumDiff==0)//checksum correct
    {
        SPI_Data_checkSumDiffCNTR=0;
        CPLD_FIRST_SPI_Data_Recieved_Flag=1;
        CPLD_SPI_Data_Recieved_Flag=1;

    }
    else
    {
        SPI_Data_checkSumDiffCNTR++;
    }

    if(CPLD_SPI_FaultCtr>0)  CPLD_SPI_FaultCtr--;
    else  CPLD_SPI_FaultCtr=0;}



void SetSPI_A_messageSize(int16_t val)
{
    ReplaceRegWrite = val;
}


void SetupExternalRAM_ForRecord(int16_t val)
{
    if(val==1)
    {
        ReplaceRegWrite = 1;// enable chipselect
        SetSPI_A_messageSize(0xF);

        ReplaceRegWrite=(0x0141);
    }
    else
    {
        ReplaceRegWrite= 1;//disable chipselect

    }
}

void InitializeExternalRAM_ForRecord(void)
{

    ReplaceRegWrite = 1;
    uSleep(10);         // Delay before converting ADC channels
    ReplaceRegWrite= 0;
    ReplaceRegWrite= 0;


    ReplaceRegWrite= 1;
    SetSPI_A_messageSize(0x7);

    ReplaceRegWrite=(2<<8);
    ReplaceRegWrite=(0);
    ReplaceRegWrite=(0);
}



uint32_t ReadRamLong(uint16_t addr)
{
    uint32_t data=0;
    uint32_t temp1,temp2,temp3,temp4;
    int16_t temp=0;
    int16_t timeout = 2000;


    ReplaceRegWrite = 0x7; //16 bits
    ReplaceRegWrite= 1;
    temp++;

    ReplaceRegWrite=(3<<8);// Write READ OPCODE '11'
    //Send address
    addr=addr*4;
    ReplaceRegWrite=(((addr>>8) & 0xff)<<8);
    ReplaceRegWrite=((addr & 0xff)<<8);
    timeout=1000;
    while( ((ReplaceRegRead) !=0) && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    //Read data
    timeout=1000;
    while( ((ReplaceRegRead) !=0) && timeout > 0)
    {
        timeout--;

        data = GetSPIA_RX_Buffer();
        uSleep(1);

    }
    ReplaceRegWrite=(0x0000);
    ReplaceRegWrite=(0x0000);
    ReplaceRegWrite=(0x0000);
    ReplaceRegWrite=(0x0000);
    timeout=1000;
    while( (ReplaceRegRead) <4 && timeout > 0)
    {

        uSleep(1);

        timeout--;
    }
    temp1 =  GetSPIA_RX_Buffer();
    temp2 =  GetSPIA_RX_Buffer();
    temp3 =  GetSPIA_RX_Buffer();
    temp4 =  GetSPIA_RX_Buffer();

    data = (temp1<<24) |((temp2<<16) |(temp3<<8) |(temp4));

    //Set clock phase to previos position
    // SpiaRegs.SPICTL.bit.CLK_PHASE = 1;
    uSleep(10);

    //Set EEPROM CS enable low
    ReplaceRegWrite= 1;
    //Update Checksum
    //   Config_Data_Checksum = Config_Data_Checksum + data;

    return data;
}


uint16_t ReadRamInt(uint16_t addr)
{
    uint32_t data=0;
    uint32_t temp3,temp4;
    int16_t temp=0;
    int16_t timeout = 2000;


    ReplaceRegWrite = 0x7; //16 bits
    ReplaceRegWrite = 1;
    ReplaceRegWrite=(3<<8);// Write READ OPCODE '11'
    //Send address
    addr=addr*2;
    ReplaceRegWrite=(((addr>>8) & 0xff)<<8);
    ReplaceRegWrite=((addr & 0xff)<<8);
    timeout=1000;
    while( ((ReplaceRegRead) !=0)& timeout>0)
    {
        timeout--;
        uSleep(1);

    }
    //Read data
    timeout=1000;
    while(( (ReplaceRegRead) !=0)& timeout>0)
    {
        timeout--;
        data = GetSPIA_RX_Buffer();
        uSleep(2);

    }
    ReplaceRegWrite=(0x0000);
    ReplaceRegWrite=(0x0000);

    while( (ReplaceRegRead) <4 && timeout > 0)
    {

        uSleep(1);

        timeout--;
    }

    //   temp3 = Record.Memory[addr];// GetSPIA_RX_Buffer();
    //   temp4 =  Record.Memory[addr+1];//GetSPIA_RX_Buffer();
    temp3 =GetSPIA_RX_Buffer();
    temp4 =GetSPIA_RX_Buffer();

    data = ((temp3<<8) |(temp4));

    //Set clock phase to previos position
    // SpiaRegs.SPICTL.bit.CLK_PHASE = 1;
    uSleep(10);

    //Set EEPROM CS enable low
    ReplaceRegWrite = 1;
    //Update Checksum
    //   Config_Data_Checksum = Config_Data_Checksum + data;
    temp++;

    return (int16_t)data;
}


int16_t I2C_Write_EEPROM_Int (int16_t address, int16_t data  )
{
    int16_t temp, timeout,intcode;

    timeout = 10000;
    while ( timeout > 0)
    {
        uSleep(1);
        timeout--;
    }

    timeout = 10000;
    while ( ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }
    if ((uint16_t) address < 0xff) ReplaceRegWrite = (0x50);
    else    if ((uint16_t) address < 0x1ff) ReplaceRegWrite = (0x51);
    else    if ((uint16_t) address < 0x2ff) ReplaceRegWrite = (0x52);
    else    if ((uint16_t) address < 0x3ff) ReplaceRegWrite = (0x53);
    //  else    if ((uint16_t) address < 0x4ff) ReplaceRegWrite = (0x54);
    else    return -1;

    timeout = 2000;
    while ( ReplaceRegRead== 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }

    // Setup number of bytes to send
    // MsgBuffer + Address
    ReplaceRegWrite =3;
    //address is 16 bit
    //address=address;
    ReplaceRegWrite= (char)(address&0xff);
    ReplaceRegWrite= (char)((data>>8)&0xff);
    ReplaceRegWrite= (char)(data&0xff);

    // Send start as master transmitter
    /*
0-2 I2caRegs.I2CMDR.bit.BC=000;//8 Bits data
3   I2caRegs.I2CMDR.bit.FDF=0;//Free datja format mode is disabled

4   I2caRegs.I2CMDR.bit.STB=0;//The I2C module is not in the START byte mode.
                            //only 1 bit is used for start
5   I2caRegs.I2CMDR.bit.IRS=1;
6   I2caRegs.I2CMDR.bit.DLB=0;//Digital loopback mode is disabled
7   I2caRegs.I2CMDR.bit.RM=0;//Nonrepeat mode. The value in the data count register (I2CCNT) determines how many bytes are
                            //received/transmitted by the I2C module.3

8   I2caRegs.I2CMDR.bit.XA=0;//7-bit addressing mode (normal address mode).
9   I2caRegs.I2CMDR.bit.TRX=1;//Transmitter mode. The I2C module is a transmitter and transmits data on the SDA pin.
10  I2caRegs.I2CMDR.bit.MST=1;//Master mode. The I2C module is a master and generates the serial clock on the SCL pin.
11  I2caRegs.I2CMDR.bit.STP=1;//STP has been set by the DSP to generate a STOP condition when the internal data counter of the                              //I2C module counts down to 0.

12  ///Reserved 0
13  I2caRegs.I2CMDR.bit.STT=1;//In the master mode, setting STT to 1 causes the I2C module to generate a START condition
14  I2caRegs.I2CMDR.bit.FREE=1;//The I2C module runs free; that is, it continues to operate when a breakpoint occurs
15  I2caRegs.I2CMDR.bit.NACKMOD=0;//The I2C module sends an ACK bit during each acknowledge cycle

110111000100000
     */
    ReplaceRegWrite = 0x6E20;
    timeout = 2000;
    intcode = ReplaceRegRead;
    while (( intcode != ReplaceRegRead) && timeout > 0)
    {
        intcode = ReplaceRegRead;
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }
    timeout = 2000;
    /*  while ( ReplaceRegRead == 1 && timeout > 0)
    {
        HWD_DELAY;
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }
     */
    while ( ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }

    timeout = 2000;
    while ( ReplaceRegRead == 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Write_Fault_Flag = 1;
        return -1;
    }
    //acknolag

    temp =ReplaceRegRead ;
    temp++;
    return(0);
}

int16_t I2C_Read_EEPROM_Int(uint16_t address)
{


    int16_t data = 0;
    int16_t temp,intcode=-1;
    int16_t timeout;



    timeout = 20000;
    while (ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 20000;
    while (ReplaceRegRead == 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 20000;
    /* while ( intcode!=I2C_NO_ISRC && timeout > 0)
    {
        intcode = ReplaceRegRead;
        temp =ReplaceRegRead ;
        HWD_DELAY;
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }
     */


    //address is 16 bit
    //address=address;
    /*
    I2caRegs.I2CMDR.bit.BC=000;//8 Bits data
    I2caRegs.I2CMDR.bit.FDF=0;//Free data format mode is disabled

    I2caRegs.I2CMDR.bit.STB=0;//The I2C module is not in the START byte mode.
                            //only 1 bit is used for start
    I2caRegs.I2CMDR.bit.IRS=1;
    I2caRegs.I2CMDR.bit.DLB=0;//Digital loopback mode is disabled
    I2caRegs.I2CMDR.bit.RM=0;//Nonrepeat mode. The value in the data count register (I2CCNT) determines how many bytes are
                            //received/transmitted by the I2C module.3

    I2caRegs.I2CMDR.bit.XA=0;//7-bit addressing mode (normal address mode).
    I2caRegs.I2CMDR.bit.TRX=1;//Transmitter mode. The I2C module is a transmitter and transmits data on the SDA pin.
    I2caRegs.I2CMDR.bit.MST=1;//Master mode. The I2C module is a master and generates the serial clock on the SCL pin.
    I2caRegs.I2CMDR.bit.STP=0;//STP has been set by the DSP to generate a STOP condition when the internal data counter of the
                                //I2C module counts down to 0.

    ///Reserved 0
    I2caRegs.I2CMDR.bit.STT=1;//In the master mode, setting STT to 1 causes the I2C module to generate a START condition
    I2caRegs.I2CMDR.bit.FREE=1;//The I2C module runs free; that is, it continues to operate when a breakpoint occurs
    I2caRegs.I2CMDR.bit.NACKMOD=0;//The I2C module sends an ACK bit during each acknowledge cycle
     */
    if ((uint16_t) address < 0xff) ReplaceRegWrite = (0x50);
    else    if ((uint16_t) address < 0x1ff) ReplaceRegWrite = (0x51);
    else    if ((uint16_t) address < 0x2ff) ReplaceRegWrite = (0x52);
    else    if ((uint16_t) address < 0x3ff) ReplaceRegWrite = (0x53);
    //  else    if ((uint16_t) address < 0x4ff) ReplaceRegWrite = (0x54);
    else    return -1;
    ReplaceRegWrite =1;

    //011011000100000
    //11011000100000
    ReplaceRegWrite= 0x3620;           // Send data to setup EEPROM address
    ReplaceRegWrite= (char)(address&0xff);
    //  while ( I2caRegs.I2CMDR.bit.STP==1 )HWD_DELAY;


    intcode=0;
    timeout = 20000;
    while ( intcode!=ReplaceRegRead && timeout > 0)
    {
        intcode = ReplaceRegRead;
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    /*  I2caRegs.I2CMDR.bit.BC=000;//8 Bits data
    I2caRegs.I2CMDR.bit.FDF=0;//Free data format mode is disabled

    I2caRegs.I2CMDR.bit.STB=0;//The I2C module is not in the START byte mode.
                            //only 1 bit is used for start
    I2caRegs.I2CMDR.bit.IRS=1;
    I2caRegs.I2CMDR.bit.DLB=0;//Digital loopback mode is disabled
    I2caRegs.I2CMDR.bit.RM=0;//Nonrepeat mode. The value in the data count register (I2CCNT) determines how many bytes are
                            //received/transmitted by the I2C module.3

    I2caRegs.I2CMDR.bit.XA=0;//7-bit addressing mode (normal address mode).
    I2caRegs.I2CMDR.bit.TRX=0;//Transmitter mode. The I2C module is a transmitter and transmits data on the SDA pin.
    I2caRegs.I2CMDR.bit.MST=1;//Master mode. The I2C module is a master and generates the serial clock on the SCL pin.
    I2caRegs.I2CMDR.bit.STP=1;//STP has been set by the DSP to generate a STOP condition when the internal data counter of the
                                //I2C module counts down to 0.

    ///Reserved 0
    I2caRegs.I2CMDR.bit.STT=1;//In the master mode, setting STT to 1 causes the I2C module to generate a START condition
    I2caRegs.I2CMDR.bit.FREE=1;//The I2C module runs free; that is, it continues to operate when a breakpoint occurs
    I2caRegs.I2CMDR.bit.NACKMOD=0;//The I2C module sends an ACK bit during each acknowledge cycle

     */
    ReplaceRegWrite =2;
    ReplaceRegWrite = 0x3C20;           // Send restart as master receiver


    timeout = 20000;
    while ( intcode!=ReplaceRegWrite && timeout > 0)
    {
        intcode = ReplaceRegRead;
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    WAIT_FOR_DATA;
    data = ReplaceRegRead;
    WAIT_FOR_DATA;
    temp =ReplaceRegRead ;
    data = (data<<8) | temp;
    timeout = 20000;
    while ( ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 2000;
    while ( ReplaceRegRead == 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }
    if(ReplaceRegRead== 1)
    {
        ReplaceRegWrite = 1;
        ReplaceRegWrite = 1;
        //          while ( I2caRegs.I2CSTR.bit.ARDY==0)HWD_DELAY;
    }

    return data;
}


int16_t I2C_Read_CPLD_Byte(uint16_t address)
{


    int16_t data = 0;
    int16_t intcode=-1;
    int16_t timeout;



    timeout = 20000;
    while ( ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 20000;
    while ( ReplaceRegRead == 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 20000;
    /* while ( intcode!=I2C_NO_ISRC && timeout > 0)
    {
        intcode = ReplaceRegRead;
        temp =ReplaceRegRead ;
        HWD_DELAY;
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }
     */


    //address is 16 bit
    //address=address;
    /*
    I2caRegs.I2CMDR.bit.BC=000;//8 Bits data
    I2caRegs.I2CMDR.bit.FDF=0;//Free data format mode is disabled

    I2caRegs.I2CMDR.bit.STB=0;//The I2C module is not in the START byte mode.
                            //only 1 bit is used for start
    I2caRegs.I2CMDR.bit.IRS=1;
    I2caRegs.I2CMDR.bit.DLB=0;//Digital loopback mode is disabled
    I2caRegs.I2CMDR.bit.RM=0;//Nonrepeat mode. The value in the data count register (I2CCNT) determines how many bytes are
                            //received/transmitted by the I2C module.3

    I2caRegs.I2CMDR.bit.XA=0;//7-bit addressing mode (normal address mode).
    I2caRegs.I2CMDR.bit.TRX=1;//Transmitter mode. The I2C module is a transmitter and transmits data on the SDA pin.
    I2caRegs.I2CMDR.bit.MST=1;//Master mode. The I2C module is a master and generates the serial clock on the SCL pin.
    I2caRegs.I2CMDR.bit.STP=0;//STP has been set by the DSP to generate a STOP condition when the internal data counter of the
                                //I2C module counts down to 0.

    ///Reserved 0
    I2caRegs.I2CMDR.bit.STT=1;//In the master mode, setting STT to 1 causes the I2C module to generate a START condition
    I2caRegs.I2CMDR.bit.FREE=1;//The I2C module runs free; that is, it continues to operate when a breakpoint occurs
    I2caRegs.I2CMDR.bit.NACKMOD=0;//The I2C module sends an ACK bit during each acknowledge cycle
     */
    ReplaceRegWrite = (0x40);

    ReplaceRegWrite =1;

    //011011000100000
    //11011000100000
    ReplaceRegWrite = 0x3620;           // Send data to setup EEPROM address
    ReplaceRegWrite= (char)(address&0xff);
    //  while ( I2caRegs.I2CMDR.bit.STP==1 )HWD_DELAY;


    intcode=0;
    timeout = 20000;
    while ( intcode!=1 && timeout > 0)
    {
        intcode = ReplaceRegRead;
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    /*  I2caRegs.I2CMDR.bit.BC=000;//8 Bits data
    I2caRegs.I2CMDR.bit.FDF=0;//Free data format mode is disabled

    I2caRegs.I2CMDR.bit.STB=0;//The I2C module is not in the START byte mode.
                            //only 1 bit is used for start
    I2caRegs.I2CMDR.bit.IRS=1;
    I2caRegs.I2CMDR.bit.DLB=0;//Digital loopback mode is disabled
    I2caRegs.I2CMDR.bit.RM=0;//Nonrepeat mode. The value in the data count register (I2CCNT) determines how many bytes are
                            //received/transmitted by the I2C module.3

    I2caRegs.I2CMDR.bit.XA=0;//7-bit addressing mode (normal address mode).
    I2caRegs.I2CMDR.bit.TRX=0;//Transmitter mode. The I2C module is a transmitter and transmits data on the SDA pin.
    I2caRegs.I2CMDR.bit.MST=1;//Master mode. The I2C module is a master and generates the serial clock on the SCL pin.
    I2caRegs.I2CMDR.bit.STP=1;//STP has been set by the DSP to generate a STOP condition when the internal data counter of the
                                //I2C module counts down to 0.

    ///Reserved 0
    I2caRegs.I2CMDR.bit.STT=1;//In the master mode, setting STT to 1 causes the I2C module to generate a START condition
    I2caRegs.I2CMDR.bit.FREE=1;//The I2C module runs free; that is, it continues to operate when a breakpoint occurs
    I2caRegs.I2CMDR.bit.NACKMOD=0;//The I2C module sends an ACK bit during each acknowledge cycle

     */
    ReplaceRegWrite =1;
    ReplaceRegWrite = 0x3C20;           // Send restart as master receiver


    timeout = 20000;
    while ( intcode!=ReplaceRegRead && timeout > 0)
    {
        intcode = ReplaceRegRead;
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    WAIT_FOR_DATA;
    data =ReplaceRegRead;

    timeout = 20000;
    while ( ReplaceRegRead==1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }

    timeout = 2000;
    while ( ReplaceRegRead == 1 && timeout > 0)
    {
        uSleep(1);
        timeout--;
    }
    if (timeout == 0)
    {
        Eeprom_Read_Fault_Flag = 1;
        return -1;
    }
    if(ReplaceRegRead == 1)
    {
        ReplaceRegWrite = 1;
        ReplaceRegWrite = 1;
    }

    return data;
}

/*
extern int ScuGicInterrupt_Init(u16 DeviceId,XTmrCtr *TimerInstancePtr);
extern XScuGic InterruptController; // Instance of the Interrupt Controller
extern  XScuGic_Config *GicConfig;// The configuration parameters of the
void InitSystem__(void)
{
	XTmrCtr TimerInstancePtr;
		int xStatus;

	init_platform();

	xStatus = XTmrCtr_Initialize(&TimerInstancePtr,XPAR_AXI_TIMER_0_DEVICE_ID);
		if(XST_SUCCESS != xStatus)
			print("TIMER INIT FAILED \n\r");
		//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//Set Timer Handler
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			XTmrCtr_SetHandler(&TimerInstancePtr,
					HighPriorityInterupt,
					&TimerInstancePtr);



			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//Setting timer Reset Value
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			XTmrCtr_SetResetValue(&TimerInstancePtr,
					0, //Change with generic value
					0xFFFFFFFF - NUMBER_OF_COUNTS_FOR_INTERRUPT);
			//0xFFE17B80

			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//Setting timer Option (Interrupt Mode And Auto Reload )
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			XTmrCtr_SetOptions(&TimerInstancePtr,
					XPAR_AXI_TIMER_0_DEVICE_ID,
					(XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION ));

			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			//SCUGIC interrupt controller Intialization
			//Registration of the Timer ISR
			//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

			xStatus=ScuGicInterrupt_Init(XPAR_PS7_SCUGIC_0_DEVICE_ID,&TimerInstancePtr);
			if(XST_SUCCESS != xStatus)
				print(" :( SCUGIC INIT FAILED \n\r");

			//Start Timer

			XTmrCtr_Start(&TimerInstancePtr,0);
}
*/
void ExitSystem(void)
{

cleanup_platform();
}
void BranchToBoot(void)
{
ReplaceRegWrite;
}



void ServiceWatchDog(void)
{
    ReplaceRegWrite;

}

void DisableWatchDog(void)
{
    ReplaceRegWrite;

}

void EnableWatchDog(void)
{
    ReplaceRegWrite;

}


void InitPwm(int16_t freq)
{
    ReplaceRegWrite;
}




void WriteDacOutput(int16_t dacOutput)
{
   ReplaceRegWrite;
}

